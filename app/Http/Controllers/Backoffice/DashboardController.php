<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Student;
use App\Models\Register;
use App\Utilities\HashId;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Carbon, DB, Response, Cache;

class DashboardController extends Controller
{
	use HashId;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$sumStudent = Student::count();
		$sumStudentMan = Student::where('gender', 'Laki-laki')->count();
		$sumStudentWoman = Student::where('gender', 'Perempuan')->count();
		$sumRegister = Register::count();
		
        return view('backoffice.dashboard', compact('sumStudent', 'sumStudentMan', 'sumStudentWoman', 'sumRegister'));
    }

    /**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		try {
			$id = $this->decodeHash($id);
			$data = Admin::where('id', $id)->first();
			return view('backoffice.auth.profile.show', compact('data'));
        } catch (\Exception $exc) {
            return redirect()->back()->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
        }
	}

    public function editProfile($id)
	{
		try {
			$id = $this->decodeHash($id);
			$data = Admin::where('id', $id)->first();
			return view('backoffice.auth.profile.edit', compact('data'));

        } catch (\Exception $exc) {
            return redirect()->back()->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
        }
	}

    public function updateProfile(Request $request, $id)
	{
		DB::beginTransaction();
		try {
            $validator = Validator::make($request->all(), [
                'image'          		=> 'required|image|mimes:jpg,png,jpeg,gif,svg|max:1000',
            ]);

			if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

            if (!empty($request->file('image'))) {
                $data = Admin::where('id', $id)->first();
				if (\File::exists(public_path('backoffice/assets/images/admin/' . $data->image))) {
                    \File::delete(public_path('backoffice/assets/images/admin/' . $data->image));
				}
				$image = time() . '.' .$request->file('image')->getClientOriginalExtension();
				$destinationPath = public_path('backoffice/assets/images/admin/');
				$request->file('image')->move($destinationPath, $image);

			}

			if(empty($request->file('image'))) {
				$data = Admin::where('id', $id)->first();
				$request['image'] = $data->image;
			}
            $data = Admin::where('id', $id)->first();
			$data->update([
                'name' 			=> isset($request['name']) ? $request->input('name') : null,
                'email' 		=> isset($request['email']) ? $request->input('email') : null,
                'phone_number' 	=> isset($request['phone_number']) ? $request->input('phone_number') : null,
                'gender' 	    => isset($request['gender']) ? $request->input('gender') : null,
                'address' 	    => isset($request['address']) ? $request->input('address') : null,
                'image' 	    => $image,
            ]);

			if (!$data->exists) {
				return redirect()->route('backoffice.dashboard')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
			}

			return redirect()->route('backoffice.dashboard')->with('success', 'Admin Berhasil dirubah');

		 	Cache::flush();
			DB::commit();
        } catch (\Exception $exc) {
            DB::rollback();
			return redirect()->route('backoffice.dashboard')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
		}
	}

    public function changePassword($id)
	{
		try {
			$id = $this->decodeHash($id);
            $data = Admin::where('id', $id)->first();
			return view('backoffice.auth.profile.change_password', compact('data'));
        } catch (\Exception $exc) {
            return redirect()->back()->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
        }
	}

	public function updatePassword(Request $request, $id)
	{
		DB::beginTransaction();
		try {
            $validator = Validator::make($request->all(), [
                'password' 			=> 'required|min:3',
                'password_confirmation' 	=> 'required|same:password',
            ]);

            $data = Admin::where('id', $id)->first();
			$data->update([
				'password' => Hash::make($request['password'])
			]);

			if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

			return redirect()->route('backoffice.dashboard')->with('success', 'Password Berhasil dirubah');

		 	Cache::flush();
			DB::commit();
        } catch (\Exception $exc) {
            DB::rollback();
			return redirect()->back()->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
		}
	}

}
