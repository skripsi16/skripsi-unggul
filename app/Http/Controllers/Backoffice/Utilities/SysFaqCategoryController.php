<?php

namespace App\Http\Controllers\Backoffice\Utilities;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SysFaqCategory;
use App\Utilities\AutoNumber;
use App\Utilities\HashId;
use Illuminate\Support\Facades\Validator;
use DB, Cache;

class SysFaqCategoryController extends Controller
{
    use AutoNumber, HashId;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
			$data = SysFaqCategory::orderBy('created_at','DESC')->paginate($request->input('limit', 15));

			return view('backoffice.utilities.faq_category.index', compact('data'));
		} catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('backoffice.utilities.faq_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
				'name' => 'required',
                'description' => 'required',
			]);

			if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

            $data = SysFaqCategory::create([
                'id' 			=> $request['id'] = $this->GenerateAutoNumber('sys_faq_categories'),
                'name' 			=> isset($request['name']) ? $request->input('name') : null,
                'description' 	=> isset($request['description']) ? $request->input('description') : null,
                'enabled' 		=> isset($request['enabled']) ? $request->input('enabled') : null,
            ]);
           
            Cache::flush();
			DB::commit();
            if (!$data->exists) {
                return redirect()->route('faq_categories.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
            return redirect()->route('faq_categories.index')->with('success', 'Data Berhasil ditambahkan');
        } catch (\Exception $exc) {
            DB::rollback();
            return $exc;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = $this->decodeHash($id);
            $data = SysFaqCategory::where('id', $id)->first();

            if (!$data->exists) {
                return redirect()->route('faq_categories.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }

            return view('backoffice.utilities.faq_category.edit', compact('data'));
        } catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
			$data = SysFaqCategory::where('id', $id)->first();
            $data->update([
                'name' 			=> isset($request['name']) ? $request->input('name') : null,
                'description' 	=> isset($request['description']) ? $request->input('description') : null,
                'enabled' 		=> isset($request['enabled']) ? $request->input('enabled') : null,
            ]);
           
            Cache::flush();
			DB::commit();
            if (!$data->exists) {
                return redirect()->route('faq_categories.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
            return redirect()->route('faq_categories.index')->with('success', 'Data Berhasil ditambahkan');
        } catch (\Exception $exc) {
            DB::rollback();
            return $exc;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	public function activeNonActive($id, $status)
	{
        DB::beginTransaction();
       	try {
            $id = $this->decodeHash($id);
			$activeNonActive = SysFaqCategory::where('id', $id)->update([
                'enabled' => $status,
            ]);
			//dd($activeNonActive);

            Cache::flush();
			DB::commit();
            if (!$activeNonActive) {
                return redirect()->route('faq_categories.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }

			return redirect()->route('faq_categories.index')->with('success', 'Data Berhasil diubah');
        } catch (\Exception $exc) {
            DB::rollback();
        }
	}

    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {
            //get filename with extension
            $filenamewithextension = $request->file('upload')->getClientOriginalName();
      
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
      
            //get file extension
            $extension = $request->file('upload')->getClientOriginalExtension();
      
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
      
            //Upload File
            $request->file('upload')->storeAs('public/category', $filenametostore);
 
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('storage/category/'.$filenametostore);
            $msg = 'Image successfully uploaded';
            $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
             
            // Render HTML output
            @header('Content-type: text/html; charset=utf-8');
            echo $re;
        }
    }
}
