<?php

namespace App\Http\Controllers\Backoffice\Utilities;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SysProfileCompany;
use Spatie\Permission\Models\Permission;
use App\Utilities\AutoNumber;
use App\Utilities\HashId;
use DB, Auth, Cache;

class SysProfileCompanyController extends Controller
{
    use Autonumber, HashId;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $profile = SysProfileCompany::all()->first();
            $id = $profile->id;
            
            return redirect()->route('profile-company.show', $id);

		} catch (\Exception $exc) {
			 return $exc;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
			// 
		} catch (\Exception $exc) {
			 return $exc;
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = SysProfileCompany::where('id', $id)->first();

            $staticPage = isset($data->description) ? json_decode($data->description) : 'null';
            $address = isset($data->address) ? json_decode($data->address) : 'null';
            $phone_number = isset($data->phone_number) ? json_decode($data->phone_number) : 'null';
            $email = isset($data->email) ? json_decode($data->email) : 'null';
            $website = isset($data->website) ? json_decode($data->website) : 'null';
            $social_media = isset($data->social_media) ? json_decode($data->social_media) : 'null';

            $staticPage = [
                'profile' => ($staticPage->profile) ?? '-',
                'visimisi' => ($staticPage->visimisi) ?? '-',
                'sejarah' => ($staticPage->sejarah) ?? '-',
                'manajemen' => ($staticPage->manajemen) ?? '-',
                'legalitas' => ($staticPage->legalitas) ?? '-',
                'termcondition' => ($staticPage->termcondition) ?? '-',
                'termfundraiser' => ($staticPage->termfundraiser) ?? '-',
                'privacy' => ($staticPage->privacy) ?? '-',
            ];

            $address = [
                'address1' => ($address->address1) ?? '-',
                'address2' => ($address->address2) ?? '-',
            ];

            $phone_number = [
                'phone_1' => ($phone_number->phone_1) ?? '-',
                'phone_2' => ($phone_number->phone_2) ?? '-',
            ];

            $email = [
                'email1' => ($email->email1) ?? '-',
                'email2' => ($email->email2) ?? '-',
            ];

            $website = [
                'website1' => ($website->website1) ?? '-',
                'website2' => ($website->website2) ?? '-',
            ];

            $social_media = [
                'facebook' => ($social_media->facebook) ?? '-',
                'instagram' => ($social_media->instagram) ?? '-',
                'youtube' => ($social_media->youtube) ?? '-',
            ];

            
            if (!$data) {
                return redirect()->route('profile-company.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }

            return view('backoffice.utilities.profilecompany.create', compact('data', 'staticPage', 'address', 'phone_number', 'email', 'website', 'social_media'));
        } catch (\Exception $exc) {
             return $exc;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $data = SysProfileCompany::where('id', $id)->first();
            $staticPage = json_decode($data->description);
            $address = json_decode($data->address);
            $phone_number = json_decode($data->phone_number);
            $email = json_decode($data->email);
            $website = json_decode($data->website);
            $social_media = json_decode($data->social_media);

            $staticPage = [
                'profile' => ($staticPage->profile) ?? '-',
                'visimisi' => ($staticPage->visimisi) ?? '-',
                'sejarah' => ($staticPage->sejarah) ?? '-',
                'manajemen' => ($staticPage->manajemen) ?? '-',
                'legalitas' => ($staticPage->legalitas) ?? '-',
                'termcondition' => ($staticPage->termcondition) ?? '-',
                'termfundraiser' => ($staticPage->termfundraiser) ?? '-',
                'privacy' => ($staticPage->privacy) ?? '-',
            ];

            $address = [
                'address1' => ($address->address1) ?? '-',
                'address2' => ($address->address2) ?? '-',
            ];

            $phone_number = [
                'phone_1' => ($phone_number->phone_1) ?? '-',
                'phone_2' => ($phone_number->phone_2) ?? '-',
            ];

            $email = [
                'email1' => ($email->email1) ?? '-',
                'email2' => ($email->email2) ?? '-',
            ];

            $website = [
                'website1' => ($website->website1) ?? '-',
                'website2' => ($website->website2) ?? '-',
            ];

            $social_media = [
                'facebook' => ($social_media->facebook) ?? '-',
                'instagram' => ($social_media->instagram) ?? '-',
                'youtube' => ($social_media->youtube) ?? '-',
            ];


            if (!$data->exists) {
                return redirect()->route('profile-company.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }

            return view('backoffice.utilities.profilecompany.create', compact('data', 'staticPage', 'address', 'phone_number', 'email', 'website', 'social_media'));
        } catch (\Exception $exc) {
             return $exc;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $request['id'] = $id;

            if(!empty($request->file('image_url'))){
                $image = time() . '.' .$request->file('image_url')->getClientOriginalExtension();
            }else if (isset($request['image_url'])){
                $image = $request['image_url'];
            }else {
                $image = null;
            }
    
            $description = [
                'profile' => $request->input('profile'),
                'visimisi' => $request->input('visimisi'),
                'sejarah' => $request->input('sejarah'),
                'manajemen' => $request->input('manajemen'),
                'legalitas' => $request->input('legalitas'),
                'termcondition' => $request->input('termcondition'),
                'termfundraiser' => $request->input('termfundraiser'),
                'privacy' => $request->input('privacy')
    
            ];
    
            $phone_number = [
                'phone_1' => $request->input('phone_1'),
                'phone_2' => $request->input('phone_2')
            ];
    
            $social_media = [
                'facebook' => $request->input('facebook'),
                'instagram' => $request->input('instagram'),
                'youtube' => $request->input('youtube'),
            ];
    
            $website = [
                'website1' => $request->input('website1'),
                'website2' => $request->input('website2')
            ];
    
            $address = [
                'address1' => $request->input('address1'),
                'address2' => $request->input('address2')
            ];
    
            $email = [
                'email1' => $request->input('email1'),
                'email2' => $request->input('email2')
            ];
    
            $jsonDescription = json_encode($description);
            $jsonPhone_number = json_encode($phone_number);
            $jsonSocial_media = json_encode($social_media);
            $jsonWebsite = json_encode($website);
            $jsonAddress = json_encode($address);
            $jsonEmail = json_encode($email);
    
    
            $data = SysProfileCompany::where('id', $id)->first();
            // Update
            $data->update([
                 'id' 			=> isset($request['id']) ? $request->input('id') : null,
                'name' 			=> isset($request['name']) ? $request->input('name') : null,
                'description' 	=> isset($jsonDescription) ? $jsonDescription : null,
                'image_url' 	=> $image,
                'website' 		=> isset($jsonWebsite) ? $jsonWebsite : null,
                'address'   	=> isset($jsonAddress) ? $jsonAddress : null,
                'email' 		=> isset($jsonEmail) ? $jsonEmail : null,
                'phone_number' 	=> isset($jsonPhone_number) ? $jsonPhone_number : null,
                'social_media' 	=> isset($jsonSocial_media) ? $jsonSocial_media : null,
                'created_by' 	=> Auth::user()->name,
                'updated_by' 	=> Auth::user()->name,
            ]);
            
            Cache::flush();
			DB::commit();
            if (!$data->exists) {
                return redirect()->route('profile-company.show', $id)->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
    
            return redirect()->route('profile-company.show', $id)->with('success', 'Data Berhasil diubah');
        } catch (\Exception $exc) {
            DB::rollback();
            return $exc;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
			$delete = SysProfileCompany::where('id', $id)->delete();

			return redirect()->route('profile-company.index')->with('success', 'Data Berhasil dihapus');
        } catch (\Exception $exc) {
             return $exc;
        }
    }

    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {
            //get filename with extension
            $filenamewithextension = $request->file('upload')->getClientOriginalName();
      
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
      
            //get file extension
            $extension = $request->file('upload')->getClientOriginalExtension();
      
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
      
            //Upload File
            $request->file('upload')->storeAs('public/profile-company', $filenametostore);
 
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('storage/profile-company/'.$filenametostore);
            $msg = 'Image successfully uploaded';
            $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
             
            // Render HTML output
            @header('Content-type: text/html; charset=utf-8');
            echo $re;
        }
    }
}
