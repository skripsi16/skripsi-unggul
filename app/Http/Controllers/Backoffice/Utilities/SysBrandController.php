<?php

namespace App\Http\Controllers\Backoffice\Utilities;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SysBrand;
use App\Utilities\AutoNumber;
use App\Utilities\HashId;
use Illuminate\Support\Facades\Validator;
use DB, Cache;

class SysBrandController extends Controller
{
    use AutoNumber, HashId;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
			$data = SysBrand::orderBy('created_at','DESC')->paginate($request->input('limit', 15));

			return view('backoffice.utilities.brand.index', compact('data'));
		} catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('backoffice.utilities.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'image'          		=> 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);

			if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

            if (!empty($request->file('image'))) {
				if (\File::exists(public_path('backoffice/assets/images/brand/' . $request->image))) {
                    \File::delete(public_path('backoffice/assets/images/brand/' . $request->image));
				}
				$image = time() . '.' .$request->file('image')->getClientOriginalExtension();
				$destinationPath = public_path('backoffice/assets/images/brand/');
				$request->file('image')->move($destinationPath, $image);

			}  
            
            if(!empty($request->file('image'))){
                $image = time() . '.' .$request->file('image')->getClientOriginalExtension();
            }else if (isset($request['image'])){
                $image = $request['image'];
            }else {
                $image = null;
            }

            $data = SysBrand::create([
                'id' 			=> $request['id'] = $this->GenerateAutoNumber('sys_brands'),
                'image' 		=> $image,
                'description' 	=> isset($request['description']) ? $request->input('description') : null,
                'enabled' 		=> isset($request['enabled']) ? $request->input('enabled') : null,
            ]);
            // dd($data);
            Cache::flush();
			DB::commit();
            if (!$data->exists) {
                return redirect()->route('brands.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
            return redirect()->route('brands.index')->with('success', 'Data Berhasil ditambahkan');
        } catch (\Exception $exc) {
            DB::rollback();
            return $exc;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = $this->decodeHash($id);
            $data = SysBrand::where('id', $id)->first();

            if (!$data->exists) {
                return redirect()->route('brands.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }

            return view('backoffice.utilities.brand.edit', compact('data'));
        } catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'image'          		=> 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);

			if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

			$data = SysBrand::where('id', $id)->first();
            if (!empty($request->file('image'))) {
                $data = SysBrand::where('id', $id)->first();
				if (\File::exists(public_path('backoffice/assets/images/brand/' . $data->image))) {
                    \File::delete(public_path('backoffice/assets/images/brand/' . $data->image));
				}
				$image = time() . '.' .$request->file('image')->getClientOriginalExtension();
				$destinationPath = public_path('backoffice/assets/images/brand/');
				$request->file('image')->move($destinationPath, $image);

                $request['image'] = $data->image;

			} else if (isset($request['image'])){
                $image = $request['image'];
            }else {
                $image = null;
            }

            $data->update([
                'image' 		=> $image,
                'description'   => isset($request['description']) ? $request->input('description') : null,
                'enabled' 		=> isset($request['enabled']) ? $request->input('enabled') : null,
            ]);
          
            Cache::flush();
			DB::commit();
            if (!$data->exists) {
                return redirect()->route('brands.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
            return redirect()->route('brands.index')->with('success', 'Data Berhasil ditambahkan');
           
        } catch (\Exception $exc) {
            DB::rollback();
            return $exc;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	public function activeNonActive($id, $status)
	{
        DB::beginTransaction();
       	try {
            $id = $this->decodeHash($id);
			$activeNonActive = SysBrand::where('id', $id)->update([
                'enabled' => $status,
            ]);
			//dd($activeNonActive);

            Cache::flush();
			DB::commit();
            if (!$activeNonActive) {
                return redirect()->route('brands.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }

			return redirect()->route('brands.index')->with('success', 'Data Berhasil diubah');
        } catch (\Exception $exc) {
            DB::rollback();
            return $exc;
        }
	}
}
