<?php

namespace App\Http\Controllers\Backoffice\MasterData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SysBankAccount;
use App\Models\SysBank;
use App\Utilities\AutoNumber;
use App\Utilities\HashId;
use Illuminate\Support\Facades\Validator;
use DB, Cache;

class SysBankAccountController extends Controller
{
    use AutoNumber, HashId;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
			$data = SysBankAccount::orderBy('created_at','DESC')->paginate($request->input('limit', 15));

			return view('backoffice.masterdata.bank_accounts.index', compact('data'));
		} catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bank = SysBank::where('enabled', 1)->get();
		return view('backoffice.masterdata.bank_accounts.create', compact('bank'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
				'account_name' => 'required',
                'bank_id' => 'required',
                'account_number' => 'required|unique:sys_bank_accounts',
			]);

			if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

            $data = SysBankAccount::create([
                'id' 			=> $request['id'] = $this->GenerateAutoNumber('sys_bank_accounts'),
                'account_name'  => isset($request['account_name']) ? $request->input('account_name') : null,
                'account_number'=> isset($request['account_number']) ? $request->input('account_number') : null,
                'bank_id'       => isset($request['bank_id']) ? $request->input('bank_id') : null,
                'enabled' 		=> isset($request['enabled']) ? $request->input('enabled') : null,
            ]);

            Cache::flush();
			DB::commit();
            if (!$data->exists) {
                return redirect()->route('bank_accounts.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
            return redirect()->route('bank_accounts.index')->with('success', 'Data Berhasil ditambahkan');

		 } catch (\Exception $exc) {
            DB::rollback();
            return $exc;
		 }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = $this->decodeHash($id);
            $data = SysBankAccount::where('id', $id)->first();
            $bank = SysBank::where('enabled', 1)->get();

            if (!$data->exists) {
                return redirect()->route('bank_accounts.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }

            return view('backoffice.masterdata.bank_accounts.edit', compact('data', 'bank'));
        } catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
			$data = SysBankAccount::where('id', $id)->first();
            $data->update([
                'account_name'  => isset($request['account_name']) ? $request->input('account_name') : null,
                'account_number'=> isset($request['account_number']) ? $request->input('account_number') : null,
                'bank_id'       => isset($request['bank_id'     ]) ? $request->input('bank_id'   ) : null,
                'enabled' 		=> isset($request['enabled']) ? $request->input('enabled') : null,
            ]);

            Cache::flush();
			DB::commit();

            if (!$data->exists) {
                return redirect()->route('bank_accounts.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
            return redirect()->route('bank_accounts.index')->with('success', 'Data Berhasil ditambahkan');
           
        } catch (\Exception $exc) {
            DB::rollback();
            return $exc;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	public function activeNonActive($id, $status)
	{
        DB::beginTransaction();
       	try {
            $id = $this->decodeHash($id);
			$activeNonActive = SysBankAccount::where('id', $id)->update([
                'enabled' => $status,
            ]);
			//dd($activeNonActive);

            Cache::flush();
			DB::commit();
            if (!$activeNonActive) {
                return redirect()->route('bank_accounts.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }

			return redirect()->route('bank_accounts.index')->with('success', 'Data Berhasil diubah');
        } catch (\Exception $exc) {
            DB::rollback();
            return $exc;
        }
	}
}
