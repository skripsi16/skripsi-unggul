<?php

namespace App\Http\Controllers\Backoffice\MasterData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SysBank;
use App\Utilities\AutoNumber;
use App\Utilities\HashId;
use Illuminate\Support\Facades\Validator;
use DB, Cache;

class SysBankController extends Controller
{
    use AutoNumber, HashId;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
			$data = SysBank::orderBy('created_at','DESC')->paginate($request->input('limit', 15));

			return view('backoffice.masterdata.bank.index', compact('data'));
		} catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('backoffice.masterdata.bank.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name'          		=> 'required',
            ]);

			if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

            if (!empty($request->file('image'))) {
				if (\File::exists(public_path('backoffice/assets/images/bank/' . $request->image))) {
                    \File::delete(public_path('backoffice/assets/images/bank/' . $request->image));
				}
				$image = time() . '.' .$request->file('image')->getClientOriginalExtension();
				$destinationPath = public_path('backoffice/assets/images/bank/');
				$request->file('image')->move($destinationPath, $image);

			}  
            
            if(!empty($request->file('image'))){
                $image = time() . '.' .$request->file('image')->getClientOriginalExtension();
            }else if (isset($request['image'])){
                $image = $request['image'];
            }else {
                $image = null;
            }

            $data = SysBank::create([
                'id' 			=> $request['id'] = $this->GenerateAutoNumber('sys_banks'),
                'image' 		=> $image,
                'name' 	    => isset($request['name']) ? $request->input('name') : null,
                'enabled' 		=> isset($request['enabled']) ? $request->input('enabled') : null,
            ]);
            // dd($data);
            Cache::flush();
			DB::commit();
            if (!$data->exists) {
                return redirect()->route('banks.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
            return redirect()->route('banks.index')->with('success', 'Data Berhasil ditambahkan');
        } catch (\Exception $exc) {
            DB::rollback();
            return $exc;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = $this->decodeHash($id);
            $data = SysBank::where('id', $id)->first();

            if (!$data->exists) {
                return redirect()->route('banks.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }

            return view('backoffice.masterdata.bank.edit', compact('data'));
        } catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
                'name'          		=> 'required',
            ]);

			if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

			$data = SysBank::where('id', $id)->first();
            if (!empty($request->file('image'))) {
                $data = SysBank::where('id', $id)->first();
                if (\File::exists(public_path('/backoffice/assets/images/bank/' . $data->image))) {
                    \File::delete(public_path('/backoffice/assets/images/bank/' . $data->image));
				}
				$image = time() . '.' .$request->file('image')->getClientOriginalExtension();
				$destinationPath = public_path('/backoffice/assets/images/bank/');
				$request->file('image')->move($destinationPath, $image);

			} else if (isset($request['image'])){
                $image = $request['image'];
            }else {
                $image = null;
            }

            if(empty($request->file('image'))) {
				$data = SysBank::where('id', $id)->first();
				$image = $data->image;
            }

            $data->update([
                'image' 		=> $image,
                'name' 	    => isset($request['name']) ? $request->input('name') : null,
                'enabled' 		=> isset($request['enabled']) ? $request->input('enabled') : null,
            ]);
          
            Cache::flush();
			DB::commit();
            if (!$data->exists) {
                return redirect()->route('banks.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
            return redirect()->route('banks.index')->with('success', 'Data Berhasil ditambahkan');
           
        } catch (\Exception $exc) {
            DB::rollback();
            return $exc;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	public function activeNonActive($id, $status)
	{
        DB::beginTransaction();
       	try {
            $id = $this->decodeHash($id);
			$activeNonActive = SysBank::where('id', $id)->update([
                'enabled' => $status,
            ]);
			//dd($activeNonActive);

            Cache::flush();
			DB::commit();
            if (!$activeNonActive) {
                return redirect()->route('banks.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }

			return redirect()->route('banks.index')->with('success', 'Data Berhasil diubah');
        } catch (\Exception $exc) {
            DB::rollback();
            return $exc;
        }
	}
}
