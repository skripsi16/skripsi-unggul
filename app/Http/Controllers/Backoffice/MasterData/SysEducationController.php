<?php

namespace App\Http\Controllers\Backoffice\MasterData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SysEducation;
use App\Utilities\AutoNumber;
use App\Utilities\HashId;
use Illuminate\Support\Facades\Validator;
use DB, Cache;

class SysEducationController extends Controller
{
    use AutoNumber, HashId;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
			$data = SysEducation::orderBy('created_at','DESC')->paginate($request->input('limit', 15));

			return view('backoffice.masterdata.education.index', compact('data'));
		} catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('backoffice.masterdata.education.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $validator = Validator::make($request->all(), [
				'name' => 'required',
                'description' => 'required',
			]);

			if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);

            $data = SysEducation::create([
                'id' 			=> $request['id'] = $this->GenerateAutoNumber('sys_educations'),
                'name' 			=> isset($request['name']) ? $request->input('name') : null,
                'description' 	=> isset($request['description']) ? $request->input('description') : null,
                'enabled' 		=> isset($request['enabled']) ? $request->input('enabled') : null,
            ]);

            Cache::flush();
			DB::commit();
            if (!$data->exists) {
                return redirect()->route('educations.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
            return redirect()->route('educations.index')->with('success', 'Data Berhasil ditambahkan');
		 } catch (\Exception $exc) {
            DB::rollback();
            return redirect()->route('educations.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
		 }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = $this->decodeHash($id);
            $data = SysEducation::where('id', $id)->first();

            if (!$data->exists) {
                return redirect()->route('educations.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }

            return view('backoffice.masterdata.education.edit', compact('data'));
        } catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
			$data = SysEducation::where('id', $id)->first();
            $data->update([
                'name' 			=> isset($request['name']) ? $request->input('name') : null,
                'description' 	=> isset($request['description']) ? $request->input('description') : null,
                'enabled' 		=> isset($request['enabled']) ? $request->input('enabled') : null,
            ]);
           
            Cache::flush();
			DB::commit();
            if (!$data->exists) {
                return redirect()->route('educations.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
            return redirect()->route('educations.index')->with('success', 'Data Berhasil ditambahkan');
        } catch (\Exception $exc) {
            DB::rollback();
             return $exc;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	public function activeNonActive($id, $status)
	{
        DB::beginTransaction();
       	try {
            $id = $this->decodeHash($id);
			$activeNonActive = SysEducation::where('id', $id)->update([
                'enabled' => $status,
            ]);
			// dd($activeNonActive);
           
            Cache::flush();
			DB::commit();

            if (!$activeNonActive) {
                return redirect()->route('educations.index')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }

			return redirect()->route('educations.index')->with('success', 'Data Berhasil diubah');
        } catch (\Exception $exc) {
            DB::rollback();
             return $exc;
        }
	}
}
