<?php

namespace App\Http\Controllers\Backoffice\DataRegister;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Register;
use App\Models\School;
use App\Utilities\AutoNumber;
use App\Utilities\HashId;
use Illuminate\Support\Facades\Validator;
use DB;
use Carbon\Carbon;

class RegisterController extends Controller
{
    use AutoNumber;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
			$data = Register::orderBy('created_at','DESC')->paginate($request->input('limit', 15));

			return view('backoffice.registrasi.index', compact('data'));
		} catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //
        } catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data = Register::where('santri_id', $id)->first();
            $school = School::where('santri_id', $id)->first();

            return view('backoffice.registrasi.show', compact('data', 'school'));
        } catch (\Exception $exc) {
            return $exc;
        }
    }

    public function approvalProccess(Request $request)
	{
       	try {
            $id = $request->id;
            $data = Register::where('id', $id)->first();
            $status = $request->input('status');

            if ($status == 'VERIFIED') {
                $approve =  Register::where('id', $id)
                            ->update([ 'status' => $status,
                                        'register_date' => Carbon::now()->format('Y-m-d H:i:s')]);
            }else{
                $approve =  Register::where('id', $id)
                            ->update([ 'status' => $status]);
            }
            
			return redirect()->route('data-registrasi.index');
        } catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
                // 

        } catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            // 
           
        } catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
