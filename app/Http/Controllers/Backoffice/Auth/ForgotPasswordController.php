<?php

namespace App\Http\Controllers\Backoffice\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin;
use App\Models\PasswordReset;
use App\Utilities\HashId;
use Carbon, DB, Cache;
use App\Mail\ForgotPassword;

class ForgotPasswordController extends Controller
{

	public function getEmail()
	{
       	try {
			$title = 'Forgot Password Page';
			return view('backoffice.auth.forgot_password', compact('title'));
        } catch (\Exception $exc) {
            return redirect()->back()->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
        }
	}

	public function activatedUser($token)
	{
		DB::beginTransaction();
		try {
			$checkToken = PasswordReset::where('token', $token)->first();
            
			if (empty($checkToken)) return redirect()->back()->with("error", "Token tidak ditemukan !");

			$verified = Admin::where('email', $checkToken->email)->update(['email_verified_at' => \Carbon\Carbon::now()]);
			// dd($verified);
			if (empty($verified)) {
				return redirect()->route('backoffice.login')->with('error', 'Terjadi kesalahan hubungi administrator');
			}

			if(isset($verified)) {
				PasswordReset::where('email', $checkToken->email)->delete();
			}

			Cache::flush();
			DB::commit();
			return redirect()->route('backoffice.login')->with('success', 'Admin Berhasil diaktivasi');
		} catch (\Exception $exc) {
			DB::rollback();
			return redirect()->route('backoffice.login')->with('error', 'Terjadi kesalahan hubungi administrator');
		}
	}

	public function sendEmail(Request $request)
	{
		DB::beginTransaction();
       	try {
			$checkEmail = Admin::where('email', $request->email)->first();
			if (empty($checkEmail)) {
				return redirect()->back()->with('error', 'Email Tidak Terdaftar');
			} elseif ($checkEmail->enabled != 1) {
				return redirect()->back()->with('error', 'Akun anda tidak aktif, pengajuan di reject / akun di blokir');
			} else {
				$request['name'] = $checkEmail->name;
				$token = Str::random(60);
				$storeToken = PasswordReset::create([
					'email' => isset($request['email']) ? $request->input('email') : null,
					'name'  => isset($request['name']) ? $request->input('name') : null,
           			'token' => $token,
				]);
				if(isset($storeToken)){
					$token = PasswordReset::where('email', $request->email)->first();
					Mail::to($request->email)->send(new ForgotPassword($token));
					return back()->with('success', 'Link Reset Email sudah dikirim');
				}
			}
			Cache::flush();
			DB::commit();
        } catch (\Exception $exc) {
			DB::rollback();
            return redirect()->back()->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
        }
	}

	public function editPassword($token)
	{
       	try {
			$title = 'Forgot Password Page';
			$data = PasswordReset::where('token', $token)->first();
			// dd($data);
			return view('backoffice.auth.reset_password', compact('title', 'data'));
        } catch (\Exception $exc) {
            return redirect()->back()->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
        }
	}

	public function updatePassword(Request $request)
	{
		DB::beginTransaction();
       	try {
			$validator = Validator::make($request->all(), [
				'password' => 'required|min:3',
				'confirm_password' => 'required|same:password',
			]);
			// $validator = $request->validate([
			// 	'password' => 'required|min:3',
			// 	'confirm_password' => 'required|same:password',
			// ]);
			if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);
			$checkToken = PasswordReset::where('token', $request->token)->first();

			if (empty($checkToken)) {
				return redirect()->back()->withInput()->with('error', 'Invalid token!');
			}
			// Update Password
			$update = Admin::where('email', $request->email)->first();
			$update->update([
				'password' => Hash::make($request['password'])
			]);

			if(isset($update)) {
				PasswordReset::where('email', $request->email)->delete();
			}

			Cache::flush();
			DB::commit();

			return redirect()->route('backoffice.login')->with('success', 'Password anda berhasil diubah!');
        } catch (\Exception $exc) {
			DB::rollback();
            return redirect()->back()->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
        }

	}

}