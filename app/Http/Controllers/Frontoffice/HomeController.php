<?php

namespace App\Http\Controllers\Frontoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\SysSlider;
use App\Models\SysFaqDescription;
use App\Models\SysProsedur;
use App\Models\SysBrand;
use App\Utilities\AutoNumber;
use App\Utilities\HashId;
use Carbon, DB, Response;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        try {
            $slider = SysSlider::where('enabled', 1)->get();
            $faqDescription = SysFaqDescription::where('enabled', 1)->get();
            $prosedur = SysProsedur::where('enabled', 1)->get();
            $brand = SysBrand::where('enabled', 1)->limit(4)->get();
            
            return view('frontoffice.index', compact('slider', 'faqDescription', 'prosedur', 'brand'));
        } catch (\Exception $exc) {
            return $exc;
        }
      
    }
}
