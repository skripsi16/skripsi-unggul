<?php

namespace App\Http\Controllers\Frontoffice\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use App\Models\User;
use App\Models\Student;
use App\Models\Parents;
use App\Models\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Utilities\AutoNumber;
use App\Utilities\HashId;
use Illuminate\Support\Str;
use App\Mail\ActivationRegisterEmail;
use Auth;
use Session;

class AuthController extends Controller
{
    use Autonumber, ThrottlesLogins, AuthenticatesUsers;
    protected $redirectTo = '/dashboard-user';

    public $maxAttempts = 3;
	public $decayMinutes = 1;

    public function __construct()
    {
        $this->middleware('guest:web')->except('logout');
    }

      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        try {
			return view('frontoffice.auth.login');
        } catch (\Exception $exc) {
            return $exc;
        }
    }

    /**
     * Login Process a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function LoginProcess(Request $request)
    {
        try {
            $validator = $this->validator($request);
            
			if ($validator->fails()) return redirect()->back()->withInput()->withErrors($validator);
            // dd($validator);
            
			//check if the user has too many login attempts.
			if (
				method_exists($this, 'hasTooManyLoginAttempts') &&
				$this->hasTooManyLoginAttempts($request)
			) {
				$this->fireLockoutEvent($request);

				return $this->sendLockoutResponse($request);
            }
            $checkUser = Student::where('email', $request->email)->first();
            // dd($checkUser);
			if($checkUser->enabled != 1){
                return redirect()->back()->with('error', 'Akun anda tidak aktif, pengajuan di reject / akun di blokir');
			}else{
                if ($this->attemptLogin($request)) {
                    return $this->sendLoginResponse($request);
                }
            }
    

			//keep track of login attempts from the user.
			$this->incrementLoginAttempts($request);
            //Authentication failed, redirect back with input.
			return $this->loginFailed();
        } catch (\Exception $exc) {
            return $exc;
        }
    }

      // Validator Login 
      private function validator(Request $request)
      {
          return Validator::make($request->all(), [
                  $this->username() => 'required|string|exists:students',
                  'password' => 'required|string|',
              ], [
                  'required' => ':attribute wajib diisi',
                  $this->username() . '.exists' => 'User tidak terdaftar!',
              ]);
      }
      
      protected function attemptLogin(Request $request)
      {
          return $this->guard()->attempt(
              $this->credentials($request),
              $request->filled('remember')
          );
      }
  
  
      protected function credentials(Request $request)
      {
          return $request->only($this->username(), 'password');
      }
  
      protected function sendLoginResponse(Request $request)
      {
          $request->session()->regenerate();
  
          $this->clearLoginAttempts($request);
  
          return $this->authenticated($request, $this->guard()->user())
              ?: redirect()->intended($this->redirectPath());
      }
  
      protected function authenticated(Request $request, $user)
      {
          //
      }
  
      private function loginFailed()
      {
          return redirect()
              ->back()
              ->withInput()
              ->with('error', 'Login gagal, email atau password salah!');
      }
      
      //Username used in ThrottlesLogins trait
      public function username()
      {
          return 'email';
      }
  
    protected function guard()
	{
        return Auth::guard('web');
    }
    
    public function logout(Request $request)
	{
		$this->guard()->logout();

        $this->flushSession('users');

		if ($response = $this->loggedOut($request)) {
			return $response;
		}

		return redirect()->route('auth-user.login')->with('status', 'Anda telah keluar!');
	}
  
      protected function loggedOut(Request $request)
      {
          //
      }

    protected function flushSession($sessionPrefix)
      {
          $sessionPrefix = $sessionPrefix ? 'login_' . $sessionPrefix : 'login_' . 'web';
  
          $content = [];
          foreach (session()->all() as $key => $value) {
              if (preg_match("/$sessionPrefix/", $key)) {
                  session()->forget($key);
              }
          }
  
          return true;
      }
    
}
