<?php

namespace App\Http\Controllers\Frontoffice\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use App\Models\Student;
use App\Models\School;
use App\Models\SysPacketEducation;
use App\Models\SysEducation;
use App\Models\SysSalary;
use App\Models\SysJob;
use App\Models\Parents;
use App\Models\SysCountry;
use App\Models\SysProvince;
use App\Models\SysCity;
use App\Models\SysDistrict;
use App\Models\SysArea;
use App\Models\Register;
use App\Models\SysCategory;
use App\Models\SysBankAccount;
use App\Models\SysTerm;
use Carbon\Carbon;
use App\Models\PasswordReset;
use App\Models\SysCategoryPacket;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Utilities\AutoNumber;
use Illuminate\Support\Str;
use App\Mail\ActivationRegisterEmail;
use Auth, DB, Cache;

class RegisterController extends Controller
{
    use Autonumber;

    public function showRegisterForm()
    {
        $country = SysCountry::all();
        $province = SysProvince::all();
        $city = SysCity::all();
        $education = SysEducation::where('enabled', 1)->get();
        $categories = SysCategory::where('enabled', 1)->get();
        $salary = SysSalary::where('enabled', 1)->get();
        $job = SysJob::where('enabled', 1)->get();
        $packet = SysPacketEducation::where('enabled', 1)->get();
        $category_packet = SysCategoryPacket::where('enabled', 1)->get();
        $term = SysTerm::where('enabled', 1)->get();
        $district = NULL;
        $area = NULL;
        $bank = SysBankAccount::where('enabled', 1)->get();

        return view('frontoffice.auth.register', compact('term', 'country', 'province', 'city', 'district', 'area', 'education', 'job', 'salary', 'packet', 'category_packet', 'categories', 'bank'));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        DB::beginTransaction();   
        try {
            $id = $this->GenerateAutoNumber('students');

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|unique:students',
                'password' => 'required',
                'nik' => 'required|unique:students',
                'nisn' => 'required|unique:students',
                'phone_number' => 'required|unique:students',
                'gender' => 'required',
                'birth_date' => 'required',
                'birth_place' => 'required',
                'religion' => 'required',
                'country_id' => 'required',
                'province_id' => 'required',
                'city_id' => 'required',
                'district_id' => 'required',
                'area_id' => 'required',
                'address' => 'required',
            ]);
    
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            if (!empty($request->file('image'))) {
				if (\File::exists(public_path('backoffice/assets/images/students/' . $request->image))) {
                    \File::delete(public_path('backoffice/assets/images/students/' . $request->image));
				}
				$image = time() . '.' .$request->file('image')->getClientOriginalExtension();
				$destinationPath = public_path('backoffice/assets/images/students/');
				$request->file('image')->move($destinationPath, $image);

			}  
            
            if(!empty($request->file('image'))){
                $image = time() . '.' .$request->file('image')->getClientOriginalExtension();
            }else if (isset($request['image'])){
                $image = $request['image'];
            }else {
                $image = null;
            }

            $data = Student::create([
                'id' => $id,
                'name' 	        => isset($request['name']) ? $request->input('name') : null,
                'nisn' 	=> isset($request['nisn']) ? $request->input('nisn') : null,
                'nik' 	=> isset($request['nik']) ? $request->input('nik') : null,
                'phone_number' 	=> isset($request['phone_number']) ? $request->input('phone_number') : null,
                'gender' 	=> isset($request['gender']) ? $request->input('gender') : null,
                'email' 	=> isset($request['email']) ? $request->input('email') : null,
                'password' => Hash::make($request['password']),
                'birth_place' 	=> isset($request['birth_place']) ? $request->input('birth_place') : null,
                'birth_date' 	=> isset($request['birth_date']) ? $request->input('birth_date') : null,
                'religion' 	=> isset($request['religion']) ? $request->input('religion') : null,
                'status_of_family' 	=> isset($request['status_of_family']) ? $request->input('status_of_family') : null,
                'country_id' 		=> isset($request['country_id']) ? $request->input('country_id') : null,
                'province_id' 		=> isset($request['province_id']) ? $request->input('province_id') : null,
                'city_id' 	=> isset($request['city_id']) ? $request->input('city_id') : null,
                'district_id' 		=> isset($request['district_id']) ? $request->input('district_id') : null,
                'area_id' 		=> isset($request['area_id']) ? $request->input('area_id') : null,
                'address' 		=> isset($request['address']) ? $request->input('address') : null,
                'image' 		=> $image,
                'enabled'    => 0,
            ]);

            if($data) {
                $parent = Parents::create([
                    'id' 			=> $this->GenerateAutoNumber('parents'),
                    'santri_id' 		=> $id,
                    'name_father'   => isset($request['name_father']) ? $request->input('name_father') : null,
                    'name_mother'   => isset($request['name_mother']) ? $request->input('name_mother') : null,
                    'name_guardian'   => isset($request['name_guardian']) ? $request->input('name_guardian') : null,
                    'father_jobs'   => isset($request['father_jobs']) ? $request->input('father_jobs') : null,
                    'mother_jobs'   => isset($request['mother_jobs']) ? $request->input('mother_jobs') : null,
                    'guardian_jobs'   => isset($request['guardian_jobs']) ? $request->input('guardian_jobs') : null,
                    'father_education'   => isset($request['father_education']) ? $request->input('father_education') : null,
                    'mother_education'   => isset($request['mother_education']) ? $request->input('mother_education') : null,
                    'guardian_education'   => isset($request['guardian_education']) ? $request->input('guardian_education') : null,
                    'father_income'   => isset($request['father_income']) ? $request->input('father_income') : null,
                    'mother_income'   => isset($request['mother_income']) ? $request->input('mother_income') : null,
                    'guardian_income'   => isset($request['guardian_income']) ? $request->input('guardian_income') : null,
                    'father_phone_number'   => isset($request['father_phone_number']) ? $request->input('father_phone_number') : null,
                    'mother_phone_number'   => isset($request['mother_phone_number']) ? $request->input('mother_phone_number') : null,
                    'guardian_phone_number'   => isset($request['guardian_phone_number']) ? $request->input('guardian_phone_number') : null,
                ]);

                $school = School::create([
                    'id' 			=> $this->GenerateAutoNumber('schools'),
                    'santri_id' 		=> $id,
                    'npsn'          => isset($request['npsn']) ? $request->input('npsn') : null,
                    'school_name'    => isset($request['school_name']) ? $request->input('school_name') : null,
                    'status_of_school'    => isset($request['status_of_school']) ? $request->input('status_of_school') : null,
                    'graduation_year'    => isset($request['graduation_year']) ? $request->input('graduation_year') : null,
                    'exam' 		=> isset($request['exam']) ? $request->input('exam') : null,
                    'country_id_school' 	=> isset($request['country_id_school']) ? $request->input('country_id_school') : null,
                    'province_id_school' 		=> isset($request['province_id_school']) ? $request->input('province_id_school') : null,
                    'city_id_school' 	=> isset($request['city_id_school']) ? $request->input('city_id_school') : null,
                    'district_id_school' 		=> isset($request['district_id_school']) ? $request->input('district_id_school') : null,
                    'area_id_school' 		=> isset($request['area_id_school']) ? $request->input('area_id_school') : null,
                    'address_of_school' 		=> isset($request['address_of_school']) ? $request->input('address_of_school') : null,
                ]);
                $no_register = 'REG'. '-' . $id;

                $register = Register::create([
                    'id' 			=> $this->GenerateAutoNumber('registers'),
                    'santri_id'    => $id,
                    'packet_id' 	=> isset($request['packet_id']) ? $request->input('packet_id') : null,
                    'branch' 	=> isset($request['branch']) ? $request->input('branch') : null,
                    'year' 	=> isset($request['year']) ? $request->input('year') : null,
                    'category' 	=> isset($request['category']) ? $request->input('category') : null,
                    'financing' 	=> isset($request['financing']) ? $request->input('financing') : null,
                    'payment' 	            => isset($request['payment']) ? str_replace('.', '', str_replace('Rp ', '', $request->input('payment'))) : null,
                    'register_date'          => Carbon::now(),
                    'no_register'   => $no_register,
                    'status'   => 'UNVERIFIED',
                    'bank_id' 	=> isset($request['bank_id']) ? $request->input('bank_id') : null,
                ]);
            } 
            $token = Str::random(60);
            $storeToken = PasswordReset::create([
                'email' 		=> isset($request['email']) ? $request->input('email') : null,
                'name'			=> isset($request['name']) ? $request->input('name') : null,
                'token' 		=> $token,
            ]);
                if(isset($storeToken)){
                    $token = PasswordReset::where('email', $request->email)->first();
                    Mail::to($request->email)->send(new ActivationRegisterEmail($token));
                }

            Cache::flush();
            DB::commit();
            return redirect()->route('register-sukses', $no_register);
         } catch (\Exception $exc) {
            DB::rollback();
            return redirect()->back()->with('error', 'Harap cek data terlebih dahulu!');
        }
    }

    public function getCities($id){
        try{
            $data = SysCity::where('province_id', '=', $id)
                            ->orderBy('name','ASC')
                            ->get();
            return response()->json($data);
        }catch(\Exception $exc){
            return $exc;
        }

    }

    public function getDistricts($id){
        try{
            $data = SysDistrict::where('city_id', '=', $id)
                            ->orderBy('name','ASC')
                            ->get();
            return response()->json($data);
        }catch(\Exception $exc){
            return $exc;
        }

    }

    public function getAreas($id){
        try{
            $data = SysArea::where('district_id', '=', $id)
                            ->orderBy('name','ASC')
                            ->get();
        
            return response()->json($data);
        }catch(\Exception $exc){
            return $exc;
        }
    }

    public function invoice($id)
    {
        try {
            $data = Register::where('no_register', $id)->first();
            // dd($data);
            return view('frontoffice.auth.success-register', compact('data'));
        } catch (\Exception $exc) {
            return $exc;
        }
      
    }
}
