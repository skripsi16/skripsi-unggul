<?php

namespace App\Http\Controllers\Frontoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Utilities\AutoNumber;
use Carbon, DB, Response;

class UserController extends Controller
{
    use Autonumber;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
			$data = User::all();

            return $data;
        } catch (\Exception $exc) {
            
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       try {
           return view('frontoffice.auth.register');
       } catch (\Exception $exc) {
           return $exc;
       }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'nisn' => ['required', 'number', 'max:255', 'unique:users'],
                'nik' => ['required', 'number', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8'],
			]);
    
            $data = User::create([
                'id' 	=> $this->GenerateAutoNumber('users'),
                'name' => isset($request['name']) ? $request->input('name') : null,
                'email' => isset($request['email']) ? $request->input('email') : null,
                'password' => Hash::make($request['password']),
                'enabled' => 0,
            ]);
           
            // $data = User::create([
            //     'id' 			=> $id,
            //     'name' 		    => isset($request['name']) ? $request->input('name') : null,
            //     'nik' 	=> isset($request['nik']) ? $request->input('nik') : null,
            //     'nisn' 		=> isset($request['nisn']) ? $request->input('nisn') : null,
            //     'gender' 	=> isset($request['gender']) ? $request->input('gender') : null,
            //     'birth_place' 		=> isset($request['birth_place']) ? $request->input('birth_place') : null,
            //     'birth_date' 	=> isset($request['birth_date']) ? $request->input('birth_date') : null,
            //     'religion' 		=> isset($request['religion']) ? $request->input('religion') : null,
            //     'status_of_family' 	=> isset($request['status_of_family']) ? $request->input('status_of_family') : null,
            //     'phone_number' 	=> isset($request['phone_number']) ? $request->input('phone_number') : null,
            //     'email' 		=> isset($request['email']) ? $request->input('email') : null,
            //     'country_id' 	=> isset($request['country_id']) ? $request->input('country_id') : null,
            //     'province_id' 		=> isset($request['province_id']) ? $request->input('province_id') : null,
            //     'city_id' 	=> isset($request['city_id']) ? $request->input('city_id') : null,
            //     'district_id' 		=> isset($request['district_id']) ? $request->input('district_id') : null,
            //     'area_id' 		=> isset($request['area_id']) ? $request->input('area_id') : null,
            //     'address' 		=> isset($request['address']) ? $request->input('address') : null,
            // ]);

            // if($data) {
            //     $parents = Parents::create([
            //         'id' 			=> $this->GenerateAutoNumber('parents'),
            //         'user_id' 		=> $id,
            //         'name_father'   => isset($request['name_father']) ? $request->input('name_father') : null,
            //         'name_mother'   => isset($request['name_mother']) ? $request->input('name_mother') : null,
            //         'name_guardian'   => isset($request['name_guardian']) ? $request->input('name_guardian') : null,
            //         'father_jobs'   => isset($request['father_jobs']) ? $request->input('father_jobs') : null,
            //         'mother_jobs'   => isset($request['mother_jobs']) ? $request->input('mother_jobs') : null,
            //         'guardian_jobs'   => isset($request['guardian_jobs']) ? $request->input('guardian_jobs') : null,
            //         'father_education'   => isset($request['father_education']) ? $request->input('father_education') : null,
            //         'mother_education'   => isset($request['mother_education']) ? $request->input('mother_education') : null,
            //         'guardian_education'   => isset($request['guardian_education']) ? $request->input('guardian_education') : null,
            //         'father_income'   => isset($request['father_income']) ? $request->input('father_income') : null,
            //         'mother_income'   => isset($request['mother_income']) ? $request->input('mother_income') : null,
            //         'guardian_income'   => isset($request['guardian_income']) ? $request->input('guardian_income') : null,
            //         'father_phone_number'   => isset($request['father_phone_number']) ? $request->input('father_phone_number') : null,
            //         'mother_phone_number'   => isset($request['mother_phone_number']) ? $request->input('mother_phone_number') : null,
            //         'guardian_phone_number'   => isset($request['guardian_phone_number']) ? $request->input('guardian_phone_number') : null,
            //     ]);

            //     $school = School::create([
            //         'id' 			=> $this->GenerateAutoNumber('parents'),
            //         'user_id' 		=> $id,
            //         'npsn'          => isset($request['npsn']) ? $request->input('npsn') : null,
            //         'school_name'    => isset($request['school_name']) ? $request->input('school_name') : null,
            //         'status_of_school'    => isset($request['status_of_school']) ? $request->input('status_of_school') : null,
            //         'graduation_year'    => isset($request['graduation_year']) ? $request->input('graduation_year') : null,
            //         'country_id' 	=> isset($request['country_id']) ? $request->input('country_id') : null,
            //         'province_id' 		=> isset($request['province_id']) ? $request->input('province_id') : null,
            //         'city_id' 	=> isset($request['city_id']) ? $request->input('city_id') : null,
            //         'district_id' 		=> isset($request['district_id']) ? $request->input('district_id') : null,
            //         'area_id' 		=> isset($request['area_id']) ? $request->input('area_id') : null,
            //         'address' 		=> isset($request['address']) ? $request->input('address') : null,
            //     ]);
            // }

            if ($data->errorInfo != null) {
                return response()->json([
                                            'response' => $data,
                                            'error' => 'gagal'
                                        ]);
		 	}
			return response()->json([
				'response' => $data,
				'success' => 'berhasil'

			]);
        } catch (\Exception $exc) {
            return $exc;
        }
    }


    /**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		try {
			// 
        } catch (\Exception $exc) {
            
        }
	}

    public function edit($id)
	{
		try {
			//
        } catch (\Exception $exc) {
            
        }
	}

    public function update(Request $request, $id)
	{
		try {
            //
		} catch (\Exception $exc) {
			
		}
	}

}
