<?php

namespace App\Http\Controllers\Frontoffice\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Utilities\AutoNumber;
use App\Utilities\HashId;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\SysCountry;
use App\Models\SysProvince;
use App\Models\SysCity;
use App\Models\SysDistrict;
use App\Models\Parents;
use App\Models\SysArea;
use App\Models\Register;
use Auth, PDF;

class StudentController extends Controller
{
    use AutoNumber;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            // getUnicode
            $data = Student::first();
            $country = SysCountry::all();
            $province = SysProvince::all();
            $city = SysCity::all();
            $register = Register::first();
            $district = NULL;
            $area = NULL;
            $activeMenu = "pribadi";

            return view('frontoffice.user.student', compact('data','country', 'province', 'city', 'district', 'area', 'activeMenu', 'register'));
        } catch (\Exception $exc) {
            return $exc;
        }
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
               
			]);
            $parentID = Parents::where('user_id', Auth::guard('web')->user()->id)->first();
            $data = Student::create([
                'id' 			=> $this->GenerateAutoNumber('students'),
                'user_id' 			=> Auth::guard('web')->user()->id,
                'parent_id'    => $parentID->id,
                'name' 	        => isset($request['name']) ? $request->input('name') : null,
                'nisn' 	=> isset($request['nisn']) ? $request->input('nisn') : null,
                'nik' 	=> isset($request['nik']) ? $request->input('nik') : null,
                'phone_number' 	=> isset($request['phone_number']) ? $request->input('phone_number') : null,
                'gender' 	=> isset($request['gender']) ? $request->input('gender') : null,
                'email' 	=> isset($request['email']) ? $request->input('email') : null,
                'birth_place' 	=> isset($request['birth_place']) ? $request->input('birth_place') : null,
                'birth_date' 	=> isset($request['birth_date']) ? $request->input('birth_date') : null,
                'religion' 	=> isset($request['religion']) ? $request->input('religion') : null,
                'status_of_family' 	=> isset($request['status_of_family']) ? $request->input('status_of_family') : null,
                'npsn' 	=> isset($request['npsn']) ? $request->input('npsn') : null,
                'name_of_school' 	=> isset($request['name_of_school']) ? $request->input('name_of_school') : null,
                'status_of_school' 	=> isset($request['status_of_school']) ? $request->input('status_of_school') : null,
                'address_of_school' 	=> isset($request['address_of_school']) ? $request->input('address_of_school') : null,
                'country_id' 		=> isset($request['country_id']) ? $request->input('country_id') : null,
                'province_id' 		=> isset($request['province_id']) ? $request->input('province_id') : null,
                'city_id' 	=> isset($request['city_id']) ? $request->input('city_id') : null,
                'district_id' 		=> isset($request['district_id']) ? $request->input('district_id') : null,
                'area_id' 		=> isset($request['area_id']) ? $request->input('area_id') : null,
                'address' 		=> isset($request['address']) ? $request->input('address') : null,
                'exam'          => isset($request['exam']) ? $request->input('exam') : null,
                'graduation_year'    => isset($request['graduation_year']) ? $request->input('graduation_year') : null,
                'status'    => isset($request['status']) ? $request->input('status') : null,
            ]);

            if ($data->errorInfo != null) {
                return redirect()->back()->with('error', 'Terjadi Kesalahan, silahkan coba lagi!');
		 	} else {
                return redirect()->back()->with('success', 'Terjadi Kesalahan, silahkan coba lagi!');
            }
			
        } catch (\Exception $exc) {
            return $exc;
        }
    }

     /**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		try {
			// 
        } catch (\Exception $exc) {
            
        }
	}

    public function edit($id)
	{
		try {
			//
        } catch (\Exception $exc) {
            
        }
	}

    public function update(Request $request, $id)
	{
		try {
            $data = Student::where('id', $id)->first();
            $data->update([
                'name' 	        => isset($request['name']) ? $request->input('name') : null,
                'nisn' 	=> isset($request['nisn']) ? $request->input('nisn') : null,
                'nik' 	=> isset($request['nik']) ? $request->input('nik') : null,
                'phone_number' 	=> isset($request['phone_number']) ? $request->input('phone_number') : null,
                'gender' 	=> isset($request['gender']) ? $request->input('gender') : null,
                'email' 	=> isset($request['email']) ? $request->input('email') : null,
                'birth_place' 	=> isset($request['birth_place']) ? $request->input('birth_place') : null,
                'birth_date' 	=> isset($request['birth_date']) ? $request->input('birth_date') : null,
                'religion' 	=> isset($request['religion']) ? $request->input('religion') : null,
                'status_of_family' 	=> isset($request['status_of_family']) ? $request->input('status_of_family') : null,
                'npsn' 	=> isset($request['npsn']) ? $request->input('npsn') : null,
                'name_of_school' 	=> isset($request['name_of_school']) ? $request->input('name_of_school') : null,
                'status_of_school' 	=> isset($request['status_of_school']) ? $request->input('status_of_school') : null,
                'address_of_school' 	=> isset($request['address_of_school']) ? $request->input('address_of_school') : null,
                'country_id' 		=> isset($request['country_id']) ? $request->input('country_id') : null,
                'province_id' 		=> isset($request['province_id']) ? $request->input('province_id') : null,
                'city_id' 	=> isset($request['city_id']) ? $request->input('city_id') : null,
                'district_id' 		=> isset($request['district_id']) ? $request->input('district_id') : null,
                'area_id' 		=> isset($request['area_id']) ? $request->input('area_id') : null,
                'address' 		=> isset($request['address']) ? $request->input('address') : null,
                'exam'          => isset($request['exam']) ? $request->input('exam') : null,
                'graduation_year'    => isset($request['graduation_year']) ? $request->input('graduation_year') : null,
                'status'    => 'PENDING',
            ]);

            if ($data->errorInfo != null) {
                return redirect()->back()->with('error', 'Terjadi Kesalahan, silahkan coba lagi!');
		 	} else {
                return redirect()->back()->with('success', 'Terjadi Kesalahan, silahkan coba lagi!');
            }
            
		} catch (\Exception $exc) {
			return $exc;
		}
	}

    public function getCities($id){
        try{
            $data = SysCity::where('province_id', '=', $id)
                            ->orderBy('name','ASC')
                            ->get();
            return response()->json($data);
        }catch(\Exception $exc){
            return $exc;
        }

    }

    public function getDistricts($id){
        try{
            $data = SysDistrict::where('city_id', '=', $id)
                            ->orderBy('name','ASC')
                            ->get();
            return response()->json($data);
        }catch(\Exception $exc){
            return $exc;
        }

    }

    public function getAreas($id){
        try{
            $data = SysArea::where('district_id', '=', $id)
                            ->orderBy('name','ASC')
                            ->get();
        
            return response()->json($data);
        }catch(\Exception $exc){
            return $exc;
        }
    }

    public function approvalProccess(Request $request)
	{
       	try {
            $data = Student::where('user_id', Auth::guard('web')->user()->id)->first();
            $dataStatus = $request->input('status');
            $updateTransaction = $data->update([
               'status' => $dataStatus,
            ]);
            
            if (!$updateTransaction) {
                return redirect()->route('dashboard-user')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
            
            // dd($activeNonActive);
            if ($request->input('status') == "VERIFIED") {
                return redirect()->route('dashboard-user')->with('error', 'Berhasil Menyimpan Data');
               
            } else {
                return redirect()->route('dashboard-user')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
            
			return redirect()->route('dashboard-user');
        } catch (\Exception $exc) {
            return $exc;
        }
    }


}
