<?php

namespace App\Http\Controllers\Frontoffice\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Utilities\AutoNumber;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Student;
use App\Models\School;
use App\Models\SysEducation;
use App\Models\SysJob;
use App\Models\Parents;
use App\Models\SysSalary;
use App\Models\Register;
use App\Utilities\HashId;
use Auth, PDF;

class DashboardController extends Controller
{
    use AutoNumber;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            // getUnicode
            $data = Student::where('id', Auth::guard('web')->user()->id)->first();
            $parent = Parents::where('santri_id', Auth::guard('web')->user()->id)
                            ->first();
            $school = School::where('santri_id', Auth::guard('web')->user()->id)
                            ->first();
            $register = Register::where('santri_id', Auth::guard('web')->user()->id)->first();
            return view('frontoffice.user.dashboard', compact('data', 'school', 'parent', 'register'));
        } catch (\Exception $exc) {
            return $exc;
		}
       
       
    }

    public function store(Request $request)
    {
        try {
            $id = $this->GenerateAutoNumber('registers');
            $parent = Parents::where('status', 'VERIFIED')->first();
            $student = Student::where('status', 'VERIFIED')->first();

            $no_register = 'REG'. '-' . $id;

            $data = Register::create([
                'id' 			=> $id,
                'user_id' 		=> Auth::guard('web')->user()->id,
                'student_id'    => $student->id,
                'parent_id'    => $parent->id,
                'date'          => Carbon::now(),
                'no_register'   => $no_register,
            ]);

            if ($data != null) {
                return redirect()->back()->with('error', 'Terjadi Kesalahan, silahkan coba lagi!');
		 	} else {
                return redirect()->back()->with('success', 'Berhasil Mendaftar!');
            }
			
        } catch (\Exception $exc) {
            return $exc;
        }
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function student()
    {
        try {
            // getUnicode
            $data = Parents::where('user_id', Auth::guard('web')->user()->id)->first();
            $job = SysJob::where('enabled', 1)->get();
            $education = SysEducation::where('enabled', 1)->get();
            $salary = SysSalary::where('enabled', 1)->get();
            $activeMenu = "orangtua";

            return view('frontoffice.user.parent', compact('data', 'activeMenu', 'job', 'education', 'salary'));
        } catch (\Exception $exc) {
            return $exc;
		}
       
    }

}
