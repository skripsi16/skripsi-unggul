<?php

namespace App\Http\Controllers\Frontoffice\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Utilities\AutoNumber;
use App\Utilities\HashId;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\SysEducation;
use App\Models\SysJob;
use App\Models\Parents;
use App\Models\SysSalary;
use App\Models\Register;
use Auth, PDF;

class ParentController extends Controller
{
    use AutoNumber;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            // getUnicode
            $data = Parents::where('user_id', Auth::guard('web')->user()->id)->first();
            $job = SysJob::where('enabled', 1)->get();
            $education = SysEducation::where('enabled', 1)->get();
            $salary = SysSalary::where('enabled', 1)->get();
            $register = Register::first();
            $activeMenu = "orangtua";

            return view('frontoffice.user.parent', compact('data', 'activeMenu', 'job', 'education', 'salary', 'register'));
        } catch (\Exception $exc) {
            return $exc;
        }
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // 
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name_father' => ['required', 'string', 'max:255'],
                'name_mother' => ['required', 'string', 'max:255'],
                'father_jobs' => ['required', 'string', 'max:255'],
                'mother_jobs' => ['required', 'string', 'max:255'],
                'father_education' => ['required', 'string', 'max:255'],
                'mother_education' => ['required', 'string', 'max:255'],
                'father_income' => ['required', 'string', 'max:255'],
                'mother_income' => ['required', 'string', 'max:255'],
                'father_phone_number' => ['required', 'string', 'max:255'],
                'mother_phone_number' => ['required', 'string', 'max:255'],
			]);
    
            $data = Parents::create([
                'id' 			=> $this->GenerateAutoNumber('parents'),
                'santri_id' 		=> $id,
                'name_father'   => isset($request['name_father']) ? $request->input('name_father') : null,
                'name_mother'   => isset($request['name_mother']) ? $request->input('name_mother') : null,
                'name_guardian'   => isset($request['name_guardian']) ? $request->input('name_guardian') : null,
                'father_jobs'   => isset($request['father_jobs']) ? $request->input('father_jobs') : null,
                'mother_jobs'   => isset($request['mother_jobs']) ? $request->input('mother_jobs') : null,
                'guardian_jobs'   => isset($request['guardian_jobs']) ? $request->input('guardian_jobs') : null,
                'father_education'   => isset($request['father_education']) ? $request->input('father_education') : null,
                'mother_education'   => isset($request['mother_education']) ? $request->input('mother_education') : null,
                'guardian_education'   => isset($request['guardian_education']) ? $request->input('guardian_education') : null,
                'father_income'   => isset($request['father_income']) ? $request->input('father_income') : null,
                'mother_income'   => isset($request['mother_income']) ? $request->input('mother_income') : null,
                'guardian_income'   => isset($request['guardian_income']) ? $request->input('guardian_income') : null,
                'father_phone_number'   => isset($request['father_phone_number']) ? $request->input('father_phone_number') : null,
                'mother_phone_number'   => isset($request['mother_phone_number']) ? $request->input('mother_phone_number') : null,
                'guardian_phone_number'   => isset($request['guardian_phone_number']) ? $request->input('guardian_phone_number') : null,
            ]);

            if ($data->errorInfo != null) {
                return redirect()->back()->with('error', 'Terjadi Kesalahan, silahkan coba lagi!');
		 	} else {
                return redirect()->back()->with('success', 'Terjadi Kesalahan, silahkan coba lagi!');
            }
			
        } catch (\Exception $exc) {
            return $exc;
        }
    }

     /**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		try {
			// 
        } catch (\Exception $exc) {
            
        }
	}

    public function edit($id)
	{
		try {
			//
        } catch (\Exception $exc) {
            
        }
	}

    public function update(Request $request, $id)
	{
		try {
            //
		} catch (\Exception $exc) {
			
		}
	}


    public function approvalProccess(Request $request)
	{
       	try {
            $data = Parents::where('user_id', Auth::guard('web')->user()->id)->first();
            $dataStatus = $request->input('status');
            $updateTransaction = $data->update([
               'status' => $dataStatus,
            ]);
            
            if (!$updateTransaction) {
                return redirect()->route('dashboard-user')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
            
            // dd($activeNonActive);
            if ($request->input('status') == "VERIFIED") {
                return redirect()->route('dashboard-user')->with('error', 'Berhasil Menyimpan Data');
               
            } else {
                return redirect()->route('dashboard-user')->with('error', 'Terjadi kesalahan saat input coba ulangi lagi');
            }
            
			return redirect()->route('dashboard-user');
        } catch (\Exception $exc) {
            return $exc;
        }
    }

}
