<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class SysJob extends Model
{
    use HasFactory, HashId;

	protected $table = 'sys_jobs';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'name', 'category', 'description', 'enabled'
	];
}
