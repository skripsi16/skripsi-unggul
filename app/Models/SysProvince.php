<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Utilities\HashId;

class SysProvince extends Authenticatable
{
    use HasFactory, Notifiable, HashId;

	protected $table = 'sys_provinces';
	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'name', 'enabled', 'country_id'
	];

	public function country()
    {
        return $this->belongsTo(SysCountry::class, 'country_id', 'id');
    }

    public function cities()
    {
        return $this->hasMany(SysCity::class, 'city_id', 'id');
    }



}
