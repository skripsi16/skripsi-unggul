<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class SysEducation extends Model
{
    use HasFactory, HashId;

	protected $table = 'sys_educations';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'name', 'description', 'enabled'
	];
}
