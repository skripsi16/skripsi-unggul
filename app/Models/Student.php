<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Utilities\HashId;

class Student extends Authenticatable
{
    use HasFactory, HashId;

	protected $table = 'students';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'nisn', 'nik', 'name', 'phone_number', 'email', 'password', 'gender',
        'birth_place', 'birth_date', 'religion', 'status_of_family', 'country_id', 'province_id', 'city_id', 'district_id', 'area_id', 
        'address', 'enabled', 'image',

	];

	public function country()
    {
        return $this->belongsTo(SysCountry::class);
    }

    public function province()
    {
        return $this->belongsTo(SysProvince::class);
    }

    public function city()
    {
        return $this->belongsTo(SysCity::class);
    }

    public function district()
    {
        return $this->belongsTo(SysDistrict::class);
    }

    public function area()
    {
        return $this->belongsTo(SysArea::class);
    }
}
