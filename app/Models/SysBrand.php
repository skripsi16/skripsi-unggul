<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class SysBrand extends Model
{
    use HasFactory, HashId;

	protected $table = 'sys_brands';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'enabled', 'description', 'image'
	];
}
