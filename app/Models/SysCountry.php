<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Utilities\HashId;

class SysCountry extends Authenticatable
{
    use HasFactory, Notifiable, HashId;

	protected $table = 'sys_countries';
	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'name', 'enabled'
	];

	public function provinces()
    {
        return $this->hasMany(SysProvince::class, 'province_id', 'id');
    }


}
