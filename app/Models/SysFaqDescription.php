<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class SysFaqDescription extends Model
{
    use HasFactory, HashId;

	protected $table = 'sys_faq_descriptions';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'faq_categories_id', 'answer', 'enabled'
	];

    public function hasCategory()
	{
		return $this->belongsto(SysFaqCategory::class, 'faq_categories_id', 'id');
	}
}
