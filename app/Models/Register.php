<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class Register extends Model
{
    use HasFactory, HashId;

	protected $table = 'registers';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'santri_id', 'packet_id', 'no_register', 'date', 'payment', 'register_date', 'branch', 'year', 'category', 'financing', 'bank_id', 'status'
	];

	public function hasStudent()
    {
        return $this->belongsTo(Student::class, 'santri_id', 'id');
    }

	public function hasBranch()
    {
        return $this->belongsTo(SysCity::class, 'branch', 'id');
    }

	public function hasCategory()
    {
        return $this->belongsTo(SysCategoryPacket::class, 'category', 'id');
    }

	public function hasPacket()
    {
        return $this->belongsTo(SysPacketEducation::class, 'packet_id', 'id');
    }

    public function hasBankAccount()
	{
		return $this->belongsto(SysBankAccount::class, 'bank_id','id');
	}

	
}
