<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class SysBankAccount extends Model
{
    use HasFactory, HashId;

	protected $table = 'sys_bank_accounts';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'account_name', 'enabled', 'bank_id', 'account_number'
	];

    public function hasBank()
	{
		return $this->belongsto(SysBank::class, 'bank_id','id');
	}
}
