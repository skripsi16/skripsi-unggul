<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Utilities\HashId;

class SysArea extends Authenticatable
{
    use HasFactory, Notifiable, HashId;

	protected $table = 'sys_areas';
	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'name', 'enabled', 'country_id', 'province_id', 'city_id', 'district_id'
	];

	public function country()
    {
        return $this->belongsTo(SysCountry::class, 'country_id', 'id');
	}

	public function province()
    {
        return $this->belongsTo(SysProvince::class, 'province_id', 'id');
    }

	public function city()
    {
        return $this->belongsTo(SysCity::class, 'city_id', 'id');
    }
	
    public function district()
    {
        return $this->belongsTo(SysDistrict::class, 'district_id', 'id');
    }


}
