<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class School extends Model
{
    use HasFactory, HashId;

	protected $table = 'schools';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'santri_id', 'npsn', 'school_name', 'status_of_school', 'address_of_school', 'exam', 'graduation_year', 'country_id_school', 'province_id_school', 'city_id_school', 'district_id_school', 'area_id_school',

	];
	public function country()
    {
        return $this->belongsTo(SysCountry::class, 'country_id_school', 'id');
    }

    public function province()
    {
        return $this->belongsTo(SysProvince::class, 'province_id_school', 'id');
    }

    public function city()
    {
        return $this->belongsTo(SysCity::class, 'area_id_school', 'id');
    }

    public function district()
    {
        return $this->belongsTo(SysDistrict::class, 'district_id_school', 'id');
    }

    public function area()
    {
        return $this->belongsTo(SysArea::class, 'area_id_school', 'id');
    }
}
