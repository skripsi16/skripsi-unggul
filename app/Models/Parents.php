<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class Parents extends Model
{
    use HasFactory, HashId;

	protected $table = 'parents';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'santri_id', 'name_father', 'name_mother', 'name_guardian', 
		'father_education', 'mother_education', 'guardian_education',
		'father_jobs', 'mother_jobs', 'guardian_jobs',
		'father_income', 'mother_income', 'guardian_income',
		'father_phone_number', 'mother_phone_number', 'guardian_phone_number',
	];
}
