<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class SysCategoryPacket extends Model
{
    use HasFactory, HashId;

	protected $table = 'sys_category_packets';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'name', 'description', 'enabled',
	];
}
