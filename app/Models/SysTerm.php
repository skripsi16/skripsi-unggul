<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class SysTerm extends Model
{
    use HasFactory, HashId;

	protected $table = 'sys_terms';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'title', 'description', 'enabled'
	];
}
