<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class PasswordReset extends Model
{
    use HasFactory, HashId;

	protected $table = 'password_resets';

	protected $fillable = [
		'email', 'name', 'token'
	];
}
