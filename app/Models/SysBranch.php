<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class SysBranch extends Model
{
    use HasFactory, HashId;

	protected $table = 'sys_branch';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'name', 'description', 'enabled'
	];
}
