<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Utilities\HashId;

class SysProfileCompany extends Authenticatable
{
    use HasFactory, Notifiable, HashId;

	protected $table = 'sys_profile_companies';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'name', 'description', 'image_url', 'address', 'website', 'email', 'phone_number', 'social_media', 'created_by', 'updated_by'
	];


}
