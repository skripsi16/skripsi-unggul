<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class SysFaqCategory extends Model
{
    use HasFactory, HashId;

	protected $table = 'sys_faq_categories';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'name', 'description', 'enabled'
	];
}
