<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class SysSlider extends Model
{
    use HasFactory, HashId;

	protected $table = 'sys_sliders';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'link', 'enabled', 'title', 'image', 'category'
	];
}
