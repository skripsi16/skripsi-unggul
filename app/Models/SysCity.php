<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Utilities\HashId;

class SysCity extends Authenticatable
{
    use HasFactory, Notifiable, HashId;

    protected $table = 'sys_cities';
	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'name', 'enabled', 'country_id', 'province_id'
	];

    public function country()
    {
        return $this->belongsTo(SysCountry::class, 'country_id', 'id');
    }
    
	public function province()
    {
        return $this->belongsTo(SysProvince::class, 'province_id', 'id');
    }

    public function districts()
    {
        return $this->hasMany(SysDistrict::class, 'district_id', 'id');
    }


}
