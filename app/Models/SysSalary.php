<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class SysSalary extends Model
{
    use HasFactory, HashId;

	protected $table = 'sys_salaries';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'amount', 'description', 'enabled'
	];
}
