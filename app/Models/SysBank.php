<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\HashId;

class SysBank extends Model
{
    use HasFactory, HashId;

	protected $table = 'sys_banks';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id', 'name', 'enabled', 'image',
	];
}
