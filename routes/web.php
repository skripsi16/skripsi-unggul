<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::namespace('Frontoffice')->group(function () {
    Route::get('/', 'HomeController@index')->name('frontoffice');
    // Route::resource('users', UserController::class);
    Route::namespace('Auth')->group(function () {
        Route::get('login-user', 'AuthController@showLoginForm')->name('auth-user.login');
		Route::post('login-user', 'AuthController@LoginProcess')->name('auth-user.loginProcess');
        Route::post('auth-user/logout', 'AuthController@logout')->name('auth-user.logout');

        Route::get('/register', 'RegisterController@showRegisterForm')->name('auth-user.register');
        Route::post('/register', 'RegisterController@create')->name('register-form');
        Route::get('/invoice/{id}', 'RegisterController@invoice')->name('register-sukses');
        Route::get('/register/create/{id}/cities', 'RegisterController@getCities')->name('register.cities');
        Route::get('/register/create/{id}/district', 'RegisterController@getDistricts')->name('register.districts');
        Route::get('/register/create/{id}/area', 'RegisterController@getAreas')->name('register.area');

        //Lupa Password
		Route::get('forgot-password', 'ForgotPasswordController@getEmail')->name('auth-user.forgotPassword');
		Route::post('forgot-password', 'ForgotPasswordController@sendEmail')->name('auth-user.sendForgotPassword');
		Route::get('reset-password/{token}', 'ForgotPasswordController@editPassword')->name('auth-user.editPassword');
		Route::put('reset-password', 'ForgotPasswordController@updatePassword')->name('auth-user.updatePassword');
		Route::get('activity-user/{token}', 'ForgotPasswordController@activatedUser')->name('auth-user.activeUser');
    });
    Route::group(['middleware' => ['auth:web']], function(){
		Route::namespace('User')->group(function(){
			Route::get('/dashboard-user', 'DashboardController@index')->name('dashboard-user');
            Route::post('/dashboard-register', 'DashboardController@store')->name('dashboard-register');

            Route::resource('parents', ParentController::class);
            Route::post('parents/approve', 'ParentController@approvalProccess')->name('parent-approval');

            Route::resource('students', StudentController::class);
            Route::post('students/approve', 'StudentController@approvalProccess')->name('student-approval');
            Route::get('/students/create/{id}/cities', 'StudentController@getCities')->name('students.cities');
			Route::get('/students/create/{id}/district', 'StudentController@getDistricts')->name('students.districts');
			Route::get('/students/create/{id}/area', 'StudentController@getAreas')->name('students.area');
		});
	});
});


Route::group(['prefix'     => 'admin', 'namespace'  => 'Backoffice',], function () {

    Route::namespace('Auth')->group(function () {
        Route::get('/', function () {
            return redirect()->route('backoffice.login');
        });
        Route::get('/login', 'LoginController@showLoginForm')->name('backoffice.login');
        Route::post('/login', 'LoginController@login');
        Route::get('/register', 'RegisterController@showRegisterForm')->name('backoffice.register');
        Route::post('/register', 'RegisterController@create');
        Route::post('/logout', 'LoginController@logout')->name('backoffice.logout');

        //Lupa Password
        Route::get('forgot-password', 'ForgotPasswordController@getEmail')->name('backoffice.forgotpassword');
        Route::post('forgot-password', 'ForgotPasswordController@sendEmail')->name('backoffice.forgetpassword.send');
        Route::get('reset-password/{token}', 'ForgotPasswordController@editPassword')->name('backoffice.edit.password');
        Route::put('reset-password', 'ForgotPasswordController@updatePassword')->name('backoffice.update.password');
    
    });
        // Route::get('activity-user/{token}', 'ForgotPasswordController@activatedUser')->name('backoffice.active-user');

    Route::group(['middleware' => ['auth:admin']], function() {
        Route::get('/dashboard', 'DashboardController@index')->name('backoffice.dashboard');
        Route::get('/profile-user/{id}', 'DashboardController@show')->name('backoffice.profile-user');
        Route::get('/change-password/{id}', 'DashboardController@changePassword')->name('backoffice.change-password');
        Route::put('/change-password/{id}', 'DashboardController@updatePassword')->name('backoffice.update-password');
        Route::get('/edit-profile/{id}', 'DashboardController@editProfile')->name('backoffice.edit-profile');
        Route::put('/update-profile{id}', 'DashboardController@updateProfile')->name('backoffice.update-profile');

        Route::namespace('DataRegister')->group(function () {
            Route::get('/data-registrasi', 'RegisterController@index')->name('data-registrasi.index');
            Route::get('/data-registrasi/{id}', 'RegisterController@show')->name('data-registrasi.show');
            Route::post('data-registrasi/approve', 'RegisterController@approvalProccess')->name('data-registrasi.approval');
		});

        Route::namespace('MasterData')->group(function () {
            Route::resource('banks', SysBankController::class);
			Route::get('banks/{bank}/{status}', 'SysBankController@activeNonActive')->name('banks.active_nonactive');

            Route::resource('bank_accounts', SysBankAccountController::class);
			Route::get('bank_accounts/{bank_accounts}/{status}', 'SysBankAccountController@activeNonActive')->name('bank_accounts.active_nonactive');

            Route::resource('categories', SysCategoryController::class);
			Route::get('categories/{category}/{status}', 'SysCategoryController@activeNonActive')->name('categories.active_nonactive');
            Route::post('categories/image_upload', 'SysCategoryController@upload')->name('upload_categories');

            Route::resource('branchs', SysBranchController::class);
			Route::get('branchs/{branch}/{status}', 'SysBranchController@activeNonActive')->name('branchs.active_nonactive');
           
            Route::resource('prosedurs', SysProsedurController::class);
			Route::get('prosedurs/{prosedur}/{status}', 'SysProsedurController@activeNonActive')->name('prosedurs.active_nonactive');

            Route::resource('packet-educations', SysPacketController::class);
			Route::get('packet-educations/{packet_education}/{status}', 'SysPacketController@activeNonActive')->name('packet-educations.active_nonactive');

            Route::resource('category-packets', SysCategoryPacketController::class);
			Route::get('category-packets/{category_packet}/{status}', 'SysCategoryPacketController@activeNonActive')->name('category-packets.active_nonactive');

            Route::resource('educations', SysEducationController::class);
			Route::get('educations/{education}/{status}', 'SysEducationController@activeNonActive')->name('educations.active_nonactive');

            Route::resource('jobs', SysJobsController::class);
			Route::get('jobs/{job}/{status}', 'SysJobsController@activeNonActive')->name('jobs.active_nonactive');

            Route::resource('salaries', SysSalaryController::class);
			Route::get('salaries/{salary}/{status}', 'SysSalaryController@activeNonActive')->name('salaries.active_nonactive');

            Route::resource('sliders', SysSliderController::class);
			Route::get('sliders/{slider}/{status}', 'SysSliderController@activeNonActive')->name('sliders.active_nonactive');

            Route::resource('terms', SysTermController::class);
			Route::get('terms/{term}/{status}', 'SysTermController@activeNonActive')->name('terms.active_nonactive');
            Route::post('terms/image_upload', 'SysTermController@upload')->name('upload_terms');
		});

        Route::namespace('Utilities')->group(function () {
            Route::resource('profile-company', SysProfileCompanyController::class);
            Route::post('profile-company/image_upload', 'SysProfileCompanyController@upload')->name('profile-company.upload');

            Route::resource('faq_categories', SysFaqCategoryController::class);
			Route::get('faq_categories/{faq_category}/{status}', 'SysFaqCategoryController@activeNonActive')->name('faq_categories.active_nonactive');
            Route::post('faq_categories/image_upload', 'SysFaqCategoryController@upload')->name('upload_faq_categories');

            Route::resource('faq_descriptions', SysFaqDescriptionController::class);
			Route::get('faq_descriptions/{description}/{status}', 'SysFaqDescriptionController@activeNonActive')->name('faq_descriptions.active_nonactive');
            Route::post('faq_descriptions/image_upload', 'SysFaqDescriptionController@upload')->name('upload_faq_descriptions');

            Route::resource('brands', SysBrandController::class);
			Route::get('brands/{brand}/{status}', 'SysBrandController@activeNonActive')->name('brands.active_nonactive');
		});

    });
});

