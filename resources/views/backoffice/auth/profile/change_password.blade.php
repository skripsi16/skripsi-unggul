@extends('backoffice.layouts.app')
@section('content')
<section class="content">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<form action="{{route('backoffice.update-password', $data->id)}}" method="POST">
					@csrf
					@method('PUT')
					
					<!-- Horizontal Form -->
					<div class="box">
						<div class="box-header with-border">
							<h4 class="box-title">Reset Password</h4>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<div class="form-group">
								<div class="controls">
									<input type="hidden" name="id" class="form-control" value="{{ $data->id }}">
								</div>
							</div>
							<div class="form-group row">
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text bg-transparent"><i class="ti-lock"></i></span>
									</div>
									<input type="password" name="password" required class="form-control pl-15 bg-transparent{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="New Password" autocomplete="password" autofocus>
									@if ($errors->has('password'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group row">
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text bg-transparent"><i class="ti-lock"></i></span>
									</div>
									<input type="password" name="password_confirmation" required class="form-control pl-15 bg-transparent{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" placeholder="Confirmation Password" autocomplete="password" autofocus>
									@if ($errors->has('password_confirmation'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('password_confirmation') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="text-xs-right">
								<a href="{{route('backoffice.dashboard')}}" type="button" class="waves-effect waves-light btn btn-outline btn-secondary mb-5"><i class="ti-trash"></i>&nbsp;Batal</a><span></span>
								<button type="submit" class="waves-effect waves-light btn btn-outline btn-primary mb-5"><i class="ti-save-alt"></i>&nbsp;Simpan</button>
							</div>
						</div>
					</div>
				</form>
				<!-- /.box -->
			</div>
		</div>
	</div>

</section>

@endsection