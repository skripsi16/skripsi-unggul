@extends('backoffice.layouts.app')
@section('content')
<!-- Main content -->
<section class="content">

	<!-- Basic Forms -->
	<div class="box">
		<div class="box-header with-border">
			<h4 class="box-title">Form Edit</h4>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="row">
				<div class="col">
                    <form action="{{route('backoffice.update-profile',$data->id)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        {{ method_field('PUT') }}
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<div class="controls">
										<input type="hidden" name="id" class="form-control" required value="{{ old('id') ? old('id') : $data->id }}">
									</div>
								</div>
								<div class="form-group">
									<h5>Nama Admin<span class="text-danger">*</span></h5>
									<div class="controls">
										<input type="text" name="name" class="form-control" required value="{{ old('name') ? old('name') : $data->name }}">
										@if ($errors->has('name'))
										<div class="form-control-feedback text-danger">{{ $errors->first('name') }}</div>
										@endif
									</div>
								</div>
								<div class="form-group">
									<h5>Email<span class="text-danger">*</span></h5>
									<div class="controls">
										<input type="email" name="email" class="form-control" required value="{{ old('email') ? old('email') : $data->email }}">
										@if ($errors->has('email'))
										<div class="form-control-feedback text-danger">{{ $errors->first('email') }}</div>
										@endif
									</div>
								</div>
								<div class="form-group">
									<h5>Nomor Telepon<span class="text-danger">*</span></h5>
									<div class="controls">
										<input type="number" min="0" name="phone_number" class="form-control" required value="{{ old('phone_number') ? old('phone_number') : $data->phone_number }}">
										@if ($errors->has('phone_number'))
										<div class="form-control-feedback text-danger">{{ $errors->first('phone_number') }}</div>
										@endif
									</div>
								</div>
								<div class="form-group">
									<h5>Jenis Kelamin <span class="text-danger">*</span></h5>
									<div class="controls">
										<select name="gender" id="select" required="" class="form-control" aria-invalid="false">
											{{-- <option value="{{ $data->gender }}" {{ @$data->gender == $data->gender ? 'selected' : "" }}>{{ old('gender') ? old('gender') : $data->gender }}</option> --}}
                                            <option>--Pilih Jenis Kelamin--</option>
											<option value="Laki-laki" {{ @$data->gender == 'Laki-laki' ? 'selected' : "" }}>Laki-laki</option>
											<option value="Perempuan" {{ @$data->gender == 'Perempuan' ? 'selected' : "" }}>Perempuan</option>
										</select>
										<div class="help-block"></div>
										@if ($errors->has('gender'))
										<div class="form-control-feedback text-danger">{{ $errors->first('gender') }}</div>
										@endif
									</div>
								</div>
								<div class="form-group">
									<h5>Alamat</h5>
									<div class="controls">
										<input type="text" name="address" class="form-control" value="{{ old('address') ? old('address') : $data->address }}">
										@if ($errors->has('address'))
										<div class="form-control-feedback text-danger">{{ $errors->first('address') }}</div>
										@endif
									</div>
                                </div>
                                <div class="form-group">
									<h5>Gambar</h5>
									<div class="controls">
										<input type="file" name="image" class="form-control" required value="{{ old('image') ? old('image') : $data->image }}">
										@if ($errors->has('image'))
										<div class="form-control-feedback text-danger">{{ $errors->first('image') }}</div>
										@endif
									</div>
								</div>
							</div>
						</div>
						<div class="text-xs-right">
							<a href="{{route('backoffice.dashboard')}}" type="button" class="waves-effect waves-light btn btn-outline btn-secondary mb-5"><i class="ti-trash"></i>&nbsp;Batal</a><span></span>
							<button type="submit" class="waves-effect waves-light btn btn-outline btn-primary mb-5"><i class="ti-save-alt"></i>&nbsp;Simpan</button>
						</div>
					</form>

				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->

@endsection