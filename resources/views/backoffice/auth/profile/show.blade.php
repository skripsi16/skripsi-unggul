@extends('backoffice.layouts.app')
@section('content')
<section class="content">
    <div class="raw">
        <div class="col-lg-12 col-12">
            <div class="box">
              <div class="box-header with-border">
                <h4 class="box-title text-info"><i class="ti-user mr-15"></i> About User</h4>
              </div>
              <!-- /.box-header -->
              <form class="form">
                  <div class="box-body">
                      <style>
                        .img-container {
                          text-align: center;
                          display: block;
                        }
                      </style>
                      <span class="img-container"> <!-- Inline parent element -->
                        @if(Auth::guard('admin')->user()->image != NULL || !empty(Auth::guard('admin')->user()->image))
                           <img src="{{asset('backoffice/assets/images/admin/'. Auth::guard('admin')->user()->image)}}" width="250px">
						            @else
							             <img src="{{asset('backoffice/assets/images/avatar/avatar-13.png')}}" class="avatar avatar-lg bg-primary-light" alt="User Image">
						            @endif
                      </span>
                      <hr class="my-15">
                      <div class="row">
                        <div class="col-md-6">   
                          <div class="form-group">
                            <label>Nama</label><br>
                            <input type="text" disabled="" style="background:white" class="form-control" value="{{ ($data->name) ?? '-' }}">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <input type="text" disabled="" style="background:white" class="form-control" value="{{ ($data->gender) ?? '-'}}">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label >E-mail</label>
                            <input type="text" disabled="" style="background:white" class="form-control" value="{{ ($data->email) ?? '-' }}" placeholder="">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label >Nomor Kontak</label>
                            <input type="text" disabled="" style="background:white" class="form-control" placeholder="Phone" value="{{ ($data->phone_number) ?? '-'}}">
                          </div>
                        </div>
                      </div>
                      <h4 class="box-title text-info"><i class="ti-envelope mr-15"></i> Kontak Info & Bio</h4>
                      <hr class="my-15">
                      <div class="form-group">
                        <label>Alamat</label>
                        <input type="text" disabled="" style="background:white" class="form-control" value="{{ empty($data->address)? 'Alamat Tidak Ada/Kosong' : $data->address }}" placeholder="">
                      </div>
                      
                  </div>
                  <!-- /.box-body -->
              </form>
            </div>
            <!-- /.box -->	
            <a href="{{route('backoffice.dashboard')}}" type="button" class="waves-effect waves-light btn btn-outline btn-secondary mb-5"><i class="fa fa-backward"></i>&nbsp;Kembali</a>		
      </div>
    </div>
    
</section>

@endsection
