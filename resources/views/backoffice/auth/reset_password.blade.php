<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="{{ asset('frontoffice/assets/images/logo.png') }}">

	<title>Lupa Password</title>

	<!-- Vendors Style-->
	<link rel="stylesheet" href="{{asset('backoffice/assets/css/vendors_css.css')}}">

	<!-- Style-->
	<link rel="stylesheet" href="{{asset('backoffice/assets/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('backoffice/assets/css/skin_color.css')}}">

</head>

<body class="hold-transition theme-primary bg-img" style="background-image: url(../images/auth-bg/bg-2.jpg)">

	<div class="container h-p100">
		<div class="row align-items-center justify-content-md-center h-p100">

			<div class="col-12">
				<div class="row justify-content-center no-gutters">
					<div class="col-lg-5 col-md-5 col-12">
						<div class="bg-white rounded30 shadow-lg">
							<div class="content-top-agile p-20 pb-0">
								<h3 class="mb-0 text-primary">Recover Password</h3>
							</div>
							<div class="p-40">
								<form action="{{route('backoffice.update.password')}}" method="post">
									@csrf
                                    @method('PUT')
									@if($message = Session::get('error'))
									<div class="alert alert-danger">
										<button type="button" class="close" data-dismiss="alert">×</button>
										{{$message}}
									</div>
									@elseif($message = Session::get('success'))
									<div class="alert alert-success">
										<button type="button" class="close" data-dismiss="alert">×</button>
										{{$message}}
									</div>
									@endif
									<input name="token" value="{{$data->token}}" hidden>
									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text bg-transparent"><i class="ti-email"></i></span>
											</div>
											<input type="text" name="email" required class="form-control pl-15 bg-transparent @error('email') is-invalid @enderror" placeholder="Email" value="{{ $data->email}}" autocomplete="email" readonly>

											@error('email')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror

										</div>
									</div>

									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text bg-transparent"><i class="ti-lock"></i></span>
											</div>
											<input type="password" name="password" required class="form-control pl-15 bg-transparent @error('password') is-invalid @enderror" placeholder="Password" autocomplete="new-password" autofocus>

											@error('password')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror

										</div>
									</div>

									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text bg-transparent"><i class="ti-lock"></i></span>
											</div>
											<input type="password" name="confirm_password" class="form-control pl-15 bg-transparent @error('confirm_password') is-invalid @enderror" placeholder="Repeat Password" autocomplete="new-password" autofocus>

											@error('confirm_password')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
											@enderror
										</div>
									</div>

									<div class="row">
										<div class="col-12 text-center">
											<button type="submit" class="btn btn-info margin-top-10">Reset</button>
										</div>
										<!-- /.col -->
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Vendor JS -->
	<script src="{{asset('backoffice/assets/js/vendors.min.js')}}"></script>
	<script src="{{asset('backoffice/assets/icons/feather-icons/feather.min.js')}}"></script>

</body>

</html>