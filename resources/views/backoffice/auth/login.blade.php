<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="{{ asset('frontoffice/assets/images/logo.png') }}">

	<title>Login - Administrator</title>

	<!-- Vendors Style-->
	<link rel="stylesheet" href="{{asset('backoffice/assets/css/vendors_css.css')}}">

	<!-- Style-->
	<link rel="stylesheet" href="{{asset('backoffice/assets/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('backoffice/assets/css/skin_color.css')}}">

</head>

<body class="hold-transition theme-primary bg-img" style="background-image: url(../images/auth-bg/bg-1.jpg)">

	<div class="container h-p100">
		<div class="row align-items-center justify-content-md-center h-p100">

			<div class="col-12">
				<div class="row justify-content-center no-gutters">
					<div class="col-lg-5 col-md-5 col-12">
						<div class="bg-white rounded30 shadow-lg">
							<div class="content-top-agile p-20 pb-0">
								<h2 class="text-primary">Login - Administrator</h2>
								{{-- <p class="mb-0"></p> --}}
							</div>
							<div class="p-40">
								<form action="" method="post">
									@csrf
									@if($message = Session::get('error'))
									<div class="alert alert-danger">
										<button type="button" class="close" data-dismiss="alert">×</button>
										{{$message}}
									</div>
									@elseif($message = Session::get('success'))
									<div class="alert alert-success">
										<button type="button" class="close" data-dismiss="alert">×</button>
										{{$message}}
									</div>
									@endif
									@if (session('status'))
									<div class="alert alert-danger">
										<button type="button" class="close" data-dismiss="alert">×</button>
										{{ session('status') }}
									</div>
									@endif
									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text bg-transparent"><i class="ti-user"></i></span>
											</div>
											<input type="email" name="email" required="Email Tidak Sesuai!" class="form-control pl-15 bg-transparent{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" value="{{ old('email') }}" autocomplete="email" autofocus>
											@if ($errors->has('email'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('email') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text  bg-transparent"><i class="ti-lock"></i></span>
											</div>
											<input type="password" name="password" required="Password Tidak Sesuai!" class="form-control pl-15 bg-transparent{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" autocomplete="current-password">
											@if ($errors->has('password'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('password') }}</strong>
											</span>
											@endif
										</div>
									</div>
									<div class="row">
										
										<div class="col-12 text-center">
											<button type="submit" class="btn btn-danger mt-10">Login</button>
										</div>
										<div class="col-12 text-center text-md-right">
											<a href="{{ route('backoffice.forgotpassword') }}" class="card-link">Lupa Password?</a>
										</div>
										<!-- /.col -->
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Vendor JS -->
	<script src="{{asset('backoffice/assets/js/vendors.min.js')}}"></script>
	<script src="{{asset('backoffice/assets/icons/feather-icons/feather.min.js')}}"></script>

</body>

</html>
