@extends('backoffice.layouts.app')
@section('content')
    		<!-- Main content -->
<section class="content">

<!-- Basic Forms -->
    <div class="row">

		<div class="col-12">
			<div class="box">
			
				<div class="box-header">
					<h4 class="box-title">Detail Pendaftar</h4>  
				</div>
				<div class="box-body">
					<div class="form-group row">
						<label class="col-form-label col-md-2">Nomor Pendaftaran</label>
						<div class="col-md-10">
						<text class="form-control">{{ $data->no_register }}</text>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label col-md-2">Nama Pendaftar</label>
						<div class="col-md-10">
						<text class="form-control">{{ $data->hasStudent->name }}</text>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label col-md-2">Paket</label>
						<div class="col-md-10">
						<text class="form-control">{{ $data->hasPacket->name }}</text>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label col-md-2">Cabang</label>
						<div class="col-md-10">
						<text class="form-control">{{ $data->hasBranch->name }}</text>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label col-md-2">Kategori</label>
						<div class="col-md-10">
						<text class="form-control">{{ $data->hasCategory->name }}</text>
						</div>
					</div>

					
					
				</div>
			<!-- /.box-body -->			  
			</div>
		</div>
		<!-- ./col -->
 	</div>

     <div class="row">

		<div class="col-12">
			<div class="box">
			
				<div class="box-header">
					<h4 class="box-title">Detail Santri</h4>  
				</div>
				<div class="box-body">
					
                    <div class="form-group row">
						<label class="col-form-label col-md-2">Nama Pendaftar</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->name }}</text>
						</div>

                        <label class="col-form-label col-md-2">Negara</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->country->name }}</text>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label col-md-2">NISN Pendaftar</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->nisn }}</text>
						</div>

                        <label class="col-form-label col-md-2">Provinsi</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->province->name }}</text>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label col-md-2">NIK Pendaftar</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->nik }}</text>
						</div>

                        <label class="col-form-label col-md-2">Kota</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->city->name }}</text>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label col-md-2">Nomor Hp Pendaftar</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->phone_number }}</text>
						</div>

                        <label class="col-form-label col-md-2">Kecamatan</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->district->name }}</text>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label col-md-2">Email Pendaftar</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->email }}</text>
						</div>

                        <label class="col-form-label col-md-2">Kelurahan</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->area->name }}</text>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label col-md-2">Jenis Kelamin</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->gender }}</text>
						</div>

                        <label class="col-form-label col-md-2">Alamat</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->address }}</text>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label col-md-2">Agama</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->religion }}</text>
						</div>

                        <label class="col-form-label col-md-2">NPSN</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $school->npsn }}</text>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label col-md-2">Status di Keluarga</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->status_of_family }}</text>
						</div>

                        <label class="col-form-label col-md-2">Nama Sekolah</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $school->school_name }}</text>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label col-md-2">Tanggal Lahir</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->birth_date }}</text>
						</div>

                        <label class="col-form-label col-md-2">Nama Pendaftar</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->name }}</text>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label col-md-2">Tempat Lahir</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $data->hasStudent->city->name }}</text>
						</div>

                        <label class="col-form-label col-md-2">Tahun Lulus</label>
						<div class="col-md-4">
						    <text class="form-control">{{ $school->graduation_year }}</text>
						</div>
					</div>
					
				</div>
			<!-- /.box-body -->			  
			</div>
			<!-- /.box -->
		    <a href="{{route('data-registrasi.index')}}" type="button" class="waves-effect waves-light btn btn-outline btn-secondary mb-5"><i class="fa fa-backward"></i>&nbsp;Kembali</a>
		</div>
		<!-- ./col -->
 	</div>
	<!-- /.box -->
</section>
<!-- /.content -->

@endsection

