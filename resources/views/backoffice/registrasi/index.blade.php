@extends('backoffice.layouts.app')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="box">
                @if(Session::has('success'))
                <div class="alert alert-outline alert-success" role="alert">
                    <i data-feather="check" class="mg-r-10"></i> {{Session::get('success')}}
                </div>
                @elseif((Session::has('error')))
                <div class="alert alert-outline alert-danger" role="alert">
                    <i data-feather="alert-circle" class="mg-r-10"></i> {{Session::get('error')}}
                </div>
                @endif
                <div class="box-header with-border">
                    <h3 class="box-title">Data Pendaftar</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                @if(count($data) > 0)
                    <div class="table-responsive">
                        <div id="example_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                            <table class="table b-1 border-primary">
                                <thead class="bg-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>Nomor Pendaftaran</th>
                                        <th>Siswa</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1;
                                    @endphp
                                    @foreach($data as $row)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $row->no_register }}</td>
                                        <td>{{ $row->hasStudent->name }}</td>
                                        <td>
                                            @if ($row->status == "VERIFIED")
                                                <span class="badge badge-success">VERIFIED</span>
                                            @elseif ($row->status == "UNVERIFIED")
                                                <span class="badge badge-primary">UNVERIFIED</span>
                                            @else
                                                <span class="badge badge-danger">Cancel</span>
                                            @endif
                                        </td>
                                        <td class="btn-group">
                                            <a href="{{ route('data-registrasi.show', $row->santri_id) }}"
                                                class="btn btn-primary btn-sm"><i class="fa fa-eye"></i>
                                            </a>
                                            @if($row->status != 'VERIFIED')
                                            <a href="#modal5" data-toggle="modal" class="btn btn-success btn-sm btn-active" data-id="{{ $row->id }}" data-approve="VERIFIED" title="Approve">
                                                <i class="fa fa-check"></i>
                                            </a>
                                            @endif
                                            @if($row->status != 'CANCEL')
                                            <a href="#modal5" data-toggle="modal" class="btn btn-danger btn-sm btn-active" data-id="{{ $row->id }}"  data-approve="CANCEL" title="Cancel">
                                                <i class="fa fa-close"></i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $data->links('backoffice.layouts.pagination') }}
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                @else
                    <H2>Data Tidak Ditemukan!</H2>
                @endif
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
      <div class="modal-content tx-14">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLabel5">Konfirmasi</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p class="mg-b-0 modal5-message-body"></p>
        </div>
        <div class="modal-footer">
          <form action="{{ route('data-registrasi.approval') }}" method="post">
              {{csrf_field()}}
              <input type="hidden" name="id" value="" id="no_register" class="xs-input-control no_register" placeholder="">
              <input type="hidden" name="status" value="" id="status" class="xs-input-control" placeholder="">
              <input type="hidden" name="page" value="list">
              <button type="submit" class="btn btn-primary tx-13">Submit</button>
              <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
          </form>
        </div>
      </div>
    </div>
</div>

@endsection


