<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="{{ asset('frontoffice/assets/images/logo.png') }}">
    
        <title>Admin - Dashboard</title>
        
        <!-- Vendors Style-->
        <link rel="stylesheet" href="{{asset('backoffice/assets/css/vendors_css.css')}}">
        @yield('top-resource')
        <!-- Style-->
        <link rel="stylesheet" href="{{asset('backoffice/assets/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('backoffice/assets/css/skin_color.css')}}">
    
    </head>
    <body class="hold-transition light-skin sidebar-mini theme-primary">
	
        <div class="wrapper">
            @include('backoffice.layouts.header')
            @include('backoffice.layouts.sidebar')
            
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div class="container">
                    <!-- Main content -->
                    @yield('content')
                    <!-- /.content -->
                </div>
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="container">
                    {{-- <div class="pull-right d-none d-sm-inline-block">
                        <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:void(0)">FAQ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Purchase Now</a>
                        </li>
                        </ul>
                    </div> --}}
                    &copy; 2020 <a href="https://www.multipurposethemes.com/">Multipurpose Themes</a>. All Rights Reserved.
                </div>
            </footer>
        </div>
</body>

 <!-- Vendor JS -->
 <script src="{{asset('backoffice/assets/icons/feather-icons/feather.min.js')}}"></script>

 {{-- Perubahan --}}

 <script src="{{asset('backoffice/assets/js/vendors.min.js')}}"></script>
 <script src="{{asset('backoffice/assets/vendor_components/c3/d3.min.js')}}"></script>
 <script src="{{asset('backoffice/assets/vendor_components/c3/c3.min.js')}}"></script>

 <!-- Power BI Admin App -->
 @yield('bottom-resource')

<script>
    $('.btn-active').click(function(){
		var id = $(this).data('id');
        var approve = $(this).data('approve');
        if (approve == "VERIFIED" || approve == "CANCEL"){
            var modal5 = $('#modal5');
            modal5.find('[id=no_register]').val(id);
            modal5.find('[id=status]').val(approve);
            if (approve == "VERIFIED"){
                modal5.find('.modal5-message-body').text("Apakah anda yakin akan menyetujui ?")
            } 
            else if (approve == "CANCEL") {
                modal5.find('.modal5-message-body').text("Apakah anda yakin akan menolak ?")
            }
        } 
        
	});
</script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>
 <script src="{{asset('backoffice/assets/js/template.js')}}"></script>
 <script src="{{asset('backoffice/assets/js/demo.js')}}"></script>
 <script src="{{asset('backoffice/assets/js/pages/dashboard.js')}}"></script>
 <script>
     $(document).ready(function(){
		$('.uang').mask('000.000.000.000.000', {reverse: true});
	});
 </script>
</html>
