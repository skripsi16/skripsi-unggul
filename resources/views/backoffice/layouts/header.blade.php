<header class="main-header">
  <div class="d-flex align-items-center logo-box justify-content-between">
      <a href="#" class="waves-effect waves-light nav-link rounded d-none d-md-inline-block mx-10 push-btn" data-toggle="push-menu" role="button">
          <span class="icon-Align-left"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
      </a>	
      <!-- Logo -->
      <a href="{{ route('backoffice.dashboard') }}" class="logo">
        <!-- logo-->
        <div class="logo-lg">
            <span class="light-logo">Dashboard Admin</span>
            <span class="dark-logo">Dashboard Admin</span>
        </div>
      </a>	
  </div>  
  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top pl-10">
      <div class="container">
        <!-- Sidebar toggle button-->
        <div class="app-menu">
          <ul class="header-megamenu nav">
              <li class="btn-group nav-item d-md-none">
                  <a href="#" class="waves-effect waves-light nav-link rounded push-btn" data-toggle="push-menu" role="button">
                      <span class="icon-Align-left"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
                  </a>
              </li>
              <li class="btn-group nav-item d-none d-xl-inline-block">
                  <a href="#" data-provide="fullscreen" class="waves-effect waves-light nav-link rounded full-screen" title="Full Screen">
                      <i class="icon-Expand-arrows"><span class="path1"></span><span class="path2"></span></i>
                  </a>
              </li>
          </ul> 
        </div>

        <div class="navbar-custom-menu r-side">
          <ul class="nav navbar-nav">		  
              <li class="btn-group d-lg-inline-flex d-none">
                  <div class="app-menu">
                      <div class="search-bx mx-5">
                          {{-- <form>
                              <div class="input-group">
                                <input type="search" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="button-addon2">
                                <div class="input-group-append">
                                  <button class="btn" type="submit" id="button-addon3"><i class="ti-search"></i></button>
                                </div>
                              </div>
                          </form> --}}
                      </div>
                  </div>
              </li>

            <!-- User Account-->
            <li class="dropdown user user-menu">
              <a href="#" class="waves-effect waves-light dropdown-toggle" data-toggle="dropdown" title="User">
                  <i class="icon-User"><span class="path1"></span><span class="path2"></span></i>
              </a>
              <ul class="dropdown-menu animated flipInX">
                <li class="user-body">
                  <a class="dropdown-item" href="{{ route('backoffice.profile-user', Auth::guard('admin')->user()->encodeHash(Auth::guard('admin')->user()->id)) }}"><i class="ti-user text-muted mr-2"></i> Profile</a>
                  <a class="dropdown-item" href="{{ route('backoffice.edit-profile', Auth::guard('admin')->user()->encodeHash(Auth::guard('admin')->user()->id)) }}"><i class="ti-wallet text-muted mr-2"></i> Edit Profile</a>
                  <a class="dropdown-item" href="{{ route('backoffice.change-password', Auth::guard('admin')->user()->encodeHash(Auth::guard('admin')->user()->id)) }}"><i class="ti-settings text-muted mr-2"></i> Ubah Password</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('backoffice.logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <i class="ft-power"></i> Logout
                </a>
                  <form id="logout-form" action="{{ route('backoffice.logout') }}" method="POST" style="display: none;">
                    @csrf
                  </form>
                </li>
              </ul>
            </li>	

          </ul>
        </div>
      </div>
  </nav>
</header>