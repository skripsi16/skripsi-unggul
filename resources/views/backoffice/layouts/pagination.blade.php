<div class="dataTables_paginate paging_simple_numbers" id="complex_header_paginate">
    <ul class="pagination">
        <!-- Previous Page Link -->
        @if ($paginator->onFirstPage())
            <li class="paginate_button page-item previous disabled" id="complex_header_previous"><a href="#"
                aria-controls="complex_header" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
        @else
        <li class="paginate_button page-item previous" id="complex_header_previous"><a href="{{ $paginator->previousPageUrl() }}"
                aria-controls="complex_header" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
        @endif

        <!-- Pagination Elements -->
        @foreach ($elements as $element)
        <!-- "Three Dots" Separator -->
        @if (is_string($element))
            <li class="disabled"><span>{{ $element }}</span></li>
        @endif

        <!-- Array Of Links -->
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <li class="paginate_button page-item active"><a href="#" aria-controls="complex_header" data-dt-idx="1"
                    tabindex="0" class="page-link">{{ $page }}</a></li>
                @else
                    <li class="paginate_button page-item "><a href="{{ $url }}" aria-controls="complex_header" data-dt-idx="2" tabindex="0"
                    class="page-link">{{ $page }}</a></li>
                @endif
            @endforeach
        @endif
        @endforeach

        <!-- Next Page Link -->
        @if ($paginator->hasMorePages())
            <li class="paginate_button page-item next" id="complex_header_next"><a href="{{ $paginator->nextPageUrl() }}" aria-controls="complex_header"
                data-dt-idx="7" tabindex="0" class="page-link">Next</a></li>
        @else
            <li class="paginate_button page-item next disabled" id="complex_header_next"><a href="#" aria-controls="complex_header"
                data-dt-idx="7" tabindex="0" class="page-link">Next</a></li>
        @endif
        
    </ul>
</div>
