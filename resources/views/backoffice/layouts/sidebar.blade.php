<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">
      <div class="user-profile px-20 py-15">
        <div class="d-flex align-items-center">
          <div class="image">
              @if(!empty(Auth::guard('admin')->user()->image))
                  <img src="{{asset('backoffice/assets/images/admin/'. Auth::guard('admin')->user()->image)}}" class="avatar avatar-lg bg-primary-light" alt="User Image">
              @else
                  <img src="{{asset('frontoffice/assets/images/logo.png')}}" class="avatar avatar-lg bg-primary-light" alt="User Image">
              @endif
          </div>
          
          <div class="info">
            <a class="px-20" data-toggle="dropdown" href="#">Hallo Kak {{Auth::guard('admin')->user()->name}}</a>
          </div>
        </div>
      </div>
        
      <!-- sidebar menu-->
      <ul class="sidebar-menu" data-widget="tree">		
        <li>
          <a href="{{ route('backoffice.dashboard') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Dashboard</span>
          </a>
        </li>
        
        <li class="header">Master Data</li>
        <li>
          <a href="{{ route('banks.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Bank</span>
          </a>
        </li>
        <li>
          <a href="{{ route('bank_accounts.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Akun Bank</span>
          </a>
        </li>
        <li>
          <a href="{{ route('categories.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Kategori</span>
          </a>
        </li>
        <li>
          <a href="{{ route('prosedurs.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Prosedur</span>
          </a>
        </li>
        <li>
          <a href="{{ route('sliders.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Slider</span>
          </a>
        </li>
        <li>
          <a href="{{ route('terms.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Syarat & Ketentuan</span>
          </a>
        </li>
        <li>
          <a href="{{ route('category-packets.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Kategori Paket Pendidikan</span>
          </a>
        </li>
        <li>
          <a href="{{ route('branchs.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Cabang Pendidikan</span>
          </a>
        </li>
        <li>
          <a href="{{ route('packet-educations.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Paket Pendidikan</span>
          </a>
        </li>
        <li>
          <a href="{{ route('educations.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Pendidikan Orangtua</span>
          </a>
        </li>
        <li>
          <a href="{{ route('jobs.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Pekerjaan</span>
          </a>
        </li>
        <li>
          <a href="{{ route('salaries.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Penghasilan</span>
          </a>
        </li>
        <li class="header">Utilities</li>
        <li>
          <a href="{{ route('profile-company.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Profil Perusahaan</span>
          </a>
        </li>
        <li>
          <a href="{{ route('faq_categories.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>FAQ Category</span>
          </a>
        </li>
        <li>
          <a href="{{ route('faq_descriptions.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>FAQ Deskripsi</span>
          </a>
        </li>
        <li>
          <a href="{{ route('brands.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Support Brand</span>
          </a>
        </li>
        <li class="header">Data</li>
        <li>
          <a href="{{ route('data-registrasi.index') }}">
            <i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
            <span>Pendaftaran Siswa</span>
          </a>
        </li>
      </ul>
    </section>
</aside>