@extends('backoffice.layouts.app')
@section('content')
    		<!-- Main content -->
<section class="content">

    <!-- Basic Forms -->
	<div class="box">
		<div class="box-header with-border">
			<h4 class="box-title">Edit Slider</h4>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			  <div class="row">
				<div class="col">
					<form action="{{route('sliders.update',$data->id)}}" method="POST" class="error" enctype="multipart/form-data">
						@csrf
						{{ method_field('PUT') }}
					  <div class="row">
						<div class="col-12">	
							<div class="form-group">
								<h5>Judul<span class="text-danger">*</span></h5>
								<div class="controls">
									<input type="text" name="title" class="form-control" value="{{$data->title}}">
									@if ($errors->has('title'))
										<div class="form-control-feedback text-danger">{{ $errors->first('title') }}</div>
									@endif
								</div>
							</div>

							<div class="form-group">
								<h5>Kategori<span class="text-danger">*</span></h5>
								<div class="controls">
									<select name="category" class="form-control" id="category">
										<option value="">--Pilih Kategori--</option>
										<option value="img" {{ @$data->category == 'img'? 'selected' : '' }}>Gambar</option>
										<option value="ytb" {{ @$data->category == 'ytb'? 'selected' : '' }}>Youtube</option>
									</select>
									@if ($errors->has('title'))
										<div class="form-control-feedback text-danger">{{ $errors->first('title') }}</div>
									@endif
								</div>
							</div>

							<div class="form-group image-form">
								<h5>Gambar</h5>
								<div class="controls">
									<input type="file" name="image" class="form-control">
									@if ($errors->has('image'))
										<div class="form-control-feedback text-danger">{{ $errors->first('image') }}</div>
									@endif
								</div>
							</div>

                            <div class="form-group link-form">
								<h5>Link</h5>
								<div class="controls">
									<input type="text" name="link" class="form-control" value="{{$data->link}}">
									<span class="text-danger">Contoh Format URL : https://www.youtube.com/embed/ewqhsu232</span>
									@if ($errors->has('link'))
										<div class="form-control-feedback text-danger">{{ $errors->first('link') }}</div>
									@endif
								</div>
							</div>
							<div class="form-group">
								<h5>Status</h5>
								<div class="controls">
									<label class="switch">
										<input type="checkbox" name="enabled" value="1" {{ $data->enabled == 1 ? 'checked="checked"' : '' }} class="switch-indicator">
										<span class="switch-indicator" value="0"></span>
                                    	<span class="switch-description" value="1">Aktif</span>
									</label>

										@if ($errors->has('enabled'))
										<div class="form-control-feedback text-danger">{{ $errors->first('enabled') }}</div>
										@endif
								</div>
							</div>
						</div>
						
					  </div>
						<div class="text-xs-right">
							<a href="{{route('sliders.index')}}" type="button" class="waves-effect waves-light btn btn-outline btn-secondary mb-5"><i class="ti-trash"></i>&nbsp;Batal</a><span></span>
							<button type="submit" class="waves-effect waves-light btn btn-outline btn-primary mb-5"><i class="ti-save-alt"></i>&nbsp;Simpan</button>		
						</div>
					</form>

				</div>
				<!-- /.col -->
			  </div>
			  <!-- /.row -->
			</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->
@section('bottom-resource')
<script>
	$(document).ready(function() {
		$('.image-form').hide();
		$('.link-form').hide();
		$('#category').on('change', function () {
			console.log($(this).val());
			let value = $(this).val();
			if(value == 'ytb') {
				$('.link-form').show();
				$('.image-form').hide();
			} else if(value == 'img') {
				$('.image-form').show();
				$('.link-form').hide();
			} else {
				$('.image-form').hide();
				$('.link-form').hide();
			}
		})
	});
</script>
@endsection

@endsection