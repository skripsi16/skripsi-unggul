@extends('backoffice.layouts.app')
@section('content')
    		<!-- Main content -->
<section class="content">

<!-- Basic Forms -->
	<div class="box">
		<div class="box-header with-border">
			<h4 class="box-title">Tambah Bank</h4>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			  <div class="row">
				<div class="col">
					<form action="{{route('bank_accounts.store')}}" method="POST" class="error" enctype="multipart/form-data">
						@csrf
					  <div class="row">
						<div class="col-12">
							<div class="form-group">
								<h5>Nama<span class="text-danger">*</span></h5>
								<div class="controls">
									<input type="text" name="account_name" class="form-control" required value="{{ old('account_name') }}">
									@if ($errors->has('account_name'))
										<div class="form-control-feedback text-danger">{{ $errors->first('account_name') }}</div>
									@endif
								</div>
							</div>

                            <div class="form-group">
								<h5>Nomor Akun<span class="text-danger">*</span></h5>
								<div class="controls">
									<input type="number" name="account_number" class="form-control" required value="{{ old('account_number') }}">
									@if ($errors->has('account_number'))
										<div class="form-control-feedback text-danger">{{ $errors->first('account_number') }}</div>
									@endif
								</div>
							</div>

                            <div class="form-group">
								<h5>Bank<span class="text-danger">*</span></h5>
								<div class="controls">
									<select class="form-control" name="bank_id" id="bank_id" required>
                                        <option value="">--Pilih Bank--</option>
                                        @foreach ($bank as $row)
                                        <option value="{{ $row->id }}" {{ @$data->bank_id == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                        @endforeach
                                    </select>
									@if ($errors->has('bank_id'))
										<div class="form-control-feedback text-danger">{{ $errors->first('bank_id') }}</div>
									@endif
								</div>
							</div>

							<div class="form-group">
								<h5>Status</h5>
								<div class="controls">
									<label class="switch">
										<input type="checkbox" name="enabled" value="1" class="switch-indicator">
										<span class="switch-indicator" value="0"></span>
                                    	<span class="switch-description" value="1">Aktif</span>
									</label>

										@if ($errors->has('enabled'))
										<div class="form-control-feedback text-danger">{{ $errors->first('enabled') }}</div>
										@endif
								</div>
							</div>
					    </div>
						<div class="text-xs-right">
							<a href="{{ route('bank_accounts.index') }}" type="button" class="waves-effect waves-light btn btn-outline btn-secondary mb-5"><i class="ti-trash"></i>&nbsp;Batal</a><span></span>
							<button type="submit" class="waves-effect waves-light btn btn-outline btn-primary mb-5"><i class="ti-save-alt"></i>&nbsp;Simpan</button>		
						</div>
					</form>

				</div>
				<!-- /.col -->
			  </div>
			  <!-- /.row -->
			</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->
@section('bottom-resource')

@endsection


@endsection