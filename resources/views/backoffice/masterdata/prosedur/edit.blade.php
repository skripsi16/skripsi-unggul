@extends('backoffice.layouts.app')
@section('content')
    		<!-- Main content -->
<section class="content">

    <!-- Basic Forms -->
	<div class="box">
		<div class="box-header with-border">
			<h4 class="box-title">Edit Prosedur</h4>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			  <div class="row">
				<div class="col">
					<form action="{{route('prosedurs.update',$data->id)}}" method="POST" class="error">
						@csrf
						{{ method_field('PUT') }}
					  <div class="row">
						<div class="col-12">						
							<div class="form-group">
								<h5>Nama<span class="text-danger">*</span></h5>
								<div class="controls">
									<input type="text" name="name" class="form-control" required value="{{$data->name}}">
									@if ($errors->has('name'))
										<div class="form-control-feedback text-danger">{{ $errors->first('name') }}</div>
									@endif
								</div>
							</div>
							<div class="form-group">
								<h5>Deskripsi</h5>
								<div class="controls">
									<input type="text" name="description" class="form-control" value="{{$data->description}}">
									@if ($errors->has('description'))
										<div class="form-control-feedback text-danger">{{ $errors->first('description') }}</div>
									@endif
								</div>
							</div>
							<div class="form-group">
								<h5>Status</h5>
								<div class="controls">
									<label class="switch">
										<input type="checkbox" name="enabled" value="1" {{ $data->enabled == 1 ? 'checked="checked"' : '' }} class="switch-indicator">
										<span class="switch-indicator" value="0"></span>
                                    	<span class="switch-description" value="1">Aktif</span>
									</label>

										@if ($errors->has('enabled'))
										<div class="form-control-feedback text-danger">{{ $errors->first('enabled') }}</div>
										@endif
								</div>
							</div>
						</div>
						
					  </div>
						<div class="text-xs-right">
							<a href="{{route('prosedurs.index')}}" type="button" class="waves-effect waves-light btn btn-outline btn-secondary mb-5"><i class="ti-trash"></i>&nbsp;Batal</a><span></span>
							<button type="submit" class="waves-effect waves-light btn btn-outline btn-primary mb-5"><i class="ti-save-alt"></i>&nbsp;Simpan</button>		
						</div>
					</form>

				</div>
				<!-- /.col -->
			  </div>
			  <!-- /.row -->
			</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->

@endsection