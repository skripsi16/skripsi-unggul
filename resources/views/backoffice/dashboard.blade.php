@extends('backoffice.layouts.app')

@section('content')
<section class="content">
        <div class="row">
            <div class="col-xl-3">
                <a href="#" class="box bg-danger bg-hover-danger">
                    <div class="box-body">
                        <span class="text-white icon-Gift font-size-40"><span class="path1"></span><span
                                class="path2"></span></span>
                        <div class="text-white font-weight-600 font-size-18 mb-2 mt-5">Data Pendaftar</div>
                        <div class="text-white font-size-16">{{ $sumRegister }} Pendaftar</div>
                        {{-- <div class="text-white font-size-16">15</div> --}}
                    </div>
                </a>
            </div>
            <div class="col-xl-3">
                <a href="#" class="box bg-primary bg-hover-info">
                    <div class="box-body">
                        <span class="text-white icon-Gift font-size-40"><span class="path1"></span><span
                                class="path2"></span></span>
                        <div class="text-white font-weight-600 font-size-18 mb-2 mt-5">Data Siswa/Santri</div>
                        <div class="text-white font-size-16">{{ $sumStudent }} Orang</div>
                        {{-- <div class="text-white font-size-16">15</div> --}}
                    </div>
                </a>
            </div>
            <div class="col-xl-3">
                <a href="#" class="box bg-success bg-hover-danger">
                    <div class="box-body">
                        <span class="text-white icon-Gift font-size-40"><span class="path1"></span><span
                                class="path2"></span></span>
                        <div class="text-white font-weight-600 font-size-18 mb-2 mt-5">Data Laki-Laki</div>
                        <div class="text-white font-size-16">{{ $sumStudentMan }}</div>
                        {{-- <div class="text-white font-size-16">15</div> --}}
                    </div>
                </a>
            </div>
            <div class="col-xl-3">
                <a href="#" class="box bg-warning bg-hover-success">
                    <div class="box-body">
                        <span class="text-white icon-Gift font-size-40"><span class="path1"></span><span
                                class="path2"></span></span>
                        <div class="text-white font-weight-600 font-size-18 mb-2 mt-5">Data Perempuan</div>
                        <div class="text-white font-size-16">{{ $sumStudentWoman }}</div>
                        {{-- <div class="text-white font-size-16">15</div> --}}
                    </div>
                </a>
            </div>
        </div>
</section>
@endsection
