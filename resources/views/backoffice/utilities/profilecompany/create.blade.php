
@extends('backoffice.layouts.app')

@section('content')

<section class="content">

    <!-- Basic Forms -->
        <div class="box">
            <div class="box-header with-border">
                <h4 class="box-title">Data Perusahaan</h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                  <div class="row">
                    <div class="col">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                      <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="true">Profile</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" id="pills-kontak-tab" data-toggle="pill" href="#pills-kontak" role="tab" aria-controls="pills-kontak" aria-selected="false">Kontak</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" id="pills-visi-misi-tab" data-toggle="pill" href="#pills-visi-misi" role="tab" aria-controls="pills-visi-misi" aria-selected="false">Visi Misi</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-legalitas-tab" data-toggle="pill" href="#pills-legalitas" role="tab" aria-controls="pills-legalitas" aria-selected="false">Legalitas</a>
                                      </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-sejarah-tab" data-toggle="pill" href="#pills-sejarah" role="tab" aria-controls="pills-sejarah" aria-selected="false">Sejarah</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-manajemen-tab" data-toggle="pill" href="#pills-manajemen" role="tab" aria-controls="pills-manajemen" aria-selected="false">Manajemen</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-syarat-dan-ketentuan-tab" data-toggle="pill" href="#pills-syarat-dan-ketentuan" role="tab" aria-controls="pills-syarat-dan-ketentuan" aria-selected="false">Syarat dan Ketentuan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-kebijakan-tab" data-toggle="pill" href="#pills-kebijakan" role="tab" aria-controls="pills-kebijakan" aria-selected="false">Kebijakan</a>
                                    </li>
                                </ul>
                                <hr>
                                <form action="{{ @$data->id ? route('profile-company.update', @$data->id) : route('profile-company.store') }}" method="POST" class="form form-horizontal">
                                    @csrf
                                    <input type="hidden" name="_method" value="{{ @$data->id ? 'PUT' : 'POST'}}">
                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-2">Profile</label>
                                                        <div class="col-lg-10">
                                                            <textarea class="form-control" name="profile" id="description">{!! @$data->id ? $staticPage['profile']: old('profile') !!}</textarea>
                                                            <!--<textarea name="profile" id="description" class="form-control" placeholder="Profile Yayasan">{!! @$data->id ? $staticPage['profile']: old('profile') !!}</textarea>-->
                                                            @if ($errors->has('route'))
                                                                <small style="color: red;">{{ $errors->first('route') }}</small>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pills-kontak" role="tabpanel" aria-labelledby="pills-kontak-tab">
                                            <div class="form-group">
                                                <label>Nama</label>
                                                <input type="text" name="name" class="form-control" placeholder="Nama Yayasan" value="{{ @$data->id ? $data->name: old('name') }}">
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Alamat</label>
                                                    <textarea class="form-control" name="address1" placeholder="Alamat Yayasan">{!! @$data->id ? $address['address1']: old('address1') !!}</textarea>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Alamat Alternatif</label>
                                                    <textarea class="form-control" name="address2" placeholder="Alamat Alternatif Yayasan">{!! @$data->id ? $address['address2']: old('address2') !!}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>No Telp</label>
                                                    <input class="form-control" name="phone_1" placeholder="No Telp Yayasan" value="{!! @$data->id ? $phone_number['phone_1']: old('phone_1') !!}">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>No Telp Alternatif</label>
                                                    <input class="form-control" name="phone_2" placeholder="No Telp Alternatif Yayasan" value="{!! @$data->id ? $phone_number['phone_2']: old('phone_2') !!}">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Email</label>
                                                    <input class="form-control" name="email1" placeholder="Email Yayasan" value="{!! @$data->id ? $email['email1']: old('email1') !!}">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Email Alternatif</label>
                                                    <input class="form-control" name="email2" placeholder="Email Alternatif Yayasan" value="{!! @$data->id ? $email['email2']: old('email2') !!}">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Website</label>
                                                    <input class="form-control" name="website1" placeholder="Website Yayasan" value="{!! @$data->id ? $website['website1']: old('website1') !!}">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Website Alternatif</label>
                                                    <input class="form-control" name="website2" placeholder="Website Alternatif Yayasan" value="{!! @$data->id ? $website['website2']: old('website2') !!}">
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label>Facebook</label>
                                                    <input class="form-control" name="facebook" placeholder="Facebook Yayasan" value="{!! @$data->id ? $social_media['facebook']: old('facebook') !!}">
                                                    </div>
                                                <div class="form-group col-md-4">
                                                    <label>Instagram</label>
                                                    <input class="form-control" name="instagram" placeholder="Instagram Yayasan" value="{!! @$data->id ? $social_media['instagram']: old('instagram') !!}">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Youtube</label>
                                                    <input class="form-control" name="youtube" placeholder="Youtube Yayasan" value="{!! @$data->id ? $social_media['youtube']: old('youtube') !!}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pills-visi-misi" role="tabpanel" aria-labelledby="pills-visi-misi-tab">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-2">Visi Misi</label>
                                                        <div class="col-lg-10">
                                                            <textarea class="form-control" name="visimisi" id="description">{!! @$data->id ? $staticPage['visimisi']: old('visimisi') !!}</textarea>
                                                            <!--<textarea name="visimisi" id="description" class="form-control" placeholder="Visi Misi Yayasan">{!! @$data->id ? $staticPage['visimisi']: old('visimisi') !!}</textarea>-->
                                                            @if ($errors->has('route'))
                                                                <small style="color: red;">{{ $errors->first('route') }}</small>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pills-legalitas" role="tabpanel" aria-labelledby="pills-legalitas-tab">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-2">Legalitas</label>
                                                        <div class="col-lg-10">
                                                            <textarea class="form-control" name="legalitas" id="description">{!! @$data->id ? $staticPage['legalitas']: old('legalitas') !!}</textarea>
                                                            <!--<textarea name="legalitas" id="description" class="form-control" placeholder="Legalitas">{!! @$data->id ? $staticPage['legalitas']: old('legalitas') !!}</textarea>-->
                                                            @if ($errors->has('route'))
                                                                <small style="color: red;">{{ $errors->first('route') }}</small>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pills-sejarah" role="tabpanel" aria-labelledby="pills-sejarah-tab">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-2">Sejarah</label>
                                                        <div class="col-lg-10">
                                                            <textarea class="form-control" name="sejarah" id="description">{!! @$data->id ? $staticPage['sejarah']: old('sejarah') !!}</textarea>
                                                            <!--<textarea name="sejarah" id="description" class="form-control" placeholder="Sejarah Yayasan">{!! @$data->id ? $staticPage['sejarah']: old('sejarah') !!}</textarea>-->
                                                            @if ($errors->has('route'))
                                                                <small style="color: red;">{{ $errors->first('route') }}</small>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pills-manajemen" role="tabpanel" aria-labelledby="pills-manajemen-tab">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-2">Manajemen</label>
                                                        <div class="col-lg-10">
                                                            <textarea class="form-control" name="manajemen" id="description">{!! @$data->id ? $staticPage['manajemen']: old('manajemen') !!}</textarea>
                                                            <!--<textarea name="manajemen" id="description" class="form-control" placeholder="Manajemen Yayasan">{{ @$data->id ? $staticPage['manajemen']: old('manajemen') }}</textarea>-->
                                                            @if ($errors->has('route'))
                                                                <small style="color: red;">{{ $errors->first('route') }}</small>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pills-syarat-dan-ketentuan" role="tabpanel" aria-labelledby="pills-syarat-dan-ketentuan-tab">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-2">Syarat dan Ketentuan</label>
                                                        <div class="col-lg-10">
                                                            <textarea class="form-control" name="termcondition" id="description">{!! @$data->id ? $staticPage['termcondition']: old('termcondition') !!}</textarea>
                                                            <!--<textarea name="termcondition" id="description" class="form-control" placeholder="Syarat dan Ketentuan Yayasan">{{ @$data->id ? $staticPage['termcondition']: old('termcondition') }}</textarea>-->
                                                            @if ($errors->has('route'))
                                                                <small style="color: red;">{{ $errors->first('route') }}</small>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="pills-kebijakan" role="tabpanel" aria-labelledby="pills-kebijakan-tab">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-lg-2">Kebijakan</label>
                                                        <div class="col-lg-10">
                                                            <textarea class="form-control" name="privacy" id="description">{!! @$data->id ? $staticPage['privacy']: old('privacy') !!}</textarea>
                                                            <!--<textarea name="privacy" id="description" class="form-control" placeholder="Kebijakan">{{ @$data->id ? $staticPage['privacy']: old('privacy') }}</textarea>-->
                                                            @if ($errors->has('route'))
                                                                <small style="color: red;">{{ $errors->first('route') }}</small>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <a href="{{ route('profile-company.index') }}" type="button" class="btn btn-warning mr-1">
                                            <i class="fa fa-undo"></i> Back
                                        </a>
                                        <button type="submit" class="btn btn-info">
                                            <i class="fa fa-check-square-o"></i> Save
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>

@push('modal-scripts')
	<script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>

<script>
	CKEDITOR.replace( 'profile', {
		filebrowserUploadUrl: "{{route('profile-company.upload', ['_token' => csrf_token() ])}}",
		filebrowserUploadMethod: 'form'
	});

	CKEDITOR.replace( 'visimisi', {
		filebrowserUploadUrl: "{{route('profile-company.upload', ['_token' => csrf_token() ])}}",
		filebrowserUploadMethod: 'form'
	});

	CKEDITOR.replace( 'legalitas', {
		filebrowserUploadUrl: "{{route('profile-company.upload', ['_token' => csrf_token() ])}}",
		filebrowserUploadMethod: 'form'
	});

	CKEDITOR.replace( 'sejarah', {
		filebrowserUploadUrl: "{{route('profile-company.upload', ['_token' => csrf_token() ])}}",
		filebrowserUploadMethod: 'form'
	});

	CKEDITOR.replace( 'manajemen', {
		filebrowserUploadUrl: "{{route('profile-company.upload', ['_token' => csrf_token() ])}}",
		filebrowserUploadMethod: 'form'
	});

	CKEDITOR.replace( 'termcondition', {
		filebrowserUploadUrl: "{{route('profile-company.upload', ['_token' => csrf_token() ])}}",
		filebrowserUploadMethod: 'form'
	});

	CKEDITOR.replace( 'termfundraiser', {
		filebrowserUploadUrl: "{{route('profile-company.upload', ['_token' => csrf_token() ])}}",
		filebrowserUploadMethod: 'form'
	});

	CKEDITOR.replace( 'privacy', {
		filebrowserUploadUrl: "{{route('profile-company.upload', ['_token' => csrf_token() ])}}",
		filebrowserUploadMethod: 'form'
	});

</script>
@endpush

@endsection
