@extends('backoffice.layouts.app')
@section('content')
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="box">
                @if(Session::has('success'))
                <div class="alert alert-outline alert-success" role="alert">
                    <i data-feather="check" class="mg-r-10"></i> {{Session::get('success')}}
                </div>
                @elseif((Session::has('error')))
                <div class="alert alert-outline alert-danger" role="alert">
                    <i data-feather="alert-circle" class="mg-r-10"></i> {{Session::get('error')}}
                </div>
                @endif
                <div class="box-header with-border">
                    <h3 class="box-title">Data FAQ Deskripsi</h3>
                    <a href="{{ route('faq_descriptions.create') }}"
                        class="waves-effect waves-light btn btn-outline btn-primary float-right mb-2">Tambah Data</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(count($data) > 0)
                    <div class="table-responsive">
                        <div id="example_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                {{-- <form method="POST" action="">
                                    @csrf
                                <div id="example_filter" class="dataTables_filter">
                                <label>Search:<input type="search" name="param" class="form-control form-control-sm" placeholder="Input Name" aria-controls="example">
                                </label>
								<button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
                            </div>
							</form> --}}
                            <table class="table b-1 border-primary">
                                <thead class="bg-primary">
                                    <tr>
                                        <th>No</th>
                                        <th>FAQ Kategori</th>
                                        <th>Jawaban</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $i = 1;
                                    @endphp
                                    @foreach($data as $row)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{ $row->hasCategory->name }}</td>
                                        <td>{!! $row->answer !!}</td>
                                        <td>
                                            @if ($row->enabled == 1)
                                            <span class="badge badge-success">Active</span>
                                            @else
                                            <span class="badge badge-secondary">Not Active</span>
                                            @endif
                                        </td>
                                        <td class="btn-group">
                                            <a href="{{ route('faq_descriptions.edit', $row->encodeHash($row->id)) }}"
                                                class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                            @if($row->enabled == 1)
                                            <a href="{{ route('faq_descriptions.active_nonactive', [$row->encodeHash($row->id), 0]) }}"
                                                class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                                            @else
                                            <a href="{{ route('faq_descriptions.active_nonactive', [$row->encodeHash($row->id), 1]) }}"
                                                class="btn btn-primary btn-sm"><i class="fa fa-undo"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $data->links('backoffice.layouts.pagination') }}
                        </div>
                        <!-- /.box-body -->
                    </div>
                    @else
                        <H2>Data Tidak Ditemukan!</H2>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
