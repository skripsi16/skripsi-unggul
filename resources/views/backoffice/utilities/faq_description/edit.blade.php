@extends('backoffice.layouts.app')
@section('content')
    		<!-- Main content -->
<section class="content">

    <!-- Basic Forms -->
	<div class="box">
		<div class="box-header with-border">
			<h4 class="box-title">Edit FAQ Deksripsi</h4>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			  <div class="row">
				<div class="col">
					<form action="{{route('faq_descriptions.update',$data->id)}}" method="POST" class="error">
						@csrf
						{{ method_field('PUT') }}
					  <div class="row">
						<div class="col-12">						
                            <div class="form-group">
								<h5>Kategori<span class="text-danger">*</span></h5>
								<div class="controls">
									<select class="form-control" name="faq_categories_id" id="faq_categories_id" required>
                                        <option value="">--Pilih FAQ Kategori--</option>
                                        @foreach ($category as $row)
                                            <option value="{{ $row->id }}" {{ @$data->faq_categories_id == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                        @endforeach
									</select>
									<div class="help-block"></div>
									@if ($errors->has('faq_categories_id'))
										<div class="form-control-feedback text-danger">{{ $errors->first('faq_categories_id') }}</div>
									@endif
								</div>
							</div>
							<div class="form-group">
								<h5>Deskripsi</h5>
								<div class="controls">
                                    <textarea name="answer" id="summary-ckeditor" class="form-control">{!! $data->answer !!}</textarea>
									@if ($errors->has('answer'))
										<div class="form-control-feedback text-danger">{{ $errors->first('answer') }}</div>
									@endif
								</div>
							</div>
							<div class="form-group">
								<h5>Status</h5>
								<div class="controls">
									<label class="switch">
										<input type="checkbox" name="enabled" value="1" {{ $data->enabled == 1 ? 'checked="checked"' : '' }} class="switch-indicator">
										<span class="switch-indicator" value="0"></span>
                                    	<span class="switch-description" value="1">Aktif</span>
									</label>

										@if ($errors->has('enabled'))
										<div class="form-control-feedback text-danger">{{ $errors->first('enabled') }}</div>
										@endif
								</div>
							</div>
						</div>
						
					  </div>
						<div class="text-xs-right">
							<a href="{{route('faq_descriptions.index')}}" type="button" class="waves-effect waves-light btn btn-outline btn-secondary mb-5"><i class="ti-trash"></i>&nbsp;Batal</a><span></span>
							<button type="submit" class="waves-effect waves-light btn btn-outline btn-primary mb-5"><i class="ti-save-alt"></i>&nbsp;Simpan</button>		
						</div>
					</form>

				</div>
				<!-- /.col -->
			  </div>
			  <!-- /.row -->
			</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->
<script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
<script>
CKEDITOR.replace( 'summary-ckeditor', {
	filebrowserUploadUrl: "{{ route('upload_faq_descriptions', ['_token' => csrf_token() ])}}",
	filebrowserUploadMethod: 'form'
});
</script>
@endsection