@extends('frontoffice.layouts.app-user')

@section('top-resource')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

@endsection

@section('content')
@if($register->status == 'UNVERIFIED')
<section class="sc-form input-card">
      <div class="container-sm">
        <div class="input-card input-card--distance">
          <div class="sc-header">
            <h1>Info Pembayaran</h1>
          </div>
          <span class="input-card__subtitle">Silahkan melakukan pembayaran </span>
          <div class="invoice__total">
            <div class="rp" id="nom" value="{{ $register->hasPacket->harga }}">{{number_format($register->hasPacket->harga,0,'.','.')}},-
              {{-- <span class="fcolor-secondary">624</span> --}}
            </div>
            <span class="_copy" onclick="copyToClipboard('#nom')"> 
              <i class="fa fa-files-o"></i>
              <span>SALIN NOMINAL</span>
            </span>
          </div>
          <div class="invoice__warning"><strong>PENTING : </strong>Transfer tepas sampai 3 digit terakhir agar memudahkan tahap proses verifikasi (Nominal 3 digit terakhir akan di sedekahkan)</div><span class="input-card__subtitle">
             Pembayaran dilakukan ke rekening  <strong class="fcolor-base">{{ $register->hasBankAccount->account_number }}</strong> dibawah ini:</span>
          {{-- <div class="invoice__title-sm">Atas nama : {{ $register->hasBankAccount->name }}</div> --}}
          <div class="invoice__bank">
            <div class="_number">
              <div class="_anum" id="accNum">{{ $register->hasBankAccount->account_number }}
              </div>
              <span class="_copy" onclick="copyToClipboard('#accNum')"><i class="fa fa-files-o"></i><span>SALIN REKENING</span></span>
            </div>
          </div>
          <div><img src="{{asset('backoffice/assets/images/bank/'.  $register->hasBankAccount->hasBank->image)}}" class="img-thumbnail" width="100px" ></div>
          <span class="invoice__title-sm"> <div >{{ $register->hasBankAccount->account_name }}</div></span><br>
          <span class="input-card__subtitle">
             Transfer ke rekening diatas, sebelum 
             <strong class="fcolor-base">{{ \Carbon\Carbon::parse($register->register_date)->addDays(4)->format('d F Y') }} </strong>
             atau donasi akan di batalkan otomatis oleh sistem
          </span>
        </div>
      </div>
</section>
@elseif($register->status == 'VERIFIED')
<div class="input-card">
    <div class="sc-header">
      <h1>Data Orangtua</h1>
    </div>
    <div class="_wrapper">
        <div class="row">
            <div class="col">
                <h4>
                    {{ $parent->name_father }} (Ayah)</h4>
                <small>{{ $parent->father_phone_number }} <i class="fa fa-phone">
                </i></small>
                <p>
                    <i class="fa fa-graduation-cap"></i> {{ $parent->father_education }}
                    <br />
                    <i class="fa fa-briefcase"></i> {{ $parent->father_jobs }}
                    <br />
                    <i class="fa fa-credit-card"></i> {{ $parent->father_income }}
                </p>
            </div>
            <div class="col">
                <h4>
                    {{ $parent->name_mother }} (Ibu)</h4>
                <small>{{ $parent->mother_phone_number }} <i class="fa fa-phone">
                </i></small>
                <p>
                    <i class="fa fa-graduation-cap"></i> {{ $parent->mother_education }}
                    <br />
                    <i class="fa fa-briefcase"></i> {{ $parent->mother_jobs }}
                    <br />
                    <i class="fa fa-credit-card"></i> {{ $parent->mother_income }}
                </p>
            </div>
            @if($parent->name_guardian != null)
            <div class="col">
                <h4>
                    {{ ($parent->name_guardian) ?? '-' }} (Wali)</h4>
                <small>{{ ($parent->guardian_phone_number) ?? '-' }} <i class="fa fa-phone">
                    </i></small>
                <p>
                    <i class="fa fa-graduation-cap"></i> {{ ($parent->guardian_education) ?? '-' }}
                    <br />
                    <i class="fa fa-briefcase"></i> {{ ($parent->guardian_jobs) ?? '-' }}
                    <br />
                    <i class="fa fa-credit-card"></i> {{ ($parent->guardian_income) ?? '-' }}
                </p>
            </div>
            @endif
        </div>
    </div>
</div>
<br>
<div class="input-card">
    <div class="sc-header">
      <h1>Data Siswa</h1>
    </div>
    <div class="_wrapper">
        <div class="row">
            <div class="col-lg-6">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th col="row">Nama</th>
                            <td>{{ (Auth::guard('web')->user()->name) }}</td>
                        </tr>
                        <tr>
                            <th col="row">NISN</th>
                            <td>{{ $data->nisn }}</td>
                        </tr>
                        <tr>
                            <th col="row">NIK</th>
                            <td>{{ $data->nik }}</td>
                        </tr>
                        <tr>
                            <th col="row">Negara</th>
                            <td>{{ ($data->country->name ?? '-') }}</td>
                        </tr>
                        <tr>
                            <th col="row">Provinsi</th>
                            <td>{{ $data->province->name }}</td>
                        </tr>
                        <tr>
                            <th col="row">Kota</th>
                            <td>{{ $data->city->name }}</td>
                        </tr>
                        <tr>
                            <th col="row">Kelurahan</th>
                            <td>{{ $data->district->name }}</td>
                        </tr>
                        <tr>
                            <th col="row">Kecamatan</th>
                            <td>{{ $data->area->name }}</td>
                        </tr>
                        <tr>
                            <th col="row">Alamat</th>
                            <td>{{ $data->address }}</td>
                        </tr>

                       
                    </tbody> 
                </table>
            </div>
            <div class="col-lg-6">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th col="row">Nomor Hp</th>
                            <td>{{ $data->phone_number }}</td>
                        </tr>
                        <tr>
                            <th col="row">Email</th>
                            <td>{{ (Auth::guard('web')->user()->email) }}</td>
                        </tr>
                        <tr>
                            <th col="row">Gender</th>
                            <td>{{ $data->gender }}</td>
                        </tr>
                        <tr>
                            <th col="row">Tanggal Lahir</th>
                            <td>{{ $data->birth_date }}</td>
                        </tr>
                        <tr>
                            <th col="row">Agama</th>
                            <td>{{ $data->religion }}</td>
                        </tr>
                        <tr>
                            <th col="row">Keluarga</th>
                            @if($data->status_of_family == 'anak_kandung')
                            <td>ANAK KANDUNG</td>
                            @else
                            <td>ANAK ANGKAT</td>
                            @endif
                        </tr>
                        <tr>
                            <th col="row">NPSN</th>
                            <td>{{ $school->npsn }}</td>
                        </tr>
                        <tr>
                            <th col="row">Sekolah Asal</th>
                            <td>{{ $school->school_name }}</td>
                        </tr>
                        <tr>
                            <th col="row">Ujian</th>
                            <td>{{ $school->exam }}</td>
                        </tr>
                        <tr>
                            <th col="row">Alamat Sekolah</th>
                            <td>{{ $school->address_of_school }}</td>
                        </tr>
                       
                    </tbody> 
                </table>
            </div>
        </div>
    </div>
</div>
@endif

@endsection

