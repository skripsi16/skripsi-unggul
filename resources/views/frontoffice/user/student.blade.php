@extends('frontoffice.layouts.app-user')

@section('top-resource')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
@endsection

@section('content')

<style>
    .costum::placeholder {
        font-size: 14px;
    }  
</style>

<div class="input-card">
    <div class="sc-header">
      <h1>Data Siswa</h1>
    </div>
    @if(@$data->status == "VERIFIED")
    <div class="fitur-lite-card">
        <div class="fitur-lite-card__title"></div>
        <div class="fitur-lite-card__desc">
            <div class="_title">Data sudah tersimpan!</div>
        </div>
    </div>
    @else
    <div class="_wrapper mt-5">
        <form action="{{ @$data->id ? route('students.update', @$data->id) : route('students.store') }}" method="POST" class="row g-3">
            @csrf
            <input type="hidden" name="_method" value="{{ @$data->id ? 'PUT' : 'POST'}}">
                <hr>
                <div class="col-md-6">
                    <span class="input-pack__label">
                        Nama Lengkap <span style="color: red">*</span>
                    </span>
                    <input type="text" class="xs-input-control costum" value="{{ @$data->id ? $data->name: old('name') }}" name="name" id="name" placeholder="Masukkan Nama Lengkap" required>
                    @if ($errors->has('name'))
                        <small>
                            {{ $errors->first('name') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        NISN <span style="color: red">*</span>
                    </span>
                    <input type="text" class="xs-input-control costum" value="{{ @$data->id ? $data->nisn: old('nisn') }}" name="nisn" id="nisn" placeholder="Masukkan NISN" required>
                    @if ($errors->has('nisn'))
                        <small>
                            {{ $errors->first('nisn') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        NIK <span style="color: red">*</span>
                    </span>
                    <input type="text" class="xs-input-control costum" name="nik" value="{{ @$data->id ? $data->nik: old('nik') }}" id="nik" placeholder="Masukkan NIK" required>
                    @if ($errors->has('nik'))
                        <small>
                            {{ $errors->first('nik') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        Nomor Hp <span style="color: red">*</span>
                    </span>
                    <input type="text" class="xs-input-control costum" value="{{ @$data->id ? $data->phone_number: old('phone_number') }}" name="phone_number" id="phone_number" placeholder="Masukkan Nomor Hp" required>
                    @if ($errors->has('phone_number'))
                        <small>
                            {{ $errors->first('phone_number') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        Email <span style="color: red">*</span>
                    </span>
                    <input type="email" class="xs-input-control costum" value="{{ @$data->id ? $data->email: old('email') }}" name="email" id="email" placeholder="Masukkan Email" required>
                    @if ($errors->has('email'))
                        <small>
                            {{ $errors->first('email') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        Jenis Kelamin <span style="color: red">*</span>
                    </span>
                    <div class="input-wrap">
                        <i class="laz laz-caret-down"></i>
                        <select class="xs-input-control" name="gender">
                            <option value="">--Pilih Jenis Kelamin--</option>
                            <option value="laki-laki" {{ @$data->gender == 'laki-laki'? 'selected' : '' }}>Laki-Laki</option>
                            <option value="perempuan" {{ @$data->gender == 'perempuan'? 'selected' : '' }}>Perempuan</option>
                        </select> 
                    </div>
                    @if ($errors->has('gender'))
                        <small>
                            {{ $errors->first('gender') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        Tempat Lahir <span style="color: red">*</span>
                    </span>
                    <div class="input-wrap">
                        <i class="laz laz-caret-down"></i>
                        <select class="xs-input-control" name="birth_place">
                            <option value="">--Pilih Tempat Lahir--</option>
                            @foreach ($city as $row)
                            <option value="{{ $row->id }}" {{ @$data->birth_place == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                            @endforeach
                        </select> 
                    </div>
                    @if ($errors->has('birth_place'))
                        <small>
                            {{ $errors->first('birth_place') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        Tanggal Lahir <span style="color: red">*</span>
                    </span>
                    <input class="datepicker-input" type="text" placeholder="Masukan Tanggal Lahir" name="birth_date" required
                            value="{{ @$data->birth_date ? Carbon\Carbon::parse(@$data->birth_date)->format('d/m/Y') : '' }}">
                    @if ($errors->has('birth_date'))
                        <small>
                            {{ $errors->first('birth_date') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        Pilih Agama <span style="color: red">*</span>
                    </span>
                    <div class="input-wrap">
                        <i class="laz laz-caret-down"></i>
                        <select class="xs-input-control" name="religion">
                            <option value="">--Pilih Agama--</option>
                            <option value="islam" {{ @$data->religion == 'islam'? 'selected' : '' }}>Islam</option>
                            <option value="kristen" {{ @$data->religion == 'kristen'? 'selected' : '' }}>Kristen</option>
                            <option value="hindu" {{ @$data->religion == 'hindu'? 'selected' : '' }}>Hindu</option>
                            <option value="budha" {{ @$data->religion == 'budha'? 'selected' : '' }}>Budha</option>
                            <option value="katolik" {{ @$data->religion == 'katolik'? 'selected' : '' }}>Katolik</option>
                        </select> 
                    </div>
                    @if ($errors->has('religion'))
                        <small>
                            {{ $errors->first('religion') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        Pilih Status Keluarga <span style="color: red">*</span>
                    </span>
                    <div class="input-wrap">
                        <i class="laz laz-caret-down"></i>
                        <select class="xs-input-control" name="status_of_family">
                            <option value="">--Pilih Status--</option>
                            <option value="anak_kandung" {{ @$data->status_of_family == 'anak_kandung'? 'selected' : '' }}>Anak Kandung</option>
                            <option value="anak_pungut" {{ @$data->status_of_family == 'anak_pungut'? 'selected' : '' }}>Anak Pungut</option>
                        </select> 
                    </div>
                    @if ($errors->has('status_of_family'))
                        <small>
                            {{ $errors->first('status_of_family') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        Pilih Negara <span style="color: red">*</span>
                    </span>
                    <div class="input-wrap">
                        <i class="laz laz-caret-down"></i>
                        <select class="xs-input-control" name="country_id">
                            <option value="">--Pilih Negara--</option>
                            @foreach ($country as $row)
                            <option value="{{ $row->id }}" {{ @$data->country_id == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                            @endforeach
                        </select> 
                    </div>
                    @if ($errors->has('country_id'))
                        <small>
                            {{ $errors->first('country_id') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        Pilih Provinsi <span style="color: red">*</span>
                    </span>
                    <div class="input-wrap">
                        <i class="laz laz-caret-down"></i>
                        <select class="xs-input-control" id="province_id" name="province_id">
                            <option value="">--Pilih Provinsi--</option>
                            @foreach ($province as $row)
                            <option value="{{ $row->id }}" {{ @$data->province_id == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                            @endforeach
                        </select> 
                    </div>
                    @if ($errors->has('province_id'))
                        <small>
                            {{ $errors->first('province_id') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        Pilih Kota <span style="color: red">*</span>
                    </span>
                    <div class="input-wrap">
                        <i class="laz laz-caret-down"></i>
                        <select class="xs-input-control" id="city_id" name="city_id">
                            <option value="">--Pilih Kota--</option>
                            @foreach ($city as $row)
                            <option value="{{ $row->id }}" {{ @$data->city_id == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                            @endforeach
                        </select> 
                    </div>
                    @if ($errors->has('city_id'))
                        <small>
                            {{ $errors->first('city_id') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        Pilih Kecamatan <span style="color: red">*</span>
                    </span>
                    <div class="input-wrap">
                        <i class="laz laz-caret-down"></i>
                        <select class="xs-input-control" id="district_id" name="district_id">
                            <option value="">--Pilih Kecamatan--</option>
                        </select> 
                    </div>
                    @if ($errors->has('district_id'))
                        <small>
                            {{ $errors->first('district_id') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-12">
                    <span class="input-pack__label">
                        Pilih Kelurahan <span style="color: red">*</span>
                    </span>
                    <div class="input-wrap">
                        <i class="laz laz-caret-down"></i>
                        <select class="xs-input-control" id="area_id" name="area_id">
                            <option value="">--Pilih Kelurahan--</option>
                        </select> 
                    </div>
                    @if ($errors->has('area_id'))
                        <small>
                            {{ $errors->first('area_id') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-12">
                    <span class="input-pack__label">
                        Alamat <span style="color: red">*</span>
                    </span>
                    <textarea type="text" class="xs-input-control costum" name="address" id="address" required>{{ @$data->id ? $data->address: old('address') }}</textarea>
                    @if ($errors->has('address'))
                        <small>
                            {{ $errors->first('address') }}
                        </small>
                    @endif
                </div>

                <div class="sc-header">
                    <h1>Data Asal Sekolah</h1>
                </div>
                <hr>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        Nama Asal Sekolah <span style="color: red">*</span>
                    </span>
                    <input type="text" class="xs-input-control costum" value="{{ @$data->id ? $data->name_of_school: old('name_of_school') }}" name="name_of_school" id="name_of_school" placeholder="Masukkan Nama Asal Sekolah" required>
                    @if ($errors->has('name_of_school'))
                        <small>
                            {{ $errors->first('name_of_school') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        NPSN <span style="color: red">*</span>
                    </span>
                    <input type="text" class="xs-input-control costum" value="{{ @$data->id ? $data->npsn: old('npsn') }}" name="npsn" id="npsn" placeholder="Masukkan NPSN" required>
                    @if ($errors->has('npsn'))
                        <small>
                            {{ $errors->first('npsn') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        Pilih Status Sekolah <span style="color: red">*</span>
                    </span>
                    <div class="input-wrap">
                        <i class="laz laz-caret-down"></i>
                        <select class="xs-input-control" name="status_of_school">
                            <option value="">--Pilih Status--</option>
                            <option value="negeri" {{ @$data->status_of_school == 'negeri'? 'selected' : '' }}>Negeri</option>
                            <option value="swasta" {{ @$data->status_of_school == 'swasta'? 'selected' : '' }}>Swasta</option>
                        </select> 
                    </div>
                    @if ($errors->has('status_of_school'))
                        <small>
                            {{ $errors->first('status_of_school') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-6">
                    <span class="input-pack__label">
                        Pilih Ujian <span style="color: red">*</span>
                    </span>
                        <input type="text" class="xs-input-control" value="{{ @$data->id ? $data->exam: old('exam') }}" name="exam" id="exam" required>
                    @if ($errors->has('exam'))
                        <small>
                            {{ $errors->first('exam') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-12">
                    <span class="input-pack__label">
                        Tahun Lulus <span style="color: red">*</span>
                    </span>
                    <input type="date" class="xs-input-control" value="{{ @$data->id ? $data->graduation_year: old('graduation_year') }}" name="graduation_year" id="graduation_year" required>
                    @if ($errors->has('graduation_year'))
                        <small>
                            {{ $errors->first('graduation_year') }}
                        </small>
                    @endif
                </div>

                <div class="col-md-12">
                    <span class="input-pack__label">
                        Alamat Sekolah <span style="color: red">*</span>
                    </span>
                    <textarea class="xs-input-control" name="address_of_school" id="address_of_school" required>{{ @$data->id ? $data->address_of_school: old('address_of_school') }}</textarea>
                    @if ($errors->has('address_of_school'))
                        <small>
                            {{ $errors->first('address_of_school') }}
                        </small>
                    @endif
                </div>
            <input
                class="link-btn link-btn-primary link-btn-sm link-btn-w-full"
                type="submit"
                data-toggle="modal"
                value="{{ @$data->id ? 'UBAH' : 'SIMPAN'}}"
            />
        </form>
    </div>
    @endif
</div>


@endsection

@section('bottom-resource')
<script>
     $('#province_id').on('change',function(e){
		
        var province_id = e.target.value;
            var url = "{{route('students.cities', ':id')}}";
            url = url.replace(':id', province_id);
            $.get(url, function(data){
                $('#city_id').empty();
                    $.each(data, function(index, subcatObj){
                    $('#city_id').append('<option value="'+subcatObj.id+'">'+subcatObj.name+'</option>');
                });
            });
        });
    
    // Area District
    $('#city_id').on('change',function(e){

    var city_id = e.target.value;
        var url = "{{route('students.districts', ':id')}}";
        url = url.replace(':id', city_id);
        $.get(url, function(data){
            $('#district_id').empty();
                $.each(data, function(index, subcatObj){
                $('#district_id').append('<option value="'+subcatObj.id+'">'+subcatObj.name+'</option>');
            });
        });
    });
    
    // Area District
    $('#district_id').on('change',function(e){

    var district_id = e.target.value;
        var url = "{{route('students.area', ':id')}}";
        url = url.replace(':id', district_id);
        $.get(url, function(data){
            $('#area_id').empty();
                $.each(data, function(index, subcatObj){
                $('#area_id').append('<option value="'+subcatObj.id+'">'+subcatObj.name+'</option>');
            });
        });
    });
</script>
@endsection
