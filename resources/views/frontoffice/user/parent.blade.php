@extends('frontoffice.layouts.app-user')

@section('top-resource')
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<style>
    body{ 
    margin-top:40px; 
}

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
</style>

@endsection

@section('content')

<div class="input-card">
    <div class="sc-header">
      <h1>Data Orangtua</h1>
    </div>
    @if(@$data->status == "VERIFIED")
    <div class="fitur-lite-card">
        <div class="fitur-lite-card__title"></div>
        <div class="fitur-lite-card__desc">
            <div class="_title">Data sudah tersimpan!</div>
        </div>
    </div>
    @else
        <div class="container">
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#ayah" type="button" class="btn btn-primary btn-circle">1</a>
                        <p>Data Ayah</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#ibu" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p>Data Ibu</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#wali" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                        <p>Data Wali</p>
                    </div>
                </div>
            </div>
            <form action="{{ @$data->id ? route('parents.update', @$data->id) : route('parents.store') }}" method="POST" class="">
                @csrf
                <input type="hidden" name="_method" value="{{ @$data->id ? 'PUT' : 'POST'}}">
                <div class="row setup-content" id="ayah">
                    <div class="col-xs-12 mt-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Nama Ayah</label>
                                <input type="text" required="required" name="name_father" class="input-wrap" placeholder="Masukan Nama Ayah"  />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Pendidikan Ayah</label>
                                <select class="xs-input-control" required="required" name="father_education">
                                    <option>--Pilih Pendidikan Ayah--</option>
                                    @foreach ($education as $row)
                                        <option value="{{ $row->name }}" {{ @$data->father_education == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Pekerjaan Ayah</label>
                                <select class="xs-input-control" required="required" name="father_jobs">
                                    <option>--Pilih Pekerjaan Ayah--</option>
                                    @foreach ($job as $row)
                                        @if($row->category == 'Ayah')
                                        <option value="{{ $row->name }}" {{ @$data->father_jobs == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                        @endif
                                    @endforeach
                                </select> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Penghasilan Ayah</label>
                                <select class="xs-input-control" required="required" name="father_income">
                                    <option>--Pilih Penghasilan Ayah--</option>
                                    @foreach ($salary as $row)
                                        <option value="{{ $row->amount }}" {{ @$data->father_income == $row->id ? 'selected' : "" }}>{{ $row->amount }}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nomor Telepon Ayah</label>
                                <input type="text" required="required" name="father_phone_number" class="input-wrap" placeholder="Masukan Nomor Telepon Ayah"  />
                            </div>
                            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                        </div>
                    </div>
                </div>
                <div class="row setup-content" id="ibu">
                    <div class="col-xs-12 mt-3">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Nama Ibu</label>
                                <input type="text" required="required" name="name_mother" class="input-wrap" placeholder="Masukan Nama Ibu"  />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Pendidikan Ibu</label>
                                <select class="xs-input-control" required="required" name="mother_education">
                                    <option>--Pilih Pendidikan Ibu--</option>
                                    @foreach ($education as $row)
                                        <option value="{{ $row->name }}" {{ @$data->mother_education == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Pekerjaan Ibu</label>
                                <select class="xs-input-control" required="required" name="mother_jobs">
                                    <option>--Pilih Pekerjaan Ibu--</option>
                                    @foreach ($job as $row)
                                        @if($row->category == 'Ibu')
                                        <option value="{{ $row->name }}" {{ @$data->mother_jobs == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                        @endif
                                    @endforeach
                                </select> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Penghasilan Ibu</label>
                                <select class="xs-input-control" required="required" name="mother_income">
                                    <option>--Pilih Penghasilan Ibu--</option>
                                    @foreach ($salary as $row)
                                        <option value="{{ $row->amount }}" {{ @$data->mother_income == $row->id ? 'selected' : "" }}>{{ $row->amount }}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nomor Telepon Ibu</label>
                                <input type="text" required="required" name="mother_phone_number" class="input-wrap" placeholder="Masukan Nomor Telepon Ibu"  />
                            </div>
                            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                        </div>
                    </div>
                </div>
                <div class="row setup-content" id="wali">
                    <div class="col-xs-12 mt-3">
                        <div class="col-md-12">
                            <span>Catatan: Silahkan kosongkan jika tidak ada Wali!</span>
                            <div class="form-group">
                                <label class="control-label">Nama Wali</label>
                                <input type="text" name="name_guardian" class="input-wrap" placeholder="Masukan Nama Wali"  />
                            </div>
                            <div class="form-group">
                                <label class="control-label">Pendidikan Wali</label>
                                <select class="xs-input-control" name="guardian_education">
                                    <option value="">--Pilih Pendidikan Wali--</option>
                                    @foreach ($education as $row)
                                        <option value="{{ $row->name }}" {{ @$data->guardian_education == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Pekerjaan Wali</label>
                                <select class="xs-input-control" name="guardian_jobs">
                                    <option value="">--Pilih Pekerjaan Wali--</option>
                                    @foreach ($job as $row)
                                        <option value="{{ $row->name }}" {{ @$data->guardian_jobs == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Penghasilan Wali</label>
                                <select class="xs-input-control" name="guardian_income">
                                    <option value="">--Pilih Penghasilan Wali--</option>
                                    @foreach ($salary as $row)
                                        <option value="{{ $row->amount }}" {{ @$data->guardian_income == $row->id ? 'selected' : "" }}>{{ $row->amount }}</option>
                                    @endforeach
                                </select> 
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nomor Telepon Wali</label>
                                <input type="text" name="guardian_phone_number" class="input-wrap" placeholder="Masukan Nomor Telepon Wali"  />
                            </div>
                            <button class="btn btn-success btn-lg pull-right" type="submit">Finish!</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @endif
   
</div>


@endsection

@section('bottom-resource')
<script>
    $(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
    });
</script>

@endsection
