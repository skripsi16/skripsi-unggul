<!DOCTYPE HTML>
<!--
	Justice by freehtml5.co
	Twitter: http://twitter.com/fh5co
	URL: http://freehtml5.co
-->
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Thoolibuut Taqwa</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by FreeHTML5.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="FreeHTML5.co" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400, 900" rel="stylesheet"> -->

	<!-- Theme style  -->
	<link rel="stylesheet" href="{{ asset('frontoffice/assets/css/style.css') }}">

	<link rel="icon" href="{{ asset('frontoffice/assets/images/logo.png') }}">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="{{ asset('frontoffice/assets/css/animate.css') }}">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{ asset('frontoffice/assets/css/icomoon.css') }}">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="{{ asset('frontoffice/assets/css/themify-icons.css') }}">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{ asset('frontoffice/assets/css/bootstrap.css') }}">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="{{ asset('frontoffice/assets/css/magnific-popup.css') }}">
	<!-- Owl Carousel  -->
	{{-- <link rel="stylesheet" href="{{ asset('frontoffice/assets/css/owl.carousel.min.css') }}"> --}}
	<link rel="stylesheet" href="{{ asset('frontoffice/assets/css/owl.theme.default.min.css') }}">
	<!-- Flexslider -->
	{{-- <link rel="stylesheet" href="{{ asset('frontoffice/assets/css/flexslider.css') }}"> --}}

	<!-- Modernizr JS -->
	<script src="{{ asset('frontoffice/assets/js/modernizr-2.6.2.min.js') }}"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">
	<nav class="gtco-nav" role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-sm-2 col-xs-12">
					<div id="gtco-logo"><a href="{{ route('frontoffice') }}">Thoolibuut<em>Taqwa</em></a></div>
				</div>
				<div class="col-xs-10 text-right menu-1 main-nav">
					<ul>
						<li class="active"><a href="#" data-nav-section="home">Home</a></li>
						<li><a href="#" data-nav-section="informasi">Informasi</a></li>
						<li><a href="#" data-nav-section="alur-pendaftaran">Alur Pendaftaran</a></li>
						<li><a href="#" data-nav-section="prosedur-pendaftaran">Prosedur Pendaftaran</a></li>
						<li class="btn-cta"><a href="#" data-nav-section="contact"><span>Contact</span></a></li>
						<!-- For external page link -->
						<!-- <li><a href="http://freehtml5.co/" class="external">External</a></li> -->
					</ul>
				</div>
			</div>
			
		</div>
	</nav>

	<section id="gtco-hero" class="gtco-cover" style="height: auto" data-section="home"  data-stellar-background-ratio="0.5">
		{{-- <div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-center">
					<div class="display-t">
						<div class="display-tc">
							<h1 class="animate-box" data-animate-effect="fadeIn">The Greatest Firm You Can Trust</h1>
							<p class="gtco-video-link animate-box" data-animate-effect="fadeIn"><a href="https://vimeo.com/channels/staffpicks/93951774" class="popup-vimeo"><i class="icon-controller-play"></i></a></p>
						</div>
					</div>
				</div>
			</div>
		</div> --}}
		<div id="my-pics" class="carousel slide" data-ride="carousel">

			<!-- Content -->
			<div class="carousel-inner" role="listbox">
			
				@foreach($slider as $key => $row)
				<!-- Slide 1 -->
				<div class="item {{ $key == 0 ? 'active' : '' }}">
					@if($row->category == 'img')
						<img class="gtco-cover img-responsive" src="{{ asset('backoffice/assets/images/slider/'. $row->image) }}" alt="{{ $row->image }}">
					@else
						<iframe class="gtco-cover responsive-iframe" src="{{ $row->link }}" frameborder="0" allowfullscreen></iframe>
					@endif
				</div>
				@endforeach
			
			</div>
			
			<!-- Previous/Next controls -->
			<a class="left carousel-control" href="#my-pics" role="button" data-slide="prev">
				<span class="icon-prev" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#my-pics" role="button" data-slide="next">
				<span class="icon-next" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
			
		</div>
		{{-- @foreach ($slider as $row)
			<iframe class="gtco-cover" src="{{ $row->link }}" frameborder="0" allowfullscreen></iframe>
		@endforeach --}}
	</section>
	
	<section id="gtco-about" data-section="informasi">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-8 col-md-offset-2 heading animate-box" data-animate-effect="fadeIn">
					<h1>SD-SMA PESANTREN TAHFIZH DAN BAHASA ARAB THOOLIBUUT TAQWA</h1>
					<a href="{{ route('auth-user.register') }}" type="button" class="btn-success btn-lg">Daftar</a>
					<a href="{{ route('auth-user.login') }}" type="button" class="btn-success btn-lg">Login</a>
					{{-- <button type="button" class="btn-success btn-lg">Login</button> --}}
					{{-- <p class="sub">Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
					<p class="subtle-text animate-box" data-animate-effect="fadeIn">Welcome</p> --}}
				</div>
			</div>
			<div class="row">
				{{-- <div class="col-md-6 col-md-pull-1 animate-box" data-animate-effect="fadeInLeft">
					<div class="img-shadow">
						<img src="{{ asset('frontoffice/assets/images/img_1.jpg') }}" class="img-responsive" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
					</div>
				</div> --}}
				@foreach($faqDescription as $row)
				<div class="col-md-6 animate-box" data-animate-effect="fadeInLeft">
					<h2 class="heading-colored">{{ $row->hasCategory->name }}</h2>
					<p>{!! $row->answer !!}</p>
				</div>
				
				@endforeach
			</div>
		</div>
	</section>

	<section id="gtco-practice-areas" data-section="alur-pendaftaran">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-8 col-md-offset-2 heading animate-box" data-animate-effect="fadeIn">
					<h1>Alur Pendaftaran</h1>
					<p class="sub"></p>
				</div>
				<img class="img-responsive" src="{{ asset('frontoffice/assets/images/alur-pendaftaran.png') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co">
			</div>
			
		</div>
	</section>

	<section id="gtco-our-team" data-section="prosedur-pendaftaran">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-8 col-md-offset-2 heading animate-box" data-animate-effect="fadeIn">
					<h1>Penjelasan Prosedur Pendaftaran</h1>
					<p class="sub">Pastikan Ayah/Bunda/Ananda Calon Santri Membaca Syarat dan Ketentuan Pendaftaran online melalui website thoolibuttaqwa.com</p>
					{{-- <p class="subtle-text animate-box" data-animate-effect="fadeIn">Our Team</p> --}}
				</div>
			</div>
			<div class="row team-item gtco-team-reverse">
				<div style="display: flex; justify-content: center;">
					<ol style="font-size:18px; text-align:justify;">
						@foreach($prosedur as $row)
						<li>{{ $row->description }}</li>
						@endforeach
					</ol>
				</div>
			</div>

		</div>
	</section>

	<section id="gtco-our-team" data-section="">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-8 col-md-offset-2 heading animate-box" data-animate-effect="fadeIn">
					<h1>Support</h1>
					<p class="sub"></p>
					{{-- <p class="subtle-text animate-box" data-animate-effect="fadeIn">Our Team</p> --}}
				</div>
			</div>
			<div class="row team-item gtco-team-reverse">
				<div class="row" style="margin: auto;">
					@foreach($brand as $row)
					<div class="col-3 col-md-3"><img src="{{ asset('backoffice/assets/images/brand/'. $row->image) }}" alt="" width="100"></div>
					@endforeach
				</div>
			</div>

		</div>
	</section>


	<section id="gtco-contact" data-section="contact">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-8 col-md-offset-2 heading animate-box" data-animate-effect="fadeIn">
					<h1>Kontak Kami</h1>
					<p class="sub">PESANTREN THOOLIBUUT TAQWA</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-push-6 animate-box">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.610903119888!2d106.6852196147689!3d-6.182800195523885!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f9acf1ac30c1%3A0x6b49250d05d1bde6!2sPesantren%20Tahfidz%20%26%20Bahasa%20Arab%20Thoolibuut%20Taqwa!5e0!3m2!1sid!2sid!4v1628220215824!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
				</div>
				<div class="col-md-4 col-md-pull-6 animate-box">
					<div class="gtco-contact-info">
						<ul>
							<li class="address">Jl. Pinus I No.139, RT.004/RW.005, Cipondoh Indah, Kec. Cipondoh, Kota Tangerang, Banten 15148</li>
							<li class="phone">Pusat Tanggerang :  0819-9868-8000</li>
							<li class="email"><a href="mailto:info@thoolibuutaqwa.com">info@thoolibuutaqwa.com</a></li>
							<li class="url"><a href="https://thoolibuttaqwa.com">https://thoolibuttaqwa.com</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="{{ asset('frontoffice/assets/js/jquery.min.js') }}"></script>
	<!-- jQuery Easing -->
	<script src="{{ asset('frontoffice/assets/js/jquery.easing.1.3.js') }}"></script>
	<!-- Bootstrap -->
	<script src="{{ asset('frontoffice/assets/js/bootstrap.min.js') }}"></script>
	<!-- Waypoints -->
	<script src="{{ asset('frontoffice/assets/js/jquery.waypoints.min.js') }}"></script>
	<!-- Stellar -->
	<script src="{{ asset('frontoffice/assets/js/jquery.stellar.min.js') }}"></script>
	<!-- Magnific Popup -->
	<script src="{{ asset('frontoffice/assets/js/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset('frontoffice/assets/js/magnific-popup-options.js') }}"></script>
	<!-- Main -->
	<script src="{{ asset('frontoffice/assets/js/main.js') }}"></script>

	<script>
		// Initialize tooltip component
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})

		// Initialize popover component
	$(function () {
		$('[data-toggle="popover"]').popover()
	})
	</script>

	</body>
</html>

