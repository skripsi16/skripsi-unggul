<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('frontoffice/assets/images/logo.png') }}">

    <title>Form Pendaftaran </title>
  
	<!-- Vendors Style-->
	<link rel="stylesheet" href="{{asset('backoffice/assets/css/vendors_css.css')}}">
	  
	<!-- Style-->  
	<link rel="stylesheet" href="{{asset('backoffice/assets/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('backoffice/assets/css/skin_color.css')}}">
    <link rel="stylesheet" href="{{asset('backoffice/assets/css/custom.css')}}">
</head>
<body class="hold-transition light-skin sidebar-mini theme-primary">
	
<div class="wrapper mt-4">
  
  <!-- Content Wrapper. Contains page content -->
    <div class="col-12 mt-4">
        <div class="container">
            @if ($message = Session::get('success'))
                <div class="alert alert-success mt-2">
                    <p>{{ $message }}</p>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-danger mt-2">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <!-- Main content -->
            <section class="content col-md-10">
                <!-- Validation wizard -->
                <div class="box box-default">
                    <div class="box-header with-border">
                    <h4 class="box-title">Pendaftaran</h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body wizard-content">
                        <form action="{{ route('register-form') }}" class="validation-wizard wizard-circle" enctype="multipart/form-data" method="POST">
                            @csrf
                            <!-- Step 1 -->
                            <h6>Siswa</h6>
                            <section>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" style="color: black"> Nama Lengkap <span class="danger">*</span> </label>
                                            <input type="text" class="form-control required" value="{{ old('name') }}" id="name" name="name"> </div>
                                            @if ($errors->has('name'))
                                                <small style="color: red;">{{ $errors->first('name') }}</small>
                                            @endif
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nisn" style="color: black"> NISN <span class="danger">*</span> </label>
                                            <input type="text" class="form-control required" value="{{ old('nisn') }}" id="nisn" name="nisn"> </div>
                                            @if ($errors->has('nisn'))
                                                <small style="color: red;">{{ $errors->first('nisn') }}</small>
                                            @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nik" style="color: black"> NIK <span class="danger">*</span> </label>
                                            <input type="text" class="form-control required" value="{{ old('nik') }}" id="nik" name="nik"> </div>
                                            @if ($errors->has('nik'))
                                                <small style="color: red;">{{ $errors->first('nik') }}</small>
                                            @endif
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="phone_number" style="color: black"> Nomor HP <span class="danger">*</span> </label>
                                            <input type="text" class="form-control required" value="{{ old('phone_number') }}" id="phone_number" name="phone_number"> </div>
                                            @if ($errors->has('phone_number'))
                                                <small style="color: red;">{{ $errors->first('phone_number') }}</small>
                                            @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email" style="color: black"> Email <span class="danger">*</span> </label>
                                            <input type="email" class="form-control required" value="{{ old('email') }}" id="email" name="email"> </div>
                                            @if ($errors->has('email'))
                                                <small style="color: red;">{{ $errors->first('email') }}</small>
                                            @endif
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="password" style="color: black"> Password <span class="danger">*</span> </label>
                                            <input type="password" class="form-control required" id="password" name="password"> </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="status_of_family" style="color: black"> Status di Keluarga<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="status_of_family" name="status_of_family">
                                                <option value="">--Pilih Status--</option>
                                                <option value="anak_kandung" @if (old('status_of_family') == 'anak_kandung') selected="selected" @endif>Anak Kandung</option>
                                                <option value="anak_angkat" @if (old('status_of_family') == 'anak_angkat') selected="selected" @endif>Anak Angkat</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="gender" style="color: black"> Pilih Jenis Kelamin <span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="gender" name="gender">
                                                <option value="">--Pilih Jenis Kelamin--</option>
                                                <option value="Laki-laki" @if (old('gender') == 'Laki-laki') selected="selected" @endif>Laki-Laki</option>
                                                <option value="Perempuan" @if (old('gender') == 'Perempuan') selected="selected" @endif>Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="birth_place" style="color: black"> Tempat Lahir<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="birth_place" name="birth_place">
                                                <option value="">--Pilih Kota--</option>
                                                @foreach ($city as $row)
                                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="birth_date" style="color: black">Tanggal Lahir</label>
                                            <input type="date" class="form-control" value="{{ old('birth_date') }}" name="birth_date" id="birth_date"> </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="religion" style="color: black"> Pilih Agama <span class="danger">*</span> </label>
                                                <select class="custom-select form-control required" id="religion" name="religion">
                                                    <option value="">--Pilih Agama--</option>
                                                    <option value="islam" @if (old('religion') == 'islam') selected="selected" @endif>Islam</option>
                                                    <option value="kristen" @if (old('religion') == 'kristen') selected="selected" @endif>Kristen</option>
                                                    <option value="hindu" @if (old('religion') == 'hindu') selected="selected" @endif>Hindu</option>
                                                    <option value="budha" @if (old('religion') == 'budha') selected="selected" @endif>Budha</option>
                                                    <option value="katolik" @if (old('religion') == 'katolik') selected="selected" @endif>Katolik</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="country_id" style="color: black"> Pilih Negara<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="country_id" name="country_id">
                                                <option value="">--Pilih Negara--</option>
                                                @foreach ($country as $row)
                                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="province_id" style="color: black"> Pilih Provinsi<span class="danger">*</span> </label>
                                            <select class="custom-select form-control province required" id="province_id" name="province_id">
                                                <option value="">--Pilih Provinsi--</option>
                                                @foreach ($province as $row)
                                                <option value="{{ $row->id }}" {{ @$data->province_id == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="city_id" style="color: black"> Pilih Kota <span class="danger">*</span> </label>
                                                <select class="custom-select form-control city required" id="city_id" name="city_id">
                                                    <option value="">--Pilih Kota--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="district_id" style="color: black"> Pilih Kecamatan<span class="danger">*</span> </label>
                                            <select class="custom-select form-control district required" id="district_id" name="district_id">
                                                <option value="">--Pilih Kecamatan--</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label for="area_id" style="color: black"> Pilih Kelurahan <span class="danger">*</span> </label>
                                                <select class="custom-select form-control area required" id="area_id" name="area_id">
                                                    <option value="">--Pilih Kelurahan--</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="address" style="color: black">Alamat Lengkap<span class="danger">*</span> </label>
                                            <textarea class="form-control required" name="address" id="" cols="30" rows="5">{{ Request::old('address') }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="image" style="color: black">Gambar<span class="danger">*</span> </label>
                                           <input type="file" class="form-control required" name="image">
                                        </div>
                                    </div>
                                </div>

                            </section>

                            <!-- Step 2 -->
                            <h6>Orangtua</h6>
                            <section>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name_father" style="color: black"> Nama Ayah <span class="danger">*</span> </label>
                                            <input type="text" class="form-control required" value="{{ old('name_father') }}" id="name_father" name="name_father"> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="father_phone_number" style="color: black"> Nomor Hp <span class="danger">*</span> </label>
                                            <input type="text" class="form-control required" value="{{ old('father_phone_number') }}" id="father_phone_number" name="father_phone_number"> </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="father_education" style="color: black"> Pilih Pendidikan<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" value="{{ old('father_education') }}" id="father_education" name="father_education">
                                                <option value="">--Pilih Pendidikan--</option>
                                                @foreach ($education as $row)
                                                    <option value="{{ $row->name }}" {{ @$data->father_education == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="father_jobs" style="color: black"> Pilih Pekerjaan<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" value="{{ old('father_jobs') }}" id="father_jobs" name="father_jobs">
                                                <option value="">--Pilih Pekerjaan--</option>
                                                @foreach ($job as $row)
                                                    @if($row->category == 'Ayah')
                                                    <option value="{{ $row->name }}" {{ @$data->father_jobs == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="father_income" style="color: black"> Pilih Penghasilan<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" value="{{ old('father_income') }}" id="father_income" name="father_income">
                                                <option value="">--Pilih Penghasilan--</option>
                                                @foreach ($salary as $row)
                                                    <option value="{{ $row->amount }}" {{ @$data->father_income == $row->id ? 'selected' : "" }}>{{ $row->amount }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <hr style="color: black">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name_mother" style="color: black"> Nama Ibu <span class="danger">*</span> </label>
                                            <input type="text" class="form-control required" value="{{ old('name_mother') }}" id="name_mother" name="name_mother"> </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="mother_phone_number" style="color: black"> Nomor Hp <span class="danger">*</span> </label>
                                            <input type="text" class="form-control required" value="{{ old('mother_phone_number') }}" id="mother_phone_number" name="mother_phone_number"> </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="mother_education" style="color: black"> Pilih Pendidikan<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" value="{{ old('mother_education') }}" id="mother_education" name="mother_education">
                                                <option value="">--Pilih Pendidikan--</option>
                                                @foreach ($education as $row)
                                                    <option value="{{ $row->name }}" {{ @$data->mother_education == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="mother_jobs" style="color: black"> Pilih Pekerjaan<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" value="{{ old('mother_jobs') }}" id="mother_jobs" name="mother_jobs">
                                                <option value="">--Pilih Pekerjaan--</option>
                                                @foreach ($job as $row)
                                                    @if($row->category == 'Ibu')
                                                    <option value="{{ $row->name }}" {{ @$data->mother_jobs == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="mother_income" style="color: black"> Pilih Penghasilan<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" value="{{ old('mother_income') }}" id="mother_income" name="mother_income">
                                                <option value="">--Pilih Penghasilan--</option>
                                                @foreach ($salary as $row)
                                                    <option value="{{ $row->amount }}" {{ @$data->mother_income == $row->id ? 'selected' : "" }}>{{ $row->amount }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <hr style="color: black">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input class="form-check-input" type="checkbox" value="" id="check">
                                        <label class="form-check-label" for="check">
                                          Jadikan Ayah sebagai Wali
                                        </label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name_guardian" style="color: black"> Nama Wali <span class="danger">*</span> </label>
                                            <input type="text" class="form-control required" value="{{ old('name_guardian') }}" id="name_guardian" name="name_guardian"> </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="guardian_phone_number" style="color: black"> Nomor Hp <span class="danger">*</span> </label>
                                            <input type="text" class="form-control required" value="{{ old('guardian_phone_number') }}" id="guardian_phone_number" name="guardian_phone_number"> </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="guardian_education" style="color: black"> Pilih Pendidikan<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="guardian_education" name="guardian_education">
                                                <option value="">--Pilih Pendidikan--</option>
                                                @foreach ($education as $row)
                                                    <option value="{{ $row->name }}" {{ @$data->guardian_education == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="guardian_jobs" style="color: black"> Pilih Pekerjaan<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="guardian_jobs" name="guardian_jobs">
                                                <option value="">--Pilih Pekerjaan--</option>
                                                @foreach ($job as $row)
                                                    <option value="{{ $row->name }}" {{ @$data->guardian_jobs == $row->id ? 'selected' : "" }}>{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="guardian_income" style="color: black"> Pilih Penghasilan<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="guardian_income" name="guardian_income">
                                                <option value="">--Pilih Penghasilan--</option>
                                                @foreach ($salary as $row)
                                                    <option value="{{ $row->amount }}" {{ @$data->guardian_income == $row->id ? 'selected' : "" }}>{{ $row->amount }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!-- Step 3 -->
                            <h6>Sekolah</h6>
                            <section>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="npsn" style="color: black"> NPSN <span class="danger">*</span> </label>
                                            <input type="text" class="form-control required" value="{{ old('npsn') }}" id="npsn" name="npsn"> </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="school_name" style="color: black"> Nama Sekolah <span class="danger">*</span> </label>
                                            <input type="text" class="form-control required" value="{{ old('school_name') }}" id="school_name" name="school_name"> </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="status_of_school" style="color: black"> Status Sekolah<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="status_of_school" name="status_of_school">
                                                <option value="">--Pilih Status--</option>
                                                <option value="NEGERI" @if (old('status_of_school') == 'NEGERI') selected="selected" @endif>Negeri</option>
                                                <option value="SWASTA" @if (old('status_of_school') == 'SWASTA') selected="selected" @endif>Swasta</option>
                                                <option value="HOMESCHOOL" @if (old('status_of_school') == 'HOMESCHOOL') selected="selected" @endif>Home School</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exam" style="color: black"> Model Ujian<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="exam" name="exam">
                                                <option value="">--Pilih Ujian--</option>
                                                <option value="UNBK" @if (old('exam') == 'UNBK') selected="selected" @endif>UNBK</option>
                                                <option value="UNKP" @if (old('exam') == 'UNKP') selected="selected" @endif>UNKP</option>
                                                <option value="LAINNYA" @if (old('exam') == 'LAINNYA') selected="selected" @endif>LAINNYA</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="graduation_year" style="color: black"> Tahun Lulus<span class="danger">*</span> </label>
                                            <input type="date" class="form-control" value="{{ old('graduation_year') }}" name="graduation_year" id="graduation_year">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="country_id_school" style="color: black"> Pilih Negara<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="country_id_school" name="country_id_school">
                                                <option value="">--Pilih Negara--</option>
                                                @foreach ($country as $row)
                                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="province_id_school" style="color: black"> Pilih Provinsi<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="province_id_school" name="province_id_school">
                                                <option value="">--Pilih Provinsi--</option>
                                                @foreach ($province as $row)
                                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="city_id_school" style="color: black"> Pilih Kota<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="city_id_school" name="city_id_school">
                                                <option value="">--Pilih Kota--</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="district_id_school" style="color: black"> Pilih Kecamatan<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="district_id_school" name="district_id_school">
                                                <option value="">--Pilih Kecamatan--</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="area_id_school" style="color: black"> Pilih Kelurahan<span class="danger">*</span> </label>
                                            <select class="custom-select form-control area required" id="area_id_school" name="area_id_school">
                                                <option value="">--Pilih Kelurahan--</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="address_of_school" style="color: black">Alamat Lengkap<span class="danger">*</span> </label>
                                            <textarea class="form-control" name="address_of_school" id="address_of_school" cols="30" rows="5">{{ Request::old('address_of_school') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!-- Step 4 -->
                            <h6>Paket Pendidikan</h6>
                            <section>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="year" style="color: black"> Pilih Tahun<span class="danger">*</span> </label>
                                            <input type="date" class="form-control" value="{{ old('year') }}" name="year" id="year">
                                        </div>
                                    </div>
                                </div>
                              
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="branch" style="color: black"> Pilih Cabang<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="branch" name="branch">
                                                <option value="">--Pilih Cabang--</option>
                                                @foreach ($city as $row)
                                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="packet_id" style="color: black"> Pilih Paket<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="packet_id" name="packet_id">
                                                <option value="">--Pilih Paket--</option>
                                                @foreach ($packet as $row)
                                                <option value="{{ $row->id }}">{{ $row->name }} - Harga {{number_format($row->harga, 0,'.','.')}},-</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="financing" style="color: black"> Pilih Pembiayaan<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="financing" name="financing">
                                                <option value="">--Pilih Pembiayaan--</option>
                                                <option value="BAYARLUNAS" @if (old('financing') == 'BAYARLUNAS') selected="selected" @endif>Bayar Lunas</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="bank_id" style="color: black"> Pilih Akun Bank<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="bank_id" name="bank_id">
                                                <option value="">--Pilih Bank--</option>
                                                @foreach ($bank as $row)
                                                <option value="{{ $row->id }}">{{ $row->account_name }} - {{ $row->hasBank->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="category" style="color: black"> Pilih Kategori<span class="danger">*</span> </label>
                                            <select class="custom-select form-control required" id="category" name="category">
                                                <option value="">--Pilih Kategori--</option>
                                                @foreach ($category_packet as $row)
                                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </section>

                             <!-- Step 5 -->
                            <h6>Persyaratan</h6>
                            <section>
                                <ol class="custom-ordered-list">
                                    @foreach($categories as $row)
                                    <li class="custom-child-ordered-list">{{ $row->name }} 
                                        <br> {!! $row->description !!}
                                    </li>
                                    @endforeach
                                </ol>

                                <div class="col-md-6">
                                    <div class="form-group pl-10">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                                            Syarat & Ketentuan
                                        </button>
                                    </div>
                                </div>
                               

                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                    <label class="form-check-label" for="defaultCheck1">
                                    Apakah anda menyetujuinya?
                                    </label>
                                </div>
                            </section>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </section>
            <!-- /.content -->
        </div>
    </div>
  <!-- /.content-wrapper -->
  
</div>
<!-- ./wrapper -->
	
<!-- Button trigger modal -->
  <!-- Modal -->
  <div class="modal fade bd-example-modal-lg" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="color: black">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Persyaratan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="color: black">
            <p>Bismillahirrohmaanirrohiim</p>

            <p>Assalamu'alaikum Wr. Wb. <br>
            Ayah dan bunda yang dirahmati Allah. Mohon di baca secara seksama persyaratan menjadi santri Indent Pesantren Tahfizh Daarul Qur'an berikut ini:
            </p>

            <p class="text-center">SYARAT DAN KETENTUAN SANTRI INDENT <br>
            <b>PESANTREN TAHFIZH DAARUL QUR'AN</b> <br>

            Syarat dan ketentun santriindent dibuat untuk dipatuhi dan ikuti oleh peserta program santri indent Pesantren Tahfizh Daarul Qur'an, baik calon santri dan calon wali santri indent diharuskan untuk membaca syarat dan ketentuan di bawah ini: </p>
            
            <hr>
            @foreach($term as $row)
            <p class="text-center">{{ $row->title }}</p>
                {!! $row->description !!}
            @endforeach
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" style="float: right;" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
	
	
	<!-- Vendor JS -->
	<script src="{{asset('backoffice/assets/js/vendors.min.js')}}"></script>
    <script src="{{asset('backoffice/assets/icons/feather-icons/feather.min.js')}}"></script>	
    <script src="{{asset('backoffice/assets/vendor_components/jquery-steps-master/build/jquery.steps.js')}}"></script>
    <script src="{{asset('backoffice/assets/vendor_components/jquery-validation-1.17.0/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('backoffice/assets/vendor_components/sweetalert/sweetalert.min.js')}}"></script>
	
	<!-- Power BI Admin App -->
	<script src="{{asset('backoffice/assets/js/template.js')}}"></script>
	<script src="{{asset('backoffice/assets/js/demo.js')}}"></script>
	
    {{-- <script src="{{asset('backoffice/assets/js/pages/steps.js')}}"></script> --}}
    <script>
        var form = $(".validation-wizard").show();

    $(".validation-wizard").steps({
        headerTag: "h6", 
        bodyTag: "section", 
        transitionEffect: "none", 
        titleTemplate: '<span class="step">#index#</span> #title#', 
        labels: {
            finish: "Submit"
            }, onStepChanging: function (event, currentIndex, newIndex) {
                return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
            }, onFinishing: function (event, currentIndex) {
                return form.validate().settings.ignore = ":disabled", form.valid()
            }, onFinished: function (event, currentIndex) {
                // swal("Apakah yakin datanya benar?") {
                    var form = $(this);
                    form.submit();
                // }
            } 
    }), 
    $(".validation-wizard").validate({
        ignore: "input[type=hidden]", 
        errorClass: "text-danger", 
        successClass: "text-success", 
            highlight: function (element, errorClass) {
                $(element).removeClass(errorClass)
            }, 
            unhighlight: function (element, errorClass) {
                $(element).removeClass(errorClass)
            }, 
            errorPlacement: function (error, element) {
            error.insertAfter(element)
        }, rules: {
            email: {
                email: !0
            }
        }
    })
    </script>
	<script>
    $('.province').on('change',function(e){
    
    var province_id = e.target.value;
        var url = "{{route('register.cities', ':id')}}";
        url = url.replace(':id', province_id);
        $.get(url, function(data){
            $('#city_id').empty();
                $.each(data, function(index, subcatObj){
                $('#city_id').append('<option value="'+subcatObj.id+'">'+subcatObj.name+'</option>');
            });
        });
    });
    
    // Area District
    $('.city').on('change',function(e){

    var city_id = e.target.value;
        var url = "{{route('register.districts', ':id')}}";
        url = url.replace(':id', city_id);
        $.get(url, function(data){
            $('#district_id').empty();
                $.each(data, function(index, subcatObj){
                $('#district_id').append('<option value="'+subcatObj.id+'">'+subcatObj.name+'</option>');
            });
        });
    });
    
    // Area District
    $('.district').on('change',function(e){

    var district_id = e.target.value;
        var url = "{{route('register.area', ':id')}}";
        url = url.replace(':id', district_id);
        $.get(url, function(data){
            $('#area_id').empty();
                $.each(data, function(index, subcatObj){
                $('#area_id').append('<option value="'+subcatObj.id+'">'+subcatObj.name+'</option>');
            });
        });
    });

    $('#province_id_school').on('change',function(e){
    
    var province_id = e.target.value;
        var url = "{{route('register.cities', ':id')}}";
        url = url.replace(':id', province_id);
        $.get(url, function(data){
            $('#city_id_school').empty();
                $.each(data, function(index, subcatObj){
                $('#city_id_school').append('<option value="'+subcatObj.id+'">'+subcatObj.name+'</option>');
            });
        });
    });
    
    // Area District
    $('#city_id_school').on('change',function(e){

    var city_id = e.target.value;
        var url = "{{route('register.districts', ':id')}}";
        url = url.replace(':id', city_id);
        $.get(url, function(data){
            $('#district_id_school').empty();
                $.each(data, function(index, subcatObj){
                $('#district_id_school').append('<option value="'+subcatObj.id+'">'+subcatObj.name+'</option>');
            });
        });
    });
    
    // Area District
    $('#district_id_school').on('change',function(e){

    var district_id = e.target.value;
        var url = "{{route('register.area', ':id')}}";
        url = url.replace(':id', district_id);
        $.get(url, function(data){
            $('#area_id_school').empty();
                $.each(data, function(index, subcatObj){
                $('#area_id_school').append('<option value="'+subcatObj.id+'">'+subcatObj.name+'</option>');
            });
        });
    });

    $(document).ready(function(){
		$('body').on('click', '#check', function () {
            let isChecked = $('#check').hasClass('checked');
            if (isChecked){
                $('#check').removeClass('checked');
                $('#name_guardian').val("");
                $("#guardian_jobs").val($("#guardian_jobs option:first").val());
                $('#guardian_education').val($("#guardian_education option:first").val());
                $('#guardian_income').val($("#guardian_income option:first").val());
                $('#guardian_phone_number').val("");
            } else {
                $('#check').addClass('checked');
                var name = $('#name_father').val();
                var education = $('#father_education').val();
                var job = $('#father_jobs').val();
                var income = $('#father_income').val();
                var phone = $('#father_phone_number').val();

                $('#name_guardian').val(name);
                $('#guardian_jobs').val(job).change();
                $('#guardian_education').val(education);
                $('#guardian_income').val(income);
                $('#guardian_phone_number').val(phone);
            }
		});
	});
    </script>
    
	
</body>
</html>
