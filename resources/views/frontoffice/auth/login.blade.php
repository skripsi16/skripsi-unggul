<!DOCTYPE html>
<html class="no-js" lang="id" dir="ltr">
  <head>
    <title>Masuk </title>
    <!-- meta-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- favicon -->
    <link rel="icon" href="{{ asset('frontoffice/assets/images/logo.png') }}">
    <!-- vendor style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontoffice/new_assets/css/vendor.bundle.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- main style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontoffice/new_assets/css/styles.css')}}">
  </head>
  <body>
    <main class="Loreg" id="login">
      <section class="sc-form">
        <div class="input-card input-card--style2">
          <div class="sc-header">
            <h2>Login</h2>
          </div>

          <form action="{{ route('auth-user.loginProcess') }}" method="POST">
            {{ csrf_field() }}
                <div class="input-pack input-pack--distance {{ $errors->has('email') ? 'input-pack--error' : ''}}">
                    <span class="input-pack__label">Email</span>
                    <div class="input-wrap">
                        <input required
                            type="email"
                            name="email"
                            placeholder="Masukan Email"
                            value="{{ old('email') }}"
                        />
                    </div>
                    @if ($errors->has('email'))
                        <small>{{ $errors->first('email') }}</small>
                    @endif
                </div>
                <div class="input-pack input-pack--distance {{ $errors->has('password') ? 'input-pack--error' : ''}}">
                    <span class="input-pack__label"
                        >Kata Sandi</span
                    >
                    <div class="input-wrap">
                        <i class="laz laz-eye toggle-password" toggle="#password-field"></i>
                        <input required
                            type="password"
                            name="password"
                            placeholder="Masukan Kata Sandi"
                            value="{{ old('password') }}" id="password-field"
                        />
                    </div>
                    @if ($errors->has('password'))
                        <small>{{ $errors->first('password') }}</small>
                    @endif
                    @if((Session::has('error')))
                      <small style="color:red">{{Session::get('error')}}</small>
                    @endif
                </div>
                <div class="input-card__label mabo-3 right">
                    <a href="{{ route('auth-user.forgotPassword') }}">Lupa kata sandi?</a>
                </div>
                <input
                    class="link-btn link-btn-primary link-btn-sm link-btn-w-full"
                    type="submit"
                    value="MASUK"
                />
            </form>
          <div class="_or">Atau<span class="mb-o"> Masuk Dengan</span></div>
          <div class="row">
            {{-- <div class="col-12 col-md-12"><a class="link-btn link-btn-fb link-btn-sm link-btn-w-full link-btn-square mabo-2" href="index-logged.html"><i class="fa fa-facebook"></i><span><span class="ds-o">MASUK DENGAN</span> FACEBOOK</span></a></div> --}}
            {{-- <div class="col-12 col-md-12"><a class="link-btn link-btn-gg link-btn-sm link-btn-w-full link-btn-square mabo-5" href="{{ url('auth/google') }}"><img src="{{ asset('frontoffice/new_assets/img/gg.svg') }}"><span><span class="ds-o">MASUK DENGAN</span> GOOGLE</span></a></div> --}}
          </div>
          <div class="input-card__label align-center"><span>Belum Punya Akun? </span><a href="{{ route('auth-user.register') }}">Daftar</a></div>
        </div>
      </section>
    </main>
    <!-- vendor script-->
    <script src="{{ asset('frontoffice/new_assets/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/slick.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/upload-img.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/forms.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/progress.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/datepicker.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/datepicker.id.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/datepicker.set.js') }}"></script>
    <!-- main script-->
    <script src="{{ asset('frontoffice/new_assets/js/main.js') }}"></script>
    <!-- only this page script-->
    <script>
        $(document).ready(function(){
            $(".toggle-password").click(function() {
                $(this).toggleClass("laz-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
        });
    </script>
  </body>
</html>
