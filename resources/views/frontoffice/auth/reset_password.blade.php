<!DOCTYPE html>
<html class="no-js" lang="id" dir="ltr">
  <head>
    <title>Daftar</title>
    <!-- meta-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="keywords" content="Crowdfunding,Yayasan,Galang Dana,Donasi,Campaign,Zakat,Wakaf,Sedekah,Program">
    <meta name="author" content="rckryd">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- favicon -->
    <link rel="icon" href="{{ asset('frontoffice/assets/images/logo.png') }}">
    <!-- vendor style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontoffice/new_assets/css/vendor.bundle.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- main style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontoffice/new_assets/css/styles.css') }}">
  </head>
  <body>
    <main class="Forms">
        <section class="sc-form">
        <div class="container-xs">
            <div class="input-card input-card--style2">
            <div class="sc-header">
                <h2>Buat Kata Sandi Baru</h2>
            </div>
            <form action="{{ route('auth-user.updatePassword') }}" method="post">
                {{ csrf_field() }}
                @method('PUT')
                <input type="hidden" name="token" value="{{ $data->token }}">
                <div class="input-pack input-pack--distance {{ $errors->has('email') ? 'input-pack--error' : ''}}">
                    <span class="input-pack__label">Email</span>
                    <div class="input-wrap">
                        <input
                            type="email"
                            name="email"
                            placeholder="Masukan Email"
                            value="{{ $data->email }}"
                        />
                    </div>
                    @if ($errors->has('email'))
                        <small class="error">{{ $errors->first('email') }}</small>
                    @endif
                </div>
                <div class="input-pack input-pack--distance {{ $errors->has('password') ? 'input-pack--error' : ''}}">
                    <span class="input-pack__label"
                        >Kata Sandi</span
                    >
                    <div class="input-wrap">
                        <i class="laz laz-eye toggle-password" toggle="#password-field"></i>
                        <input
                            type="password"
                            name="password"
                            placeholder="Masukan Kata Sandi" id="password-field"
                        />
                    </div>
                    @if ($errors->has('password'))
                        <small class="error">{{ $errors->first('password') }}</small>
                    @endif
                </div>
                <div class="input-pack input-pack--distance {{ $errors->has('confirm_password') ? 'input-pack--error' : ''}}">
                    <span class="input-pack__label"
                        >Ulangi Kata Sandi</span
                    >
                    <div class="input-wrap">
                        <i class="laz laz-eye toggle-confirm-password" toggle="#confirm-password-field"></i>
                        <input
                            type="password"
                            name="confirm_password"
                            placeholder="Masukan Kata Sandi" id="confirm-password-field"
                        />
                    </div>
                    @if ($errors->has('confirm_password'))
                        <small class="error">{{ $errors->first('confirm_password') }}</small>
                    @endif
                </div>
                <button
                    class="link-btn link-btn-primary link-btn-sm link-btn-w-full mabo-14"
                    type="submit"
                    >BUAT KATA SANDI</button
                >
            </form>
            </div>
        </div>
        </section>
    </main>
    <!-- vendor script-->
    <script src="{{ asset('frontoffice/new_assets/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/slick.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/upload-img.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/forms.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/progress.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/datepicker.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/datepicker.id.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/datepicker.set.js') }}"></script>
    <!-- main script-->
    <script src="{{ asset('frontoffice/new_assets/js/main.js') }}"></script>
    <!-- only this page script-->
  </body>
</html>
<!-- End Bottom Resource -->