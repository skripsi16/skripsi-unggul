<!DOCTYPE html>
<html class="no-js" lang="id" dir="ltr">
  <head>
    <title>Lupa Password</title>
    <!-- meta-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="keywords" content="Crowdfunding,Yayasan,Galang Dana,Donasi,Campaign,Zakat,Wakaf,Sedekah,Program">
    <meta name="author" content="rckryd">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- favicon -->
    <link rel="icon" href="{{ asset('frontoffice/assets/images/logo.png') }}">
    <!-- vendor style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontoffice/new_assets/css/vendor.bundle.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- main style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontoffice/new_assets/css/styles.css') }}">
  </head>
  <body>

    <main class="Forms">
        <section class="sc-form">
        <div class="container-xs">
            <div class="input-card input-card--style2">
            <div class="sc-header">
                <h2>Lupa Kata Sandi?</h2>
            </div><span class="input-card__subtitle">Silahkan masukan Email yang sudah terdaftar.<br>Kami akan mengirimkan petunjuk untuk mengubah kata sandi anda.</span>

                @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p style="color: red">{{ $message }}</p>
                        </div>
                    @elseif ($message = Session::get('error'))
                        <div class="alert alert-danger">
                            <p style="color: red">{{ $message }}</p>
                        </div>
                    @endif
                    <form action="{{ route('auth-user.sendForgotPassword') }}" method="post">
                        {{ csrf_field() }}
                        <div class="input-pack input-pack--distance {{ $errors->has('email') ? 'input-pack--error' : ''}}">
                            <span class="input-pack__label">Email</span>
                            <div class="input-wrap">
                                <input
                                    type="email"
                                    name="email"
                                    placeholder="Masukan Email"
                                    value="{{ old('email') }}"
                                />
                            </div>
                            @if ($errors->has('email'))
                                <small>{{ $errors->first('email') }}</small>
                            @endif
                        </div>
                        <input
                            class="link-btn link-btn-primary link-btn-sm link-btn-w-full mabo-9"
                            type="submit"
                            value="UBAH KATA SANDI"
                        />
                    </form>
            <div class="input-card__label align-center"><span>Coba Masuk Kembali? </span><a href="{{ route('auth-user.login') }}">Masuk</a></div>
            </div>
        </div>
        </section>
    </main>
    <!-- vendor script-->
    <script src="{{ asset('frontoffice/new_assets/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/slick.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/upload-img.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/forms.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/progress.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/datepicker.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/datepicker.id.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/datepicker.set.js') }}"></script>
    <!-- main script-->
    <script src="{{ asset('frontoffice/new_assets/js/main.js') }}"></script>
    <!-- only this page script-->
  </body>
</html>