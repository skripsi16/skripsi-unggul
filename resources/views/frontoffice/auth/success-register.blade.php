<!DOCTYPE html>
<html class="no-js" lang="id" dir="ltr">
  <head>
    <title>Daftar</title>
    <!-- meta-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="keywords" content="Crowdfunding,Yayasan,Galang Dana,Donasi,Campaign,Zakat,Wakaf,Sedekah,Program">
    <meta name="author" content="rckryd">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- favicon -->
    <link rel="icon" href="{{ asset('frontoffice/assets/images/logo.png') }}">
    <!-- vendor style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontoffice/new_assets/css/vendor.bundle.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- main style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontoffice/new_assets/css/styles.css') }}">
  </head>
  <body>

    <main class="Invoice">
        <section class="sc-form">
          <div class="container-sm">
            <div class="input-card input-card--distance">
              <div class="sc-header">
                <h1>Info Pembayaran</h1>
              </div>
              <span class="input-card__subtitle">Silahkan melakukan pembayaran </span>
              <div class="invoice__total">
                <div class="rp" id="nom" value="{{ $data->hasPacket->harga }}">{{number_format($data->hasPacket->harga,0,'.','.')}},-
                  {{-- <span class="fcolor-secondary">624</span> --}}
                </div>
                <span class="_copy" onclick="copyToClipboard('#nom')"> 
                  <i class="fa fa-files-o"></i>
                  <span>SALIN NOMINAL</span>
                </span>
              </div>
              <div class="invoice__warning"><strong>PENTING : </strong>Transfer tepas sampai 3 digit terakhir agar memudahkan tahap proses verifikasi (Nominal 3 digit terakhir akan di sedekahkan)</div><span class="input-card__subtitle">
                 Pembayaran dilakukan ke rekening  <strong class="fcolor-base">{{ $data->hasBankAccount->account_number }}</strong> dibawah ini:</span>
              {{-- <div class="invoice__title-sm">Atas nama : {{ $data->hasBankAccount->name }}</div> --}}
              <div class="invoice__bank">
                <div class="_number">
                  <div class="_anum" id="accNum">{{ $data->hasBankAccount->account_number }}
                  </div>
                  <span class="_copy" onclick="copyToClipboard('#accNum')"><i class="fa fa-files-o"></i><span>SALIN REKENING</span></span>
                </div>
              </div>
              <div><img src="{{asset('backoffice/assets/images/bank/'.  $data->hasBankAccount->hasBank->image)}}" class="img-thumbnail" width="100px" ></div>
              <span class="invoice__title-sm"> <div >{{ $data->hasBankAccount->account_name }}</div></span><br>
              <span class="input-card__subtitle">
                 Transfer ke rekening diatas, sebelum 
                 <strong class="fcolor-base">{{ \Carbon\Carbon::parse($data->register_date)->addDays(4)->format('d F Y') }} </strong>
                 atau donasi akan di batalkan otomatis oleh sistem
              </span>
            </div>
          </div>
        </section>
    </main>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/slick.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/upload-img.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/forms.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/progress.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/datepicker.min.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/datepicker.id.js') }}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/datepicker.set.js') }}"></script>
    <!-- main script-->
    <script src="{{ asset('frontoffice/new_assets/js/main.js') }}"></script>
    <script>
      function copyToClipboard(element) {
        var $temp = $("<input>");
          $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove();
      }
    </script>
</body>
</html>
