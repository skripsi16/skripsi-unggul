<!DOCTYPE html>
<html lang="en">

<head>
    <title>Dashboard</title>
    <!-- meta-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    @section('fb-opengraph')
    <meta property="og:url" content="{{ url('/') }}" />
    <meta property="og:title" content="{{ config('app.name', 'Website-Name') }}" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{ asset('frontoffice/assets/images/logo.png') }}" />
    <meta property="og:description" content="{{ config('app.description', 'Web-Description') }}" />
    <meta name="description" content="{{ config('app.description', 'Web-Description') }}">
    @endsection
    @yield('fb-opengraph')
    <meta name="author" content="Diginusa" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- favicon -->
    <link rel="icon" href="{{ asset('frontoffice/assets/images/logo.png') }}">
    
    <!-- favicon -->
    @yield('top-resource')

    <link rel="icon" href="{{ asset('frontoffice/assets/images/logo.png') }}">  
    <!-- vendor style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontoffice/new_assets/css/vendor.bundle.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- main style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontoffice/new_assets/css/styles.css')}}">
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <!-- End Google Tag Manager (noscript) -->
    <nav class="navbar">
        <div class="navbar__logo pl-5">
            <a href="{{ route('frontoffice') }}">
                <picture><img width="75px" src="{{ asset('frontoffice/assets/images/logo.png') }}" alt="Yayasan Al Noor Wal Hayah Al Khairiah Logo"></picture>
            </a>
        </div>
    <div class="container justify-content-end">
        <div class="navbar__wrapper">
            <ul class="navbar__links-main">
                 <li>{{ Auth::guard('web')->user()->name }}</li>
                @guest("web")
                    <li>
                        <a class="fcolor-primary" href="{{ route('auth-user.login') }}">Masuk</a>
                    </li>
                    <li>
                        <span>Atau</span>
                    </li>
                @endguest
            </ul>
            @auth('web')
                <div class="navbar__profile">
                    <img class="avatar avatar__xs shadow10" src="{{ (Auth::guard('web')->user()->image) ? asset('backoffice/assets/images/students/'.Auth::guard('web')->user()->image) : asset('frontoffice/new_assets/img/no-avatar.png') }}">
                    <div class="navbar__profile__name"></div>
                    <div class="navbar__profile__dropdown">
                        <ul>
                            <li><a href="{{ route('dashboard-user') }}">Dashboard</a></li>
                            <li>
                                <a onclick="event.preventDefault();document.getElementById('logout-form-header').submit();">Keluar</a>
                                <form id="logout-form-header" action="{{ route('auth-user.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            @endauth
            @guest("web")
                <a class="navbar__button navbar__button--register-btn link-btn link-btn-primary link-btn-xs" href="{{ route('auth-donatur.register') }}">DAFTAR</a>
            @endguest
            <button class="navbar__button navbar__button--drawer-btn link-btn link-btn-clear link-btn-xs link-btn-noshadow">
                <span class="fa fa-bars"></span>
            </button>
        </div>
    </div>
</nav>
    <main class="Dashboard">
        <section class="sc-dashboard sc-bg">
          <div class="container">
            <div class="main-content">
              <div class="row box">
                <div class="col-12 col-lg-4">
                  <div class="box-side">
                    <div class="box-side__wrapper">
                      <div class="box-side__profile">
                        <div class="box-side__profile__text">
                            <div class="drawer-menu__logged">
                                <div class="container">
                                    <div>
                                        <img class="avatar avatar__md shadow10" src="{{ (Auth::guard('web')->user()->image) ? asset('backoffice/assets/images/students/'.Auth::guard('web')->user()->image) : asset('frontoffice/new_assets/img/no-avatar.png') }}">
                                        <div class="drawer-menu__logged__user">
                                            <span>Hi, {{ (Auth::guard('web')->user()->name) }} <br>
                                               {{ Auth::guard('web')->user()->email }}
                                            </span>
                                            <div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                         <table>
                            <tbody>
                                <tr style="line-height:25px;">
                                    <td>No. Pendaftaran</td>
                                    <td>:</td>
                                    <td><b> {{ $register->no_register }}</b></td>
                                    
                                </tr>
                                <tr style="line-height:25px;">
                                    {{-- <hr> --}}
                                    <td>Paket</td>
                                    <td>:</td>
                                    <td><b> {{ $register->hasPacket->name }}</b></td>
                                    {{-- <hr> --}}
                                </tr>
                                <tr style="line-height:25px;">
                                    <td>Cabang</td>
                                    <td>:</td>
                                    <td><b> {{ $register->hasBranch->name }}</b></td>
                                    {{-- <hr> --}}
                                </tr>
                                <tr style="line-height:25px;">
                                    <td>Kategori</td>
                                    <td>:</td>
                                    <td><b> {{ $register->hasCategory->name }}</b></td>
                                </tr>
                                <tr style="line-height:25px;">
                                    <td>Pembayaran</td>
                                    <td>:</td>
                                    <td><b>@if ($register->status == "VERIFIED")
                                        <span class="btn-success">TERBAYAR</span>
                                    @elseif ($register->status == "UNVERIFIED")
                                        <span class="btn-warning">BELUM TERBAYAR</span>
                                    @else
                                        <span class="btn-danger">CANCEL</span>
                                    @endif</b></td>
                                </tr>
                            </tbody>
                        </table>
                      
                    </div>
                  </div>
                </div>
                <div class="col-12 col-lg-8">
                  @yield('content')
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
   
    <!-- vendor script-->
    <script src="{{ asset('frontoffice/new_assets/js/vendor/jquery.min.js')}}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/slick.min.js')}}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/upload-img.js')}}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/forms.js')}}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/progress.js')}}"></script>
    <!-- main script-->
    <script src="{{ asset('frontoffice/new_assets/js/main.js')}}"></script>

    <script src="{{ asset('frontoffice/new_assets/js/vendor/datepicker.min.js')}}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/vendor/datepicker.id.js')}}"></script>
    <script src="{{ asset('frontoffice/new_assets/js/datepicker.set.js')}}"></script>
    <!-- only this page script-->
    <script>
        function copyToClipboard(element) {
          var $temp = $("<input>");
            $("body").append($temp);
              $temp.val($(element).text()).select();
              document.execCommand("copy");
              $temp.remove();
        }
    </script>

    @yield('bottom-resource')
    
</body>

</html>
