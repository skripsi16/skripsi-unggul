<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterDataTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_banks', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('name', 255);
            $table->string('image', 255);
            $table->tinyInteger('enabled')->default('0')->nullable();
			$table->timestamps();
        });

        Schema::create('sys_bank_accounts', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('account_name', 255);
            $table->string('account_number', 255)->unique();
            $table->string('bank_id', 255);
            $table->tinyInteger('enabled')->default('0')->nullable();
			$table->timestamps();
        });

        Schema::create('sys_sliders', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('image', 100)->nullable();
            $table->string('title', 100)->nullable();
            $table->string('link', 255)->nullable();
            $table->string('category', 255);
            $table->tinyInteger('enabled')->default('0')->nullable();
            $table->timestamps();
        });

        Schema::create('sys_brands', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('image', 100)->nullable();
            $table->text('description')->nullalble();
            $table->tinyInteger('enabled')->default('0')->nullable();
            $table->timestamps();
        });

        Schema::create('sys_profile_companies', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('name', 255)->nullable();
            $table->text('description')->nullable();
            $table->string('image_url', 255)->nullable();
            $table->text('website')->nullable();
            $table->text('address')->nullable();
            $table->text('email')->nullable();
            $table->text('phone_number')->nullable();
            $table->text('social_media')->nullable();
			$table->string('created_by', 100)->nullable();
			$table->string('updated_by', 100)->nullable();
			$table->timestamps();
        });

        Schema::create('sys_faq_categories', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('name', 255);
			$table->text('description');
            $table->tinyInteger('enabled')->default('0')->nullable();
			$table->timestamps();
        });

        Schema::create('sys_faq_descriptions', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->foreign('faq_categories_id')->references('id')->on('sys_faq_categories');
            $table->string('faq_categories_id', 50);
            $table->text('answer');
            $table->tinyInteger('enabled')->default('0')->nullable();
			$table->timestamps();
        });

        Schema::create('sys_categories', function (Blueprint $table) {
            $table->string('id',50)->primary();
            $table->string('name');
            $table->text('description')->nullable();
            $table->tinyInteger('enabled')->default('0')->nullable();
            $table->timestamps();
        });

        Schema::create('sys_branch', function (Blueprint $table) {
            $table->string('id',50)->primary();
            $table->string('name');
            $table->text('description')->nullable();
            $table->tinyInteger('enabled')->default('0')->nullable();
            $table->timestamps();
        });

        Schema::create('sys_packets', function (Blueprint $table) {
            $table->string('id',50)->primary();
            $table->string('branch_id', 50)->nullable();
            $table->string('name');
            $table->string('harga', 50);
            $table->text('description')->nullable();
            $table->tinyInteger('enabled')->default('0')->nullable();
            $table->timestamps();
        });

        Schema::create('sys_category_packets', function (Blueprint $table) {
            $table->string('id',50)->primary();
            $table->string('name');
            $table->text('description')->nullable();
            $table->tinyInteger('enabled')->default('0')->nullable();
            $table->timestamps();
        });

        Schema::create('sys_terms', function (Blueprint $table) {
            $table->string('id',50)->primary();
            $table->string('title');
            $table->text('description')->nullable();
            $table->tinyInteger('enabled')->default('0')->nullable();
            $table->timestamps();
        });
        
        Schema::create('sys_prosedurs', function (Blueprint $table) {
            $table->string('id',50)->primary();
            $table->string('name', 100);
            $table->text('description')->nullable();
            $table->tinyInteger('enabled')->default('0')->nullable();
            $table->timestamps();
        });

        Schema::create('sys_educations', function (Blueprint $table) {
            $table->string('id',50)->primary();
            $table->string('name', 100);
            $table->text('description')->nullable();
            $table->tinyInteger('enabled')->default('0')->nullable();
            $table->timestamps();
        });

        Schema::create('sys_jobs', function (Blueprint $table) {
            $table->string('id',50)->primary();
            $table->string('name', 100);
            $table->string('category', 100);
            $table->text('description')->nullable();
            $table->tinyInteger('enabled')->default('0')->nullable();
            $table->timestamps();
        });

        Schema::create('sys_salaries', function (Blueprint $table) {
            $table->string('id',50)->primary();
            $table->string('amount', 100);
            $table->text('description')->nullable();
            $table->tinyInteger('enabled')->default('0')->nullable();
            $table->timestamps();
        });

        Schema::create('sys_countries', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('name', 100);
            $table->tinyInteger('enabled')->default('0')->nullable();
        });

        Schema::create('sys_provinces', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('country_id', 100);
            $table->string('name', 100);
            $table->tinyInteger('enabled')->default('0')->nullable();
        });

        Schema::create('sys_cities', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('country_id', 100);
            $table->string('province_id', 100);
            $table->string('name', 100);
            $table->tinyInteger('enabled')->default('0')->nullable();
        });

        Schema::create('sys_districts', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('country_id', 100);
            $table->string('province_id', 100);
            $table->string('city_id', 100);
            $table->string('name', 100);
            $table->tinyInteger('enabled')->default('0')->nullable();
        });

        Schema::create('sys_areas', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('country_id', 100);
            $table->string('province_id', 100);
            $table->string('city_id', 100);
            $table->string('district_id', 100);
            $table->string('name', 100);
            $table->tinyInteger('enabled')->default('0')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_banks');
        Schema::dropIfExists('sys_bank_accounts');
        Schema::dropIfExists('sys_sliders');
        Schema::dropIfExists('sys_brands'); 
        Schema::dropIfExists('sys_profile_companies'); 
        Schema::dropIfExists('sys_faq_description');
        Schema::dropIfExists('sys_faq_categories');
        Schema::dropIfExists('sys_categories');
        Schema::dropIfExists('sys_category_packets');
        Schema::dropIfExists('sys_branch'); 
        Schema::dropIfExists('sys_packets'); 
        Schema::dropIfExists('sys_terms');
        Schema::dropIfExists('sys_prosedurs'); 
        Schema::dropIfExists('sys_educations'); 
        Schema::dropIfExists('sys_jobs'); 
        Schema::dropIfExists('sys_salaries'); 
        Schema::dropIfExists('sys_countries');
        Schema::dropIfExists('sys_provinces');
        Schema::dropIfExists('sys_cities');
        Schema::dropIfExists('sys_districts');
        Schema::dropIfExists('sys_areas');
    }
}
