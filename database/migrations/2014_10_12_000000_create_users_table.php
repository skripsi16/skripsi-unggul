<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registers', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('no_register', 100);
            $table->string('santri_id', 100);
            $table->string('packet_id', 100);
            $table->string('branch', 100);
            $table->string('year', 100);
            $table->string('payment', 100)->nullable();
            $table->string('financing', 100)->nullable();
            $table->string('register_date', 100);
            $table->string('category', 100);
            $table->string('bank_id', 100);
            $table->string('status', 100)->nullable();
            $table->timestamps();
        });

        Schema::create('students', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('nisn', 50)->unique();
            $table->string('nik', 50)->unique();
            $table->string('name', 100);
            $table->string('phone_number', 25)->unique();
            $table->string('email', 100)->unique()->nullable();
            $table->string('password', 100)->nullable();
            $table->string('gender', 50);
            $table->string('image', 255)->nullabe();
            $table->string('birth_place', 100);
            $table->string('birth_date', 100);
            $table->string('religion', 50)->nullable();
            $table->string('status_of_family', 100);
            $table->string('country_id', 100);
            $table->string('province_id', 100);
            $table->string('city_id', 100);
            $table->string('district_id', 100);
            $table->string('area_id', 100);
            $table->text('address')->nullable();
            $table->string('enabled', 50)->nullable();
            $table->timestamps();
        });

        Schema::create('parents', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('santri_id', 50);
            $table->string('name_father', 50);
            $table->string('name_mother', 50);
            $table->string('name_guardian', 50)->nullable();
            $table->string('father_education', 50);
            $table->string('mother_education', 50);
            $table->string('guardian_education', 50)->nullable();
            $table->string('father_jobs', 50);
            $table->string('mother_jobs', 50);
            $table->string('guardian_jobs', 50)->nullable();
            $table->string('father_income', 50);
            $table->string('mother_income', 50);
            $table->string('guardian_income', 50)->nullable();
            $table->string('father_phone_number', 25);
            $table->string('mother_phone_number', 25);
            $table->string('guardian_phone_number', 25)->nullable();
            $table->string('status', 50)->nullable();
            $table->timestamps();
        });

        Schema::create('schools', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('santri_id', 50);
            $table->string('npsn', 50);
            $table->string('school_name', 50);
            $table->string('exam', 100);
            $table->string('graduation_year', 50);
            $table->string('status_of_school', 100);
            $table->string('country_id_school', 100);
            $table->string('province_id_school', 100);
            $table->string('city_id_school', 100);
            $table->string('district_id_school', 100);
            $table->string('area_id_school', 100);
            $table->text('address_of_school')->nullable();
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('name', 100);
            $table->string('email', 100)->unique();
            $table->string('password', 100);
            $table->tinyInteger('enabled')->default('0')->nullable();
            $table->timestamps();
        });

        Schema::create('sys_admins', function (Blueprint $table) {
            $table->string('id', 50)->primary();
            $table->string('name', 100);
            $table->string('email')->unique();
            $table->string('gender', 100)->nullable();
            $table->string('phone_number', 13)->nullable();
            $table->text('address')->nullable();
            $table->string('image', 100)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->tinyInteger('enabled')->default('0')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('schools');
        Schema::dropIfExists('parents');
        Schema::dropIfExists('students');
        Schema::dropIfExists('registers');
        Schema::dropIfExists('sys_admins');
    }
}
