<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use DB;

class MasterDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		/*
        * Permission master data for Salary
        * \Model\Salary
        */
		DB::table('sys_salaries')->insert(array(
            array(
				'id'			    => '1',
				'amount'            => '< 500rb',
                'description'       => 'Penghasilannya kurang dari 500rb',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_salaries')->insert(array(
            array(
				'id'			    => '2',
				'amount'            => '500 - 1jt',
                'description'       => 'Penghasilannya dari 500rb sampai dengan 1jt',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_salaries')->insert(array(
            array(
				'id'			    => '3',
				'amount'            => '1jt - 3jt',
                'description'       => 'Penghasilannya dari 1jt sampai dengan 3jt',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_salaries')->insert(array(
            array(
				'id'			    => '4',
				'amount'            => '3jt - 5jt',
                'description'       => 'Penghasilannya dari 3jt sampai dengan 5jt',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_salaries')->insert(array(
            array(
				'id'			    => '5',
				'amount'            => '5jt - 10jt',
                'description'       => 'Penghasilannya dari 5jt sampai dengan 10jt',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));


        /*
        * Permission master data for Job
        * \Model\Job
        */
		DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '1',
				'name'              => 'Buruh',
                'category'          => 'Ayah',
                'description'       => 'Buruh',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '2',
				'name'              => 'Tani',
                'category'          => 'Ayah',
                'description'       => 'Tani',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '3',
				'name'              => 'Wirawasta',
                'category'          => 'Ayah',
                'description'       => 'Wirawasta',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '4',
				'name'              => 'PNS',
                'category'          => 'Ayah',
                'description'       => 'PNS',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '5',
				'name'              => 'TNI/POLRI',
                'category'          => 'Ayah',
                'description'       => 'TNI/POLRI',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '6',
				'name'              => 'Perangkat Desa',
                'category'          => 'Ayah',
                'description'       => 'Perangkat Desa',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '7',
				'name'              => 'Nelayan',
                'category'          => 'Ayah',
                'description'       => 'Nelayan',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '8',
				'name'              => 'Karyawan Swasta',
                'category'          => 'Ayah',
                'description'       => 'Karyawan Swasta',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '9',
				'name'              => 'Lain-Lain',
                'category'          => 'Ayah',
                'description'       => 'Lain-Lain',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '10',
				'name'              => 'Ibu Rumah Tangga',
                'category'          => 'Ibu',
                'description'       => 'Ibu Rumah Tangga',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '11',
				'name'              => 'Buruh',
                'category'          => 'Ibu',
                'description'       => 'Buruh',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '12',
				'name'              => 'Tani',
                'category'          => 'Ibu',
                'description'       => 'Tani',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '13',
				'name'              => 'Wirawasta',
                'category'          => 'Ibu',
                'description'       => 'Wirawasta',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '14',
				'name'              => 'PNS',
                'category'          => 'Ibu',
                'description'       => 'PNS',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '15',
				'name'              => 'TNI/POLRI',
                'category'          => 'Ibu',
                'description'       => 'TNI/POLRI',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '16',
				'name'              => 'Perangkat Desa',
                'category'          => 'Ibu',
                'description'       => 'Perangkat Desa',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '17',
				'name'              => 'Nelayan',
                'category'          => 'Ibu',
                'description'       => 'Nelayan',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '18',
				'name'              => 'Karyawan Swasta',
                'category'          => 'Ibu',
                'description'       => 'Karyawan Swasta',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_jobs')->insert(array(
            array(
				'id'			    => '19',
				'name'              => 'Lain-Lain',
                'category'          => 'Ibu',
                'description'       => 'Lain-Lain',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        /*
        * Permission master data for Education
        * \Model\Education
        */
		DB::table('sys_educations')->insert(array(
            array(
				'id'			    => '1',
				'name'              => 'Tidak Sekolah',
                'description'       => 'Tidak Sekolah',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_educations')->insert(array(
            array(
				'id'			    => '2',
				'name'              => 'SD/MI',
                'description'       => 'SD/MI',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_educations')->insert(array(
            array(
				'id'			    => '3',
				'name'              => 'SMP/MTs',
                'description'       => 'SMP/MTs',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_educations')->insert(array(
            array(
				'id'			    => '4',
				'name'              => 'SMK/SMA/MA',
                'description'       => 'SMK/SMA/MA',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_educations')->insert(array(
            array(
				'id'			    => '5',
				'name'              => 'DIPLOMA',
                'description'       => 'DIPLOMA',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        // Profile Company
		DB::table('sys_profile_companies')->insert(array(
            array(
				'id'			=> '2020151001001',
				'name' 			=> 'PT. Lorem Ipsum',
                'description'	=> '{"profile":"Lorem Ipsum","visimisi":"Lorem Ipsum","sejarah":"Lorem Ipsum","manajemen":"Lorem Ipsum","legalitas":"Lorem Ipsum","termcondition":"Lorem Ipsum","termfundraiser":"Lorem Ipsum","privacy":"Lorem Ipsum"}',
				'website'		=> '{"website1":"Lorem Ipsum","website2":"-"}',
				'address'		=> '{"address1":"Lorem Ipsum","address2":"-"}',
				'email'			=> '{"email1":"loremipsum@mail.com","email2":"-"}',
                'phone_number'	=> '{"phone_1":"Lorem Ipsum","phone_2":"-"}',
				'social_media'	=> '{"facebook":"Lorem Ipsum","instagram":"Lorem Ipsum","youtube":"-"}',
            )
        ));

        /*
        * Permission master data for Category
        * \Model\Category
        */
		DB::table('sys_categories')->insert(array(
            array(
				'id'			    => '1',
				'name'              => 'Setiap calon siswa wajib mengisi form pendaftaran dengan lengkap. Calon Santri beserta Keluarga, diwajibkan mengamalkan Thoolibuut Taqwa Method',
                'description'       => '<p>a. Shalat Berjamaah &amp; Jaga Hati, Jaga Sikap<br />
                                        b. Tahajjud, Dhuha &amp; Qobliyah Ba&#39;diyah<br />
                                        c. Menghafal &amp; Tadabbur Al-Qur&#39;an<br />
                                        d. Sedekah &amp; Puasa Sunnah<br />
                                        e. Belajar &amp; Mengajar<br />
                                        f. Do&#39;a, Mendo&#39;akan &amp; Minta Didoakan<br />
                                        g. Ikhlas, Sabar, Syukur &amp; Ridho</p>',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

		DB::table('sys_categories')->insert(array(
            array(
				'id'			    => '2',
				'name'              => 'Keuangan',
                'description'       => '<p>a. Membayar Biaya Formulir sesuai dengan nominal yang di tentukan.<br />
                                        b. Membayar Dana Sumbangan Pendidikan.<br /></p>',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_categories')->insert(array(
            array(
				'id'			    => '3',
				'name'              => 'Patuh pada Syarat dan Ketentuan yang ditetapkan',
                'description'       => '<p>Ketentuan & Syarat berlaku, dibawah ini :</p>',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        /*
        * Permission master data for Term
        * \Model\Term
        */
		DB::table('sys_terms')->insert(array(
            array(
				'id'			    => '1',
				'title'             => 'Pasal 1. Pengertian',
                'description'       => '<ol>
                                            <li>Program santri indent adalah Program booking seat di sekolah/Thoolibuut Taqwa pada tahun yang diinginkan peserta dari indent 1 (Satu) tahun 2 (Dua) 3 Tiga) tahun dst.</li>
                                            <li>Peserta Santri Indent adalah perorangan (calon santri) yang berhak mengikuti ujian tes masuk di Sekolah Thoolibuut Taqwa ditahun yang ditetapkan setelah melakukan registrasi atau pendaftaran dan bersedia mengemban pendidikan di Sekolah Pesantren Thoolibut Tawka yang tersedia.</li>
                                            <li>Sekolah/Thoolibuut Taqwa adalah nama lembaga yang bergerak di bidang pendidikan islam dan Tahfizh berbasis boarding atau fullday dibawah naungan Yayasan Thoolibuut Taqwa Indonesia.</li>
                                            <li>Registrasi adalah Proses pendaftaran yang wajib dilakukan perorangan agar terdaftar sebagai santri indent Thoolibuut Taqwa.</li>
                                            <li>Penempatan Sekolah/Thoolibuut Taqwa adalah proses penempatan santri indent diberbagai Sekolah Thoolibuut Taqwa sesuai hasil tes ujian masuk, adapun lokasi sekolah/Thoolibuut Taqwa tersedia di website resmi Sekolah/Thoolibuut Taqwa.</li>
                                            <li>Website sekolah/Thoolibuut Taqwa adalah website resmi Sekolah Thoolibuut Taqwa yang memuat semua informasi tentang kegiatan dan aktivitas serta pendaftaran di sekolah/Thoolibuut Taqwa yaitu&nbsp;<strong>www.thoolibuutakwa.com</strong></li>
                                            <li>Masa Aktif&nbsp;<strong>Kode Daftar</strong>&nbsp;yaitu 14 (empat belas) hari kerja terhitung sejak tanggal pendaftaran.</li>
                                            <li>Kode Daftar dapat digunakan untuk akses ke portal Login Thoolibut Taqwa.</li>
                                            <li>Akun Virtual adalah kode virtual yang dimiliki santri indent pesantren Tahfizh yang digunakan pada saat pembayaran uang masuk dengan fasilitas virtual account bank dan dapat diperoleh pada saat pendaftaran.</li>
                                            <li><strong>Kode Unik</strong>&nbsp;adalah nilai yang ditambahkan dalam setiap pembayaran, guna memudahkan system mengenali pembayaran yang dilakukan, Kode unik akan berbeda-beda dalah setiap transaksi.</li>
                                            <li>Marketing Gallery Thoolibuut Taqwa adalah kantor/ gerai marketing Thoolibuut Taqwa yang tersebar di beberapa wilayah untuk memudahkan calon santri indent mendapatkan informasi tentang Thoolibuut Taqwa dan dalam proses pendaftaran, adapun lokasi terdaftar dalam website resmi Thoolibuut Taqwa.</li>
                                            <li>Pendaftaran Online adalah pengisian formulir via daring (online) pada alamat URL&nbsp;<strong>https://thoolibuttaqwa.</strong></li>
                                            <li>Refund adalah pengembalian dana akibat pengunduran diri dari keikutsertaan program santri indent Thoolibuut Taqwa</li>
                                            <li>Cashback adalah pengembalian uang yang telah disetor akan akibat tidak lulus tes masuk Thoolibuut Taqwa</li>
                                        </ol>',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_terms')->insert(array(
            array(
				'id'			    => '2',
				'title'              => 'Pasal 2. Registrasi',
                'description'       => '<ol>
                                            <li>Sebelum menjadi peserta program santri indent Thoolibuut Taqwa calon peserta wajib melakukan registrasi/pendaftaran dengan cara mengisi formulir via Formulir Online di website pendaftaran santri indent Thoolibuut Taqwa pada URL https://thoolibuttaqwa.com atau datang langsung ke Marketing Gallery Thoolibuut Taqwa.</li>
                                            <li>Ketika pengisian formulir selesai peserta akan mendapatkan souvenir merchandise dari Thoolibuut Taqwa serta menerima Kode Pendaftaran dan nomor kode virtual untuk pembayaran.</li>
                                            <li>Setelah proses pengisian formulir selesai, peserta wajib membayar biaya pendaftaran sesuai dengan yang diberikan system paling lambat 14 (Empat Belas) hari kerja setelah tanggal pengisian formulir. Apabila habis masa aktif Kode Pendaftaran dan peserta membayaran biaya pendaftaran, maka kode pendaftaran dianggap hangus dan peserta dianggap mengundurkan diri.</li>
                                            <li>Peserta calon santri indent menjamin bahwa seluruh data yang disampaikan dalam proses registrasi adalah akurat, lengkap, dan sesuai dengan kondisi terkini, peserta wajib memberikan pembaruan informasi kepada sekolah/ Thoolibuut Taqwa apabila terdapat perubahan /tambahan informasi baik dengan maupun tidak diminta oleh sekolah/Thoolibuut Taqwa.</li>
                                        </ol>',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_terms')->insert(array(
            array(
				'id'			    => '3',
				'title'             => 'Pasal 3. Program santri Indent Thoolibuut Taqwa',
                'description'       => '<ol>
                                            <li>Program santri Indent Thoolibuut Taqwa tersedia di seluruh Sekolah/Thoolibuut Taqwa yang terdaftar pada website resmi pada URL https://thoolibuttaqwa</li>
                                            <li>Program Santri Indent Sekolah/Thoolibuut Taqwa berlaku setelah peserta selesai melakukan seluruh alur atau proses registrasi pendaftaran.</li>
                                            <li>Program santri indent diselenggarakan oleh Thoolibuut Taqwa dibatasi oleh kuota masing-masing penempatan pada tiap tahun pelajaran demi kenyamanan dan keefektifan dalam proses kegiatan belajar mengajar di Thoolibuut Taqwa.</li>
                                            <li>Thoolibuut Taqwa akan melaukan tes ujian masuk yang akan diikuti oleh santri indent sesuai dengan indent pilihan tahunnya penerimaan dan penempatan santri sesuai kuota Pesantren Tahfizh dalam menerima santri.</li>
                                        </ol>',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_terms')->insert(array(
            array(
				'id'			    => '4',
				'title'             => 'Pasal 4. Tentang Kepesertaan Program santri Indent Thoolibuut Taqwa',
                'description'       => '<ol>
                                            <li>Peserta berkewajiban melakukan pembayaran melalui tata cara yang telah diinfokan tentang tata cara pembayaran uang masuk santri indent baik tunai maupun pembiayaan melalui Bank Syariah.</li>
                                            <li>Peserta bertanggung jawab sepenuhnya atas resiko dan kerugian yang terjadi selama mengikuti program santri indent sekolah/Thoolibuut Taqwa, antara lain pemotongan refund sebagai biaya administrasi akibat mengundurkan diri dari program tersebut dan peserta dengan ini membebaskan Thoolibuut Taqwa dari segala tuntutan dan gugatan dari pihak manapun atas keikutsertaan dalam program ini.</li>
                                            <li>Peserta berkewajiban memberikan data dengan akurat, lengkap dan sesuai dengan kondisi terkini dan Peserta wajib memberikan pembaruan informasi kepada Thoolibuut Taqwa apabila terdapat perubahan/tambahan informasi baik dengan maupun tidak diminta oleh Thoolibuut Taqwa.</li>
                                            <li>Peserta berhak menerima penempatan Sekolah/Thoolibuut Taqwa Sesuai hasil tes Ujian Masuk yang di selenggarakan oleh panitia ujian masuk Sekolah/Thoolibuut Taqwa.</li>
                                        </ol>',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_terms')->insert(array(
            array(
				'id'			    => '5',
				'title'             => 'Pasal 5. Perubahan Ketentuan dan syarat Program Santri Indent',
                'description'       => '<ol>
                                            <li>Thoolibuut Taqwa dapat merubah ketentuan dan syarat program ini setiap saat, dengan mencantumkan perubahan tersebut di website Thoolibuut Taqwa.</li>
                                        </ol>',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_terms')->insert(array(
            array(
				'id'			    => '6',
				'title'             => 'Pasal 6. Hukum yang berlaku dan Penyelesaian Perselisihan',
                'description'       => '<ol>
                                            <li>Ketentuan dan Syarat Program ini tunduk dan patuh pada hukum dan peraturan perundang-undangan yang berlaku di wilayah Republik Indonesia.</li>
                                            <li>Penyelesaian persellisihan antara Thoolibuut Taqwa dan Peserta Program Santri Indent akibat pelaksanaan program ini akan diselesaikan melalui Pejabat yang berwenang, dengan tidak mengenyampingkan kemungkinan penyelesaian secara kekeluargaan.</li>
                                        </ol>',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

         /*
        * Permission master data for Kategori Paket
        * \Model\Kategori Paket
        */
		DB::table('sys_category_packets')->insert(array(
            array(
				'id'			    => '1',
				'name'              => 'Paket Reguler',
                'description'       => 'Paket Reguler',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_category_packets')->insert(array(
            array(
				'id'			    => '2',
				'name'              => 'Alumni Thoolibuut Taqwa',
                'description'       => 'Alumni Thoolibuut Taqwa',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_category_packets')->insert(array(
            array(
				'id'			    => '3',
				'name'              => 'Anak ke 2 di Thoolibuut Taqwa',
                'description'       => 'Anak ke 2 di Thoolibuut Taqwa',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_category_packets')->insert(array(
            array(
				'id'			    => '4',
				'name'              => 'Anak ke 3 di Thoolibuut Taqwa',
                'description'       => 'Anak ke 3 di Thoolibuut Taqwa',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_category_packets')->insert(array(
            array(
				'id'			    => '5',
				'name'              => 'Santri Pindahan',
                'description'       => 'Santri Pindahan',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        /*
        * Permission master data for Prosedur
        * \Model\Prosedur
        */
		DB::table('sys_prosedurs')->insert(array(
            array(
				'id'			    => '1',
				'name'              => 'Pendaftaran',
                'description'       => 'Calon Santri Melakukan Pendaftaran Online di web https://thoolibuttaqwa',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_prosedurs')->insert(array(
            array(
				'id'			    => '2',
				'name'              => 'Pembayaran',
                'description'       => 'Calon Santri Melakukan Pembayaran Pendaftaran melalui Virtual Akun Bank Mandiri',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_prosedurs')->insert(array(
            array(
				'id'			    => '3',
				'name'              => 'Login',
                'description'       => 'Calon Santri Login Menggunakan User name dan Password yang di dapat pada tahap dua',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_prosedurs')->insert(array(
            array(
				'id'			    => '4',
				'name'              => 'Pemilihan',
                'description'       => 'Calon Santri Memilih Paket Pendaftaran dari Sd s/d SMA',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_prosedurs')->insert(array(
            array(
				'id'			    => '5',
				'name'              => 'Pembayaran Cicilan',
                'description'       => 'Calon Santri Dapat Melakukan Pembayaran Cicilan sesuai paket yang dipilih',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_prosedurs')->insert(array(
            array(
				'id'			    => '6',
				'name'              => 'Penerimaan',
                'description'       => 'Calon Santri mengikuti Tes Penerimaan Santri Baru',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_prosedurs')->insert(array(
            array(
				'id'			    => '7',
				'name'              => 'Pengumuman Hasil',
                'description'       => 'Calon Santri Mendapatkan Hasil Pengumuman (Lulus atau tidak Lulus)',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_prosedurs')->insert(array(
            array(
				'id'			    => '8',
				'name'              => 'Lulus',
                'description'       => 'Jika Lulus, Calon Santri harus melengkapi berkas berkas yang dibutuhkan',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));

        DB::table('sys_prosedurs')->insert(array(
            array(
				'id'			    => '9',
				'name'              => 'Tidak Lulus',
                'description'       => 'Jika Tidak Lulus, Ketentuan mengacu pada syarat dan ketentuan yang berlaku',
                'enabled'			=> '1',
                'created_at'        => NOW()
            )
        ));
    }
}