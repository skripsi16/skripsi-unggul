// Hide Show Password
$(".togglePassword").click(function () {
    $(this).toggleClass("laz-eye laz-eye-slash");
    var toggleInput = $(this).parent('.input-wrap').find('input')
    if (toggleInput.attr("type") == "password") {
        toggleInput.attr("type", "text");
    } else {
        toggleInput.attr("type", "password");
    }
});